//QuerySelectorAll for IE7-

(function(d, s) {
    d = document, s = d.createStyleSheet();
    d.querySelectorAll = function(r, c, i, j, a) {
        a = d.all, c = [], r = r.replace(/\[for\b/gi, '[htmlFor').split(',');
        for (i = 0; i < r.length; i++) {
            s.addRule(r[i], 'k:v');
            for (j =0; j < a.length; j++) a[j].currentStyle.k && c.push(a[j]);
            s.removeRule(0);
        }
        return c;
    };
    d.querySelector = function(r) {
        var e = d.querySelectorAll(r);
        // var l = e.length;
        return (e.length) ? e[0] : null;
    };
})()



// Below is testing scenario on home page of yourstory

// var tvc_all=document.querySelectorAll('#home-latest a'); for(k=0;k<tvc_all.length;k++){console.log(tvc_all[k].innerText);}