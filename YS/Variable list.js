var tvc_variables={
 "variable": [
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/1",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "1",
   "name": "gaPrimaryPropertyId",
   "type": "smm",
   "parameter": [
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "input",
     "value": "{{Page Hostname}}"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "UA-18111131-5"
    },
    {
     "type": "list",
     "key": "map",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "key",
         "value": "yourstory.com"
        },
        {
         "type": "template",
         "key": "value",
         "value": "UA-18111131-5"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "key",
         "value": "ys2.yourstory.com"
        },
        {
         "type": "template",
         "key": "value",
         "value": "UA-1111111-X"
        }
       ]
      }
     ]
    }
   ],
   "fingerprint": "1494249274305",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/1?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/2",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "2",
   "name": "authorName",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "authorName"
    }
   ],
   "fingerprint": "1481268539989",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/2?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/3",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "3",
   "name": "Search Query",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "searchQuery"
    }
   ],
   "fingerprint": "1481268662955",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/3?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/4",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "4",
   "name": "gaCampaignPageviewsId",
   "type": "c",
   "parameter": [
    {
     "type": "template",
     "key": "value",
     "value": "UA-81511582-1"
    }
   ],
   "fingerprint": "1477388837842",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/4?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/7",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "7",
   "name": "tvc_user_a_live",
   "type": "k",
   "parameter": [
    {
     "type": "boolean",
     "key": "decodeCookie",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_user_a_live"
    }
   ],
   "fingerprint": "1479127980400",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/7?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/8",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "8",
   "name": "tvc_clientid",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_clientid"
    }
   ],
   "fingerprint": "1479128123549",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/8?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/11",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "11",
   "name": "tvc_page_name",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_page_name"
    }
   ],
   "fingerprint": "1481268451218",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/11?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/12",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "12",
   "name": "tvc_page_section",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_page_section"
    }
   ],
   "fingerprint": "1481268850658",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/12?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/13",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "13",
   "name": "tvc_social_media_platform",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_social_media_platform"
    }
   ],
   "fingerprint": "1481272719214",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/13?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/14",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "14",
   "name": "tvc_language",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_language"
    }
   ],
   "fingerprint": "1481268783867",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/14?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/15",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "15",
   "name": "tvc_article_category",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_article_category"
    }
   ],
   "fingerprint": "1481268676968",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/15?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/16",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "16",
   "name": "tvc_article_name",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_article_name"
    }
   ],
   "fingerprint": "1481268683669",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/16?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/17",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "17",
   "name": "articleCategory",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "articleCategory"
    }
   ],
   "fingerprint": "1481268512989",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/17?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/18",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "18",
   "name": "articleTitle",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "articleTitle"
    }
   ],
   "fingerprint": "1479882404433",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/18?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/19",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "19",
   "name": "publicationYear",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "publicationYear"
    }
   ],
   "fingerprint": "1479882499515",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/19?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/20",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "20",
   "name": "publicationMonth",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "publicationMonth"
    }
   ],
   "fingerprint": "1479882543604",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/20?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/21",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "21",
   "name": "tvc_publication_month_year",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\tif({{publicationMonth}} == \"NA\"){\n    \treturn \"NA\";\n    }\n  return {{publicationMonth}} + \" \" + {{publicationYear}};\n}"
    }
   ],
   "fingerprint": "1479884369872",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/21?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/22",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "22",
   "name": "tvc_publication_final_date",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\tif({{publicationDate}} == \"NA\"){\n    \treturn \"NA\";\n    }\n  return {{publicationDate}} + \" \" + {{publicationMonth}} + \" \" + {{publicationYear}};\n}"
    }
   ],
   "fingerprint": "1479884416476",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/22?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/23",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "23",
   "name": "language",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "language"
    }
   ],
   "fingerprint": "1479882888106",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/23?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/24",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "24",
   "name": "videoUrl",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "videoUrl"
    }
   ],
   "fingerprint": "1479882916838",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/24?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/25",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "25",
   "name": "articleLength",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "articleLength"
    }
   ],
   "fingerprint": "1479883558137",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/25?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/26",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "26",
   "name": "tags",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tags"
    }
   ],
   "fingerprint": "1479883594685",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/26?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/27",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "27",
   "name": "publicationDate",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "publicationDate"
    }
   ],
   "fingerprint": "1479884295749",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/27?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/28",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "28",
   "name": "tvc_navigation_level_name",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_navigation_level_name"
    }
   ],
   "fingerprint": "1479886422896",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/28?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/29",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "29",
   "name": "tvc_event_category",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_event_category"
    }
   ],
   "fingerprint": "1479887058325",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/29?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/30",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "30",
   "name": "tvc_navigation_position",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_navigation_position"
    }
   ],
   "fingerprint": "1481027719599",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/30?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/31",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "31",
   "name": "tvc_footer_link_label",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_footer_link_label"
    }
   ],
   "fingerprint": "1481032244713",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/31?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/32",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "32",
   "name": "tvc_article_position",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_article_position"
    }
   ],
   "fingerprint": "1481033201697",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/32?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/33",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "33",
   "name": "tvc_author_name",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_author_name"
    }
   ],
   "fingerprint": "1483537660079",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/33?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/34",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "34",
   "name": "tvc_tag_name",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_tag_name"
    }
   ],
   "fingerprint": "1481034079265",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/34?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/35",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "35",
   "name": "tvc_inifinite_position",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_inifinite_position"
    }
   ],
   "fingerprint": "1481034222572",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/35?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/37",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "37",
   "name": "tvc_pagination_action_item",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_pagination_action_item"
    }
   ],
   "fingerprint": "1481177901424",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/37?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/38",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "38",
   "name": "tvc_new_language",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_new_language"
    }
   ],
   "fingerprint": "1481177935215",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/38?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/39",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "39",
   "name": "tvc_404_error_js",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\treturn document.querySelector(\"body.error404\");\n}"
    }
   ],
   "fingerprint": "1481181153106",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/39?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/40",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "40",
   "name": "tvc_scroll_percent",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_scroll_percent"
    }
   ],
   "fingerprint": "1483533878708",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/40?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/41",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "41",
   "name": "scrollPath",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "scroll_path"
    }
   ],
   "fingerprint": "1481268626811",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/41?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/42",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "42",
   "name": "tvc_event_action",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_event_action"
    }
   ],
   "fingerprint": "1481189491767",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/42?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/46",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "46",
   "name": "tvc_event_label",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_event_label"
    }
   ],
   "fingerprint": "1481192090437",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/46?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/47",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "47",
   "name": "tvc_event_value",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_event_value"
    }
   ],
   "fingerprint": "1481192124066",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/47?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/48",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "48",
   "name": "tvc_get_language",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\tvar lang =  document.querySelector(\"ul.sectionLanguage_list li a.active\");\n  \tif (lang!=undefined || lang!=null)\n    \treturn lang.text.toLowerCase();\n  \treturn {{language}}.toLowerCase();\n}"
    }
   ],
   "fingerprint": "1484644715824",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/48?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/49",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "49",
   "name": "tvc_get_top_nav",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\tvar top_nav = document.querySelector(\".navigation \u003e li \u003e a.active\");\n  \tif (top_nav!=undefined || top_nav!=null)\n    \treturn top_nav.getAttribute(\"data-tvc_navigation_level_name\");\n  \treturn \"yourstory\";\n}"
    }
   ],
   "fingerprint": "1481206807380",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/49?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/50",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "50",
   "name": "tvc_get_authorName",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\tif ({{authorName}}!=\"not_set\")\n      return {{authorName}}.toLowerCase();\nreturn \"na\";\n}"
    }
   ],
   "fingerprint": "1484644665409",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/50?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/51",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "51",
   "name": "tvc_get_articleCategory",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\tif ({{tvc_articleCategory_filtered_cjs}}!=\"NA\") {\n    \treturn {{tvc_articleCategory_filtered_cjs}}.split(\"|\")[0].toLowerCase();\n\t}\n\treturn \"na\";\n}"
    }
   ],
   "fingerprint": "1502173557880",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/51?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/52",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "52",
   "name": "tvc_outbound_link",
   "type": "aev",
   "parameter": [
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "varType",
     "value": "URL"
    },
    {
     "type": "template",
     "key": "component",
     "value": "URL"
    }
   ],
   "fingerprint": "1481282455112",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/52?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/53",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "53",
   "name": "tvc_outbound_link_text",
   "type": "aev",
   "parameter": [
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "varType",
     "value": "TEXT"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    }
   ],
   "fingerprint": "1481282493213",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/53?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/54",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "54",
   "name": "tvc_article_age",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n  if({{publicationYear}} != \"NA\"){\n  var date1 = Date.parse({{publicationMonth}} + \" \" + {{publicationDate}} + \", \" + {{publicationYear}});\n  var date2 = new Date();\n  var timeDiff = Math.abs(date2.getTime() - date1);\n  return Math.ceil(timeDiff / (1000 * 3600 * 24)); \n  }\n  \n  return \"NA\";\n}"
    }
   ],
   "fingerprint": "1481355018337",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/54?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/55",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "55",
   "name": "tvc_get_email_id",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\tvar email_id = document.querySelector(\"#MERGE0\").value;\n  \tif(email_id != \"\"){\n    \treturn btoa(email_id);\n    }\n  return \"NA\";\n}"
    }
   ],
   "fingerprint": "1483341146098",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/55?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/57",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "57",
   "name": "tvc_page_url",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_page_url"
    }
   ],
   "fingerprint": "1483348228165",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/57?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/58",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "58",
   "name": "tvc_tags",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_tags"
    }
   ],
   "fingerprint": "1483348306159",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/58?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/59",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "59",
   "name": "tvc_article_length",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_article_length"
    }
   ],
   "fingerprint": "1483348327632",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/59?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/60",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "60",
   "name": "tvc_video_url",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_video_url"
    }
   ],
   "fingerprint": "1483348341063",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/60?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/61",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "61",
   "name": "tvc_publication_year",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_publication_year"
    }
   ],
   "fingerprint": "1483348637523",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/61?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/62",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "62",
   "name": "tvc_publication_month_year_is",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\tif({{tvc_publication_month}} == \"NA\"){\n    \treturn \"NA\";\n    }\n  return {{tvc_publication_month}} + \" \" + {{tvc_publication_year}};\n}"
    }
   ],
   "fingerprint": "1483348887855",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/62?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/63",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "63",
   "name": "tvc_publication_final_date_is",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\tif({{tvc_publication_date}} == \"NA\"){\n    \treturn \"NA\";\n    }\n  return {{tvc_publication_date}} + \" \" + {{tvc_publication_month}} + \" \" + {{tvc_publication_year}};\n}"
    }
   ],
   "fingerprint": "1483348957819",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/63?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/64",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "64",
   "name": "tvc_article_age_is",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n  if({{tvc_publication_year}} != \"NA\"){\n  var date1 = Date.parse({{tvc_publication_month}} + \" \" + {{tvc_publication_date}} + \", \" + {{tvc_publication_year}});\n  var date2 = new Date();\n  var timeDiff = Math.abs(date2.getTime() - date1);\n  return Math.ceil(timeDiff / (1000 * 3600 * 24)); \n  }\n  \n  return \"NA\";\n}"
    }
   ],
   "fingerprint": "1483349121002",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/64?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/65",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "65",
   "name": "tvc_publication_date",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_publication_date"
    }
   ],
   "fingerprint": "1483349207492",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/65?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/66",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "66",
   "name": "tvc_publication_month",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_publication_month"
    }
   ],
   "fingerprint": "1483349235130",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/66?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/67",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "67",
   "name": "tvc_author_name_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_author_name_scroll"
    }
   ],
   "fingerprint": "1483533900028",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/67?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/68",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "68",
   "name": "tvc_article_category_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_article_category_scroll"
    }
   ],
   "fingerprint": "1483533911467",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/68?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/69",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "69",
   "name": "tvc_article_name_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_article_name_scroll"
    }
   ],
   "fingerprint": "1483533920655",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/69?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/70",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "70",
   "name": "tvc_publication_year_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_publication_year_scroll"
    }
   ],
   "fingerprint": "1483533930671",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/70?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/71",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "71",
   "name": "tvc_publication_month_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_publication_month_scroll"
    }
   ],
   "fingerprint": "1483533942603",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/71?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/72",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "72",
   "name": "tvc_publication_date_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_publication_date_scroll"
    }
   ],
   "fingerprint": "1483533953469",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/72?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/73",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "73",
   "name": "tvc_language_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_language_scroll"
    }
   ],
   "fingerprint": "1483533962471",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/73?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/74",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "74",
   "name": "tvc_video_url_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_video_url_scroll"
    }
   ],
   "fingerprint": "1483533972027",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/74?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/75",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "75",
   "name": "tvc_article_length_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_article_length_scroll"
    }
   ],
   "fingerprint": "1483533981277",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/75?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/76",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "76",
   "name": "tvc_tags_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_tags_scroll"
    }
   ],
   "fingerprint": "1483533991765",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/76?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/77",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "77",
   "name": "tvc_inifinite_position_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_inifinite_position_scroll"
    }
   ],
   "fingerprint": "1483534939634",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/77?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/79",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "79",
   "name": "tvc_article_age_scroll",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_article_age_scroll"
    }
   ],
   "fingerprint": "1483535863950",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/79?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/81",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "81",
   "name": "tvc_clicked_element",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_clicked_element"
    }
   ],
   "fingerprint": "1484139350196",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/81?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/82",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "82",
   "name": "tvc_target_link",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_target_link"
    }
   ],
   "fingerprint": "1484139366575",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/82?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/83",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "83",
   "name": "NetSpeedCookie",
   "type": "k",
   "parameter": [
    {
     "type": "boolean",
     "key": "decodeCookie",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "netSpeed"
    }
   ],
   "fingerprint": "1484212568806",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/83?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/84",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "84",
   "name": "tvc_cg_active_menu_DV",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_cg_active_menu"
    }
   ],
   "fingerprint": "1484646578060",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/84?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/86",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "86",
   "name": "tvc_cg_active_menu",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function() {\n   return {{tvc_cg_active_menu_DV}}.toLowerCase();\n}"
    }
   ],
   "fingerprint": "1484646672494",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/86?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/87",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "87",
   "name": "tvc_share_links_container",
   "type": "d",
   "parameter": [
    {
     "type": "template",
     "key": "elementSelector",
     "value": "div.share-holder.s-tab \u003e ul"
    },
    {
     "type": "template",
     "key": "selectorType",
     "value": "CSS"
    }
   ],
   "fingerprint": "1485230504909",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/87?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/88",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "88",
   "name": "tvc_get_click_text",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\treturn {{Click Text}}.toLowerCase();\n}"
    }
   ],
   "fingerprint": "1485236359551",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/88?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/91",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "91",
   "name": "tvc_tag",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_tag"
    }
   ],
   "fingerprint": "1487229050318",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/91?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/92",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "92",
   "name": "tvc_timestamp",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_timestamp"
    }
   ],
   "fingerprint": "1487229078596",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/92?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/93",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "93",
   "name": "tvc_error",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_error"
    }
   ],
   "fingerprint": "1487229104253",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/93?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/94",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "94",
   "name": "tvc_page",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_page"
    }
   ],
   "fingerprint": "1487229132265",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/94?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/95",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "95",
   "name": "tvc_connection_type",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_connection_type"
    }
   ],
   "fingerprint": "1487230790531",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/95?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/96",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "96",
   "name": "tvc_connection_speed",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_connection_speed"
    }
   ],
   "fingerprint": "1487230825073",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/96?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/97",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "97",
   "name": "tvc_words_range",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "1"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_words_range"
    }
   ],
   "fingerprint": "1487329234619",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/97?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/98",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "98",
   "name": "tvc_words",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "1"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_words"
    }
   ],
   "fingerprint": "1487595723433",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/98?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/99",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "99",
   "name": "tvc_dr_pushed_ck",
   "type": "k",
   "parameter": [
    {
     "type": "boolean",
     "key": "decodeCookie",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_dr_pushed_ck"
    }
   ],
   "fingerprint": "1491370387937",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/99?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/100",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "100",
   "name": "tvc_dr_cj",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n  \tif({{Referrer}} != ''){\n\t\tif(document.referrer.indexOf(\"android-app\") \u003e= 0){\n\t\t\treturn {{tvc_dr_protocol}} + \"://\" + {{tvc_dr_hostname}};\n      \t}\n\t\t\n\t\tif(document.referrer.indexOf(\"yourstory.com\") == -1){\n        \treturn {{tvc_dr_protocol}} + \"://\" + {{tvc_dr_hostname}}; \n        }\n    }\n\treturn undefined;\n}"
    }
   ],
   "fingerprint": "1491394558907",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/100?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/101",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "101",
   "name": "tvc_dr_protocol",
   "type": "f",
   "parameter": [
    {
     "type": "template",
     "key": "component",
     "value": "PROTOCOL"
    }
   ],
   "fingerprint": "1491377078538",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/101?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/102",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "102",
   "name": "tvc_dr_hostname",
   "type": "f",
   "parameter": [
    {
     "type": "boolean",
     "key": "stripWww",
     "value": "false"
    },
    {
     "type": "template",
     "key": "component",
     "value": "HOST"
    }
   ],
   "fingerprint": "1491377102646",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/102?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/103",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "103",
   "name": "tvc_internalTeam",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "1"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "na"
    },
    {
     "type": "template",
     "key": "name",
     "value": "internalTeam"
    }
   ],
   "fingerprint": "1491479481670",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/103?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/104",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "104",
   "name": "tvc_internalTeam_lower_cj",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n\tif({{tvc_internalTeam}} !== undefined || {{tvc_internalTeam}} !== null){\n    \treturn {{tvc_internalTeam}}.toLowerCase();\n    }\n\treturn \"na\";\n}"
    }
   ],
   "fingerprint": "1491479794746",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/104?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/105",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "105",
   "name": "tvc_article_id",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "true"
    },
    {
     "type": "template",
     "key": "defaultValue",
     "value": "NA"
    },
    {
     "type": "template",
     "key": "name",
     "value": "articleId"
    }
   ],
   "fingerprint": "1499778435029",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/105?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/106",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "106",
   "name": "tvc_404_error_cjs",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n  \n    var element = document.querySelector(\".ys-widget-title\");\n    if (element && (element.innerText.indexOf(\"Oops\") != -1 || element.innerText.indexOf(\"does not exist\") != -1)) {\n        return true;\n    }\n  \n    element = document.querySelector(\".title-large\");\n    if (element && (element.innerText.indexOf(\"Oops\") != -1 || element.innerText.indexOd(\"does not exist\") != -1)) {\n        return true;\n    }\n  \n    return false;\n}"
    }
   ],
   "fingerprint": "1501839573031",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/106?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/107",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "107",
   "name": "tvc_articleCategory_filtered_cjs",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function() {\n  \tvar articleCategory = {{articleCategory}};\n  \tif (articleCategory.indexOf(\"Community\") != -1) {\n      \tarticleCategory = articleCategory.replace(\"Community\", \"\");\n    }\n\treturn articleCategory;\n}\n"
    }
   ],
   "fingerprint": "1502269529068",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/107?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/108",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "108",
   "name": "TVCaddEvent",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\nreturn function(el, evt, fn, tagname) {\n    tagname = tagname?tagname:'Not Passed';\n    el = (el.nodeName || (el == window)) ? [el] : el;\n    var elements_count = el.length ? el.length : 1;\n    for (var item = 0; item \u003c elements_count; item++) {\n        if (el[item].addEventListener) {\n            el[item].addEventListener(evt, function(evt) {\n                try {\n                    fn.call(this, evt);\n                } catch (e) {\n                    TVCtrackError(e.name, e.message, tagname + '_TVCaddEvent');\n                    //console.log(e);\n                }\n            }, false);\n        } else if (el[item].attachEvent) {\n            el[item].attachEvent('on' + evt, function(evt) {\n                try {\n                    fn.call(el[item], evt);\n                } catch (e) {\n                    TVCtrackError(e.name, e.message, tagname + '_TVCaddEvent');\n                    //console.log(e);\n                }\n            });\n        } else if (typeof el[item]['on' + evt] === 'undefined' || el[item]['on' + evt] === null) {\n            el[item]['on' + evt] = function(evt) {\n                try {\n                    fn.call(el[item], evt);\n                } catch (e) {\n                    TVCtrackError(e.name, e.message, tagname + '_TVCaddEvent');\n                    //console.log(e);                \n                }\n            };\n        }\n    }\n}\n}"
    }
   ],
   "fingerprint": "1503050547797",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/108?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/110",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "110",
   "name": "tvc_share_text_dlv",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_share_text_dlv"
    }
   ],
   "fingerprint": "1505124004253",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/110?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/111",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "111",
   "name": "tvc_UA_ID_cv",
   "type": "c",
   "parameter": [
    {
     "type": "template",
     "key": "value",
     "value": "UA-18111131-5"
    }
   ],
   "fingerprint": "1516632536891",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/111?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/112",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "112",
   "name": "tvc_closest_cjs",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\nreturn function (el, selector) {\n    while (el && el.nodeType === 1) {\n      if ({{tvc_matches_cjs}}(el, selector)) {\n          return el;\n      }\n      el = el.parentNode;\n    }\n    return null;\n};\n}"
    }
   ],
   "fingerprint": "1509433535888",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/112?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/113",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "113",
   "name": "tvc_matches_cjs",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\nreturn function (el, selector) {\n    var elements = (el.document || el.ownerDocument).querySelectorAll(selector);\n    var index = 0;\n    while (elements[index] && elements[index] !== el) {\n        ++index;\n    }\n    return Boolean(elements[index]);\n};\n}"
    }
   ],
   "fingerprint": "1509433491300",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/113?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/114",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "114",
   "name": "tvc_pagetype_cjs",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){  \n    if({{Page Path}}=='/herstory' || {{Page Path}}=='/' || {{Page Path}}=='/socialstory' || {{Page Path}}.match('/page/')){\n      return 'home page';\n    }else if({{Page Path}}.match(/^\\/\\d{4}\\/\\d{2}\\/\\w+/)||{{Page Path}}.match(/^\\/read\\/\\w+\\/\\w+/)){\n      return 'article page';\n    }else if({{Page Path}}.match('/category/')){\n      return 'category page';\n    }else if({{Page Path}}.match('/search')){\n      return 'search page';\n    }else{\n      return {{Page Path}}.split('/')[1] + \" page\";\n    }\n}"
    }
   ],
   "fingerprint": "1510131161097",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/114?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/115",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "115",
   "name": "tvc_language_cjs",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\t\n  if({{Page Hostname}}.split('.')[0]=='yourstory'){\n  \treturn 'english';\n  }else{\n  \treturn {{Page Hostname}}.split('.')[0];\n  }\n}"
    }
   ],
   "fingerprint": "1509446753588",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/115?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/116",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "116",
   "name": "tvc_track_error_cjs",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n  return function (errorname, errormsg, tagname) {\n      tagname = tagname?tagname:'Not Passed';\n      var sheet = '1VBBREc3lraOE1lnRma8CbdCABk2oa4Zt4YNxOgeD4Dc'; //Provide your google sheet endpoint here\n      var browser = {{tvc_browser_cjs}}();\n      var device = {{tvc_device_cjs}}();\n      var page_url = window.location.href;\n      var gtm_container_version = {{Container Version}}; //Use GTM Built-in variable\n      var endpoint = sheet +\n              '?Tag_Name=' + tagname +\n              '&Error_Name=' + errorname +\n              '&Error_Msg=' + errormsg +\n              '&Page=' + page_url +\n              '&Device=' + device +\n              '&Browser=' + browser +\n              '&Container_Version=' + gtm_container_version;\n      var tvc_error_pixel = new Image();\n      tvc_error_pixel.src = endpoint;\n  }\n}"
    }
   ],
   "fingerprint": "1510138327376",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/116?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/117",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "117",
   "name": "tvc_browser_cjs",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n    var ua = navigator.userAgent,tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident|ucbrowser(?=\\/))\\/?\\s*(\\d+)/i) || [];\n    if (/trident/i.test(M[1])) {\n      tem = /\\brv[ :]+(\\d+)/g.exec(ua) || [];\n      return 'IE ' + (tem[1] || '');\n    }\n    if (M[1] === 'Chrome') {\n      tem = ua.match(/\\bOPR\\/(\\d+)/)\n      if (tem != null) return 'Opera ' + tem[1];\n    }\n    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];\n    if ((tem = ua.match(/version\\/(\\d+)/i)) != null) M.splice(1, 1, tem[1]);\n    return M.join(' ');\n}"
    }
   ],
   "fingerprint": "1517312794623",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/117?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/118",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "118",
   "name": "tvc_device_cjs",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\n      var check = 'desktop';\n      (function(a) {\n          try {\n              if (/iPad|Tablet|KFAPWI/.test(a)) {\n                  check = 'tablet';\n              } else if (/(android|bb\\d+|meego).+mobile|silk|uc[\\s]?browser|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|zte\\-/i.test(a.substr(0, 4))) {\n                  check = 'mobile';\n              }\n          } catch (err) {}\n      })(navigator.userAgent);\n      return check;\n}"
    }
   ],
   "fingerprint": "1517312686990",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/118?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/119",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "119",
   "name": "tvc_popup_check_ck",
   "type": "k",
   "parameter": [
    {
     "type": "boolean",
     "key": "decodeCookie",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_popup_check"
    }
   ],
   "fingerprint": "1510827100876",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/119?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/120",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "120",
   "name": "tvc_survey_p",
   "type": "k",
   "parameter": [
    {
     "type": "boolean",
     "key": "decodeCookie",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_survey_p"
    }
   ],
   "fingerprint": "1512052828413",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/120?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/121",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "121",
   "name": "tvc_role_dl",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_role"
    }
   ],
   "fingerprint": "1512056134370",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/121?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/122",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "122",
   "name": "tvc_industry_dl",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_industry"
    }
   ],
   "fingerprint": "1512056171375",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/122?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/123",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "123",
   "name": "tvc_survey_q",
   "type": "k",
   "parameter": [
    {
     "type": "boolean",
     "key": "decodeCookie",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_survey_q"
    }
   ],
   "fingerprint": "1512121940688",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/123?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/124",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "124",
   "name": "tvc_page_visit",
   "type": "k",
   "parameter": [
    {
     "type": "boolean",
     "key": "decodeCookie",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_page_visit"
    }
   ],
   "fingerprint": "1512122274672",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/124?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/125",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "125",
   "name": "tvc_purpose_dlv",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_purpose"
    }
   ],
   "fingerprint": "1512134490999",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/125?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/126",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "126",
   "name": "tvc_task_dlv",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_task"
    }
   ],
   "fingerprint": "1512134483198",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/126?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/127",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "127",
   "name": "tvc_text_dlv",
   "type": "v",
   "parameter": [
    {
     "type": "integer",
     "key": "dataLayerVersion",
     "value": "2"
    },
    {
     "type": "boolean",
     "key": "setDefaultValue",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_text"
    }
   ],
   "fingerprint": "1512134469112",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/127?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/130",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "130",
   "name": "TVCsetCookie",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "\nfunction(){\nreturn function(cname, cvalue, domain, duration) {\n    var d = new Date();\n    var expires = '', calc_duration = 0, set_domain;\n    if(duration){\n        if (duration.minutes) {\n            calc_duration = duration.minutes * 60 * 1000;\n        } else if (duration.hours) {\n            calc_duration = duration.hours * 60 * 60 * 1000;\n        } else if (duration.days) {\n            calc_duration = duration.days * 24 * 60 * 60 * 1000;\n        } else if (duration.weeks) {\n            calc_duration = duration.weeks * 7 * 24 * 60 * 60 * 1000;\n        } else if (duration.months) {\n            calc_duration = duration.months * 30 * 24 * 60 * 60 * 1000;\n        } else if (duration.years) {\n            calc_duration = duration.years * 365 * 24 * 60 * 60 * 1000;\n        }\n        d.setTime(d.getTime() + calc_duration);\n        expires = \"expires=\" + d.toUTCString() + ';';\n    }\n    set_domain = domain ? domain : '.' + document.domain.replace('www.', '');\n    document.cookie = cname + \"=\" + cvalue + \";\" + expires + \"domain=\" + set_domain + \";path=/\";\n}\n}"
    }
   ],
   "fingerprint": "1516273911554",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/130?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/131",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "131",
   "name": "TVCgetCookie",
   "type": "jsm",
   "parameter": [
    {
     "type": "template",
     "key": "javascript",
     "value": "function(){\nreturn function (cname) {\n    var result;\n    return (result = new RegExp('(?:^|; )' + encodeURIComponent(cname) + '=([^;]*)').exec(document.cookie)) ? decodeURIComponent(result[1]) : null;\n}\n}"
    }
   ],
   "fingerprint": "1516273811157",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/131?apiLink=variable"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/variables/132",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "variableId": "132",
   "name": "tvc_adblocker_check_ck",
   "type": "k",
   "parameter": [
    {
     "type": "boolean",
     "key": "decodeCookie",
     "value": "false"
    },
    {
     "type": "template",
     "key": "name",
     "value": "tvc_adblocker_check"
    }
   ],
   "fingerprint": "1516284588753",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/variables/132?apiLink=variable"
  }
 ]
};
var tvc_arr=[];
for(var i=0;i<tvc_variables.variable.length;i++){
  tvc_arr.push(tvc_variables.variable[i].name);
  console.log(tvc_variables.variable[i].name);
}
console.log(tvc_arr);