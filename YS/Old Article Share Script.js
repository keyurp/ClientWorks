<script>
  var tvc_home_page_article_share = function(){
      if(window.location.href == 'https://yourstory.com/'){
        setTimeout(function(){
            var tvc_article_share = document.querySelectorAll('#root div._5fc72cc5 svg.social-icon,#root div._5fc72cc5 svg.visible-xm,#root div._5fc72cc5 svg.visible-md,#root div._5fc72cc5 svg.visible-lg,#articleShareList > div._632c3311 div svg');
          if(tvc_article_share.length > 0){
            var tvc_network_name;
            window.dataLayer = window.dataLayer || [];
            for (var i = 0; i < tvc_article_share.length; i++) {
                tvc_article_share[i].addEventListener('click', function() {
                    //document.querySelectorAll('svg.visible-xm,svg.visible-md,visible-lg')
                    if (this.getAttribute('class').match('visible')) {
                        tvc_network_name = 'pocket';
                    } else {
                        tvc_network_name = this.getAttribute('class').split('social-icon--')[1];
                    }

                    window.dataLayer.push({
                        'event': 'tvc_article_share',
                        'tvc_event_category': 'share',
                        'tvc_event_action': tvc_network_name,
                        'tvc_event_label': window.location.href
                    });
                });
            }
          }
        },5000);
      }else{
    setTimeout(function(){
          var tvc_other_article_share = document.querySelectorAll('#articleShareList > div._632c3311 svg,#banner_cta > div.col-lg-7.col-lg-offset-1.col-md-7.col-md-offset-1.col-sm-10.col-sm-offset-1.col-xs-10.col-xs-offset-1 span svg,#banner_cta > div._54306314.visible-md.visible-lg > div > div._1575ac37.row svg,#banner_cta > div.col-lg-7.col-lg-offset-1.col-md-7.col-md-offset-1.col-sm-10.col-sm-offset-1.col-xs-10.col-xs-offset-1 div.LazyLoad.is-visible div.row svg');
          if(tvc_other_article_share.length > 0){
              var tvc_network_name;
            window.dataLayer = window.dataLayer || [];
            for (var i = 0; i < tvc_other_article_share.length; i++) {
                tvc_other_article_share[i].addEventListener('click', function() {
                    //document.querySelectorAll('svg.visible-xm,svg.visible-md,visible-lg')
                    if (this.getAttribute('class').match('visible')) {
                        tvc_network_name = 'pocket';
                    } else {
                        tvc_network_name = this.getAttribute('class').split('social-icon--')[1];
                    }

                    window.dataLayer.push({
                        'event': 'tvc_article_share',
                        'tvc_event_category': 'share',
                        'tvc_event_action': tvc_network_name,
                        'tvc_event_label': window.location.href
                    });
                });
            }
          }
        },5000);
      }
    
  }

  tvc_home_page_article_share();
  
  var tvc_old_url=window.location.href;;
  setInterval(function() {
    if (window.location.href != tvc_old_url) {
      tvc_home_page_article_share();
      tvc_old_url = window.location.href;
    }
  }, 100);
</script>