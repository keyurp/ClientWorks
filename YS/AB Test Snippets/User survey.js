var TVCgetCookie = function(t) {
    var e;
    return (e = new RegExp("(?:^|; )" + encodeURIComponent(t) + "=([^;]*)").exec(document.cookie)) ? decodeURIComponent(e[1]) : null
};
if (void 0 == TVCgetCookie("tvc_survey_p")) {
    var tvcDiv = document.createElement("div");
    tvcDiv.innerHTML = '<div id="tvc_popup"> <div id="tvc_modal_body"> <div id="tvc_modal_close"><span style="float: right;font-size: 32px;font-weight: 700;line-height: 1;color: #646363;opacity: .6;margin-top: -1%;cursor: pointer;">×</span> </div> <div style="padding: 2px 33px;"> <div style="text-align:center;"> <div style="margin-bottom: 4px;line-height: 1.7;"> <table style="border-color: white!important;border-style: hidden;"> <tbody style="text-align: center;"><tr class="tvc_r1"> <td colspan="2" style=""> <h3 style="text-align: left;margin-bottom: 17px;font-weight: 500;font-size: 26px;line-height: 1.4;">Thanks for stopping by! To serve you even better with more relatable content, please help us by filling this small survey.</h3></td> </tr> <tr style="text-align: center;border-style: hidden!important;"> <td style="text-align: left;width: 50%;border-style: hidden!important;"> <img src="Image1.png" style="vertical-align: text-bottom;""> Industry Domain</td> <td style="text-align: left;border-style: hidden!important;" id="tvc_role_img_web"><img src="Image2.png" style="vertical-align: sub;""> Role</td> </tr> <tr style="border-style: hidden!important;"> <td style="text-align: left;border-style: hidden!important;"> <select class="tvc_modal_select" id="tvc_industry"> </select></td> <td id="tvc_role_img_mob" style="text-align: left;border-style: hidden!important;display: none"><img src="Image2.png" style="vertical-align: sub;""> Role</td> <td style="text-align: left;border-style:hidden!important"><select class="tvc_modal_select" id="tvc_role"></select></td> </tr> <tr class="tvc_r1"> <td colspan="2"><input type="button" value="Submit" id="tvc_modal_button"></td> </tr> </tbody></table> <span style="color: white;font-size: 15px;" id="tvc_error">*Please select both the options</span> </div> </div> </div> </div> </div>', document.body.appendChild(tvcDiv), document.getElementById("tvc_industry").innerHTML = ' <option value="select" selected="" disabled="disabled">Select Industry Domain</option> <option value="Advertising &amp; Public Relations">Advertising &amp; Public Relations</option> <option value="Arts, Entertainment and Recreation">Arts, Entertainment, and Recreation</option> <option value="Education">Education</option> <option value="Engineering">Engineering</option> <option value="Finance &amp; Insurance">Finance &amp; Insurance</option> <option value="Government and Public Administration">Government and Public Administration</option> <option value="Health Care &amp; Medicines">Health Care &amp; Medicines</option> <option value="Hospitality, Food &amp; Beverages">Hospitality, Food &amp; Beverages</option> <option value="Human Resources">Human Resources</option> <option value="Information Technology">Information Technology</option> <option value="Legal Services">Legal Services</option> <option value="Manufacturing">Manufacturing</option> <option value="Military">Military</option> <option value="Non-Profit Organizations">Non-Profit Organizations</option> <option value="Publishing">Publishing</option><option value="Research/Sciences">Research/Sciences</option> <option value="Retail">Retail</option> <option value="Telecommunications">Telecommunications</option> <option value="Travel">Travel</option> <option value="Other">Other</option>', document.getElementById("tvc_role").innerHTML = ' <option value="select" selected="" disabled="disabled">Select Role</option> <option value="Administrative Staff">Administrative Staff</option> <option value="Analyst">Analyst</option> <option value="Architect">Architect</option> <option value="Consultant">Consultant</option> <option value="CXO/EVP">CXO/EVP</option> <option value="Developer">Developer</option> <option value="Educator">Educator</option> <option value="Elected Official">Elected Official</option> <option value="Individual Contributor">Individual Contributor</option> <option value="Marketing">Marketing</option> <option value="Professional Staff">Professional Staff</option> <option value="Researcher">Researcher</option> <option value="Sales">Sales</option> <option value="Self-employed/Partner">Self-employed/Partner</option> <option value="Senior Director/Director">Senior Director/Director</option> <option value="Senior Manager/Manager">Senior Manager/Manager</option> <option value="Student">Student</option> <option value="Support Staff">Support Staff</option> <option value="SVP/VP">SVP/VP</option> <option value="Other">Other</option>', tvc_setCookie("tvc_survey_p", "1", "730"), dataLayer.push({
        event: "tvc_survey",
        tvc_event_action: "survey_displayed",
        tvc_event_label: "-"
    }), document.getElementById("tvc_popup").style.display = "block", document.getElementById("tvc_modal_button").addEventListener("click", function() {
        var t = document.getElementById("tvc_role").value,
            e = document.getElementById("tvc_industry").value;
        "select" == t || "select" == e ? document.getElementById("tvc_error").style.color = "red" : (dataLayer.push({
            event: "tvc_survey",
            tvc_event_action: "survey_submitted",
            tvc_event_label: e + "|" + t,
            tvc_industry: e,
            tvc_role: t
        }), document.getElementById("tvc_popup").style.display = "none")
    }), document.querySelector("#tvc_modal_close span").addEventListener("click", function() {
        dataLayer.push({
            event: "tvc_survey",
            tvc_event_action: "survey_closed",
            tvc_event_label: "-"
        }), document.getElementById("tvc_popup").style.display = "none"
    })
} else {
    var tvcQDiv = document.createElement("div");
    tvcQDiv.innerHTML = '<div id="tvc_popup2"><div id="tvc_modal_body2"><div id="tvc_modal_close2"><span style="float: right;font-size: 32px;font-weight: 700;line-height: 1;color:#646363;opacity: .6;margin-top: -1%;cursor: pointer;">×</span> </div> <div style="padding: 2px 33px;"><div style="text-align:center;"><div style="margin-bottom: 4px;line-height: 1.7;"> <table style="border-color: white!important;border-style: hidden;width: 100%;font-weight: 500;font-size: 18px;text-align: left;"><tr><td colspan="3"><div style="margin-bottom: 7px;">What is the purpose of your visit to our website today?</div> </td> </tr><tr><td class="tvc_input_type"><input type="checkbox" name="purpose"> Gaining Knowledge on a Specific Topic </td> <td style="text-align: left;" class="tvc_input_type"> <input type="checkbox" name="purpose"> Updates on the Business World </td> <td style="text-align: left;visibility: hidden;" class="tvc_inactive"> purpose </td> </tr> <tr> <td class="tvc_input_type"> <input type="checkbox" name="purpose"> Industry & Market Research </td> <td style="text-align: left;" class="tvc_input_type"> <input type="checkbox" name="purpose"> Reading for Leisure </td> <td style="text-align: left;visibility: hidden;" class="tvc_inactive"> purpose</td></tr><tr><td class="tvc_input_type"><input type="checkbox" name="purpose"> Motivational Stories </td> <td style="text-align: left;" class="tvc_input_type"> <input type="checkbox" name="purpose"> Articles for Social Awareness </td> <td style="text-align: left;visibility: hidden;" class="tvc_inactive"> purpose </td></tr><tr><td colspan="3" style="font-size: 15px;"> <div id="tvc_check_error" style="color: white">*Please select a purpose</div> </td></tr><tr><td colspan="3"> </br> <div style="margin-bottom: 7px;">Were you able to complete your task today?</div> </td> </tr> <tr> <td class="tvc_input_type"> <input type="radio" name="task" value="Yes" checked> Yes </td> <td style="text-align: left;" class="tvc_input_type"> <input type="radio" name="task" value="No"> No </td> <td style="text-align: left;visibility: hidden;" class="tvc_inactive"> purpose </td> </tr> <tr> <td colspan="3"> </br> <div>If you were not able to complete your task today, why not?</div> </td> </tr> <tr> <td colspan="3"> <textarea name="reason" placeholder=" Write here..." style="width:96%;height: 60px;margin-top: 5px;border-top-color: black;border-left-color: black;border: 1px solid #575757!important;font-size: 15px!important;font-weight: 300;" disabled></textarea> </td> </tr> <tr> <td colspan="3" style="text-align: center;"><input type="button" value="Submit" id="tvc_modal_button2"></td></tr></table></div></div></div></div></div>', document.body.appendChild(tvcQDiv), dataLayer.push({
        event: "tvc_site_visit_form",
        tvc_event_action: "form_displayed",
        tvc_event_label: document.location.href
    }), tvc_setCookie("tvc_survey_q", "1", "730");
    for (var tvcinputRadio = document.querySelectorAll("input[name=task]"), i = 0; i < tvcinputRadio.length; i++) tvcinputRadio[i].addEventListener("click", function() {
        var t = document.querySelector("input[name=task]:checked").value;
        "No" == t ? document.querySelector("#tvc_popup2 textarea").removeAttribute("disabled") : document.querySelector("#tvc_popup2 textarea").setAttribute("disabled", "")
    });
    document.querySelector("#tvc_modal_close2 span").addEventListener("click", function() {
        dataLayer.push({
            event: "tvc_site_visit_form",
            tvc_event_action: "form_closed",
            tvc_event_label: document.location.href
        }), document.getElementById("tvc_popup2").style.display = "none"
    }), document.querySelector("#tvc_modal_button2").addEventListener("click", function() {
        for (var t = null, e = document.querySelectorAll('.tvc_input_type input[type="checkbox"]'), o = 0, i = 0; e[i]; ++i) e[i].checked && (0 == o ? (o++, t = e[i].parentNode.innerText) : t = t + "|" + e[i].parentNode.innerText);
        var n = document.querySelector("input[name=task]:checked").value,
            l = document.querySelector("#tvc_popup2 textarea").value;
        0 == o ? document.getElementById("tvc_check_error").style.color = "red" : (document.getElementById("tvc_popup2").style.display = "none", "Yes" == n ? dataLayer.push({
            event: "tvc_site_visit_form",
            tvc_event_action: "form_submitted",
            tvc_event_label: document.location.href,
            tvc_purpose: t,
            tvc_task: n
        }) : dataLayer.push({
            event: "tvc_site_visit_form",
            tvc_event_action: "form_submitted",
            tvc_event_label: document.location.href,
            tvc_purpose: t,
            tvc_task: n,
            tvc_text: l
        }))
    })
}