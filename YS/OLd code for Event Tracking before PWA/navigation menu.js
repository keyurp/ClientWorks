<script>
try{
	var tvc_nav_both = document.querySelectorAll('body > section.level-one div.trendinglisting ul > li > a,#secnav > div ul > li > a');
  	var tvc_nav_position;
  	
  	if({{Page Hostname}}=='yourstory.com'){
      try{
        var tvc_logo = document.querySelector('#YSHeader > div > nav > div > div.navbar-header > a > img');
        if(tvc_logo != null){
          tvc_logo.addEventListener('click',function(){
              dataLayer.push({
                  'event':'navigation_menu_click',
                    'tvc_event_category':'web - {{tvc_pagetype_cjs}}',
                    'tvc_event_action':'navigation - top menu clicked',
                    'tvc_navigation_level_name': 'yourstory_logo_click',
                    'tvc_navigation_position':'top',
                    'tvc_language':{{tvc_language_cjs}}
              });
          });
        }

        if(document.querySelector('#nav div.navbar-right.pull-right > ul > li > a') != null){
          document.querySelector('#nav div.navbar-right.pull-right > ul > li > a').addEventListener('click',function(){
              dataLayer.push({
                  'event':'navigation_menu_click',
                    'tvc_event_category':'web - {{tvc_pagetype_cjs}}',
                    'tvc_event_action':'navigation - top menu clicked',
                    'tvc_navigation_level_name': 'search_click',
                    'tvc_navigation_position':'top',
                    'tvc_language':{{tvc_language_cjs}}
              });
          });
        }
      }catch(e){
        tvc_track_error(e.message, {{Page Path}},'1 - tvc_navigation_menu_click_all_pages_d_inject');
      }
	}else{
    	var tvc_subdomain_social=document.querySelectorAll('body > header > section.sectionLanguage > div > ul.socialLinks.socialLinks-red.fr > li > a');
        if(tvc_subdomain_social.length > 0){
          for(var i=0;i<tvc_subdomain_social.length;i++){
              try{
                tvc_subdomain_social[i].addEventListener('click',function(){
                  dataLayer.push({
                    'event':'navigation_menu_click',
                    'tvc_event_category':'web - {{tvc_pagetype_cjs}}',
                    'tvc_event_action':'navigation - top menu clicked',
                    'tvc_navigation_level_name':this.querySelector('i').className.split('-')[1],
                    'tvc_navigation_position':'top',
                    'tvc_language':'{{tvc_language_cjs}}'
                  });
                });
              }catch(e){
                tvc_track_error(e.message, {{Page Path}},'2 - tvc_navigation_menu_click_all_pages_d_inject');
              }
          }
      	}
    }
  
  	if(tvc_nav_both.length > 0){
      for(var i=0;i<tvc_nav_both.length;i++){
        try{
          tvc_nav_both[i].addEventListener('click',function(){

              if({{tvc_closest_cjs}}(this,'.level-one')){
                  tvc_nav_position='top';
              }else{
                  tvc_nav_position='middle';
              }

              var tvc_label_name=this.innerText.split('#');

            dataLayer.push({
                'event':'navigation_menu_click',
                'tvc_event_category':'web - {{tvc_pagetype_cjs}}',
                'tvc_event_action':'navigation - '+ tvc_nav_position +' - menu clicked',
                'tvc_navigation_position': tvc_nav_position,
                'tvc_navigation_level_name': this.innerText.match('#') ? tvc_label_name[1].toLowerCase() : tvc_label_name[0].toLowerCase(),
                'tvc_language':{{tvc_language_cjs}}
            });

          });
        }catch(e){
          tvc_track_error(e.message, {{Page Path}},'3 - tvc_navigation_menu_click_all_pages_d_inject');
        }
      }
	}
}catch(e){
  tvc_track_error(e.message, {{Page Path}},'tvc_navigation_menu_click_all_pages_d_inject');
}
</script>