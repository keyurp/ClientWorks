<style type="text/css">
@media screen and (max-width:1024px) and (min-width: 300px) {
    #tvc_modal_body2 {
        width: 85%!important;
    }
    #tvc_popup2 td {
        display: block;
        width: 100%!important;
    }
    #tvc_role_img_mob2 {
        display: block!important;
        margin-top: 20px;
    }
    #tvc_role_img_web2 {
        display: none!important;
    }
    #tvc_modal_close2 span {
        font-size: 15px!important;
    }
    #tvc_popup2 {
        padding-top: 15%!important;
    }
    #tvc_popup2 table {
        font-size: 15px!important;
    }
    .tvc_input_type {
        font-size: 15px!important;
    }
    .tvc_inactive {
        display: none!important;
    }
}

#tvc_popup2 {
    position: fixed;
    z-index: 99999;
    padding-top: 7%;
    padding-bottom: 30px;
    left: 0;
    top: 0px;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgba(0, 0, 0, 0.4);
    font-weight: 500;
}

#tvc_modal_body2 {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: .4s;
    animation-name: animatetop;
    animation-duration: .4s;
    width: 50%;
}

#tvc_modal_close2 {
    color: white;
    padding: 14px 16px;
    font-size: 19px;
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    "> <span style="float: right;
    font-size: 32px;
    font-weight: 700;
    line-height: 1;
    color: #646363;
    opacity: .6;
    margin-top: -1%;
}

.tvc_modal_select {
    border: 1px solid #575757!important;
    border-radius: 4px;
    width: 90%;
    margin-top: 4px;
    height: 40px;
    padding: 0 8px;
    background-color: white;
    text-align: left;
}

#tvc_modal_button2 {
    background-color: #e5002d;
    color: white;
    font-size: 15px;
    line-height: 2.8;
    padding: 0 1.5em;
    text-transform: uppercase;
    overflow: hidden;
    display: inline-block;
    vertical-align: top;
    text-align: center;
    white-space: nowrap;
    position: relative;
    border: none;
    z-index: 0;
    cursor: pointer;
    transition: background-color 0.3s, box-shadow 0.3s, opacity 0.3s, color 0.3s;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    margin-top: 17px;
}

.tvc_input_type {
    font-size: 15px;
  font-weight:300;
}

#tvc_popup2 td {
    border-style: hidden!important;
    padding: 0!important;
}

#tvc_popup2 tr {
    border-style: hidden!important;
}
</style>
<script>
	  try{
	if({{tvc_survey_q}} == undefined){
		if({{tvc_page_visit}} == undefined){
			tvc_setCookie('tvc_page_visit',1,'1');
			console.log('first page');
		}else{
			function tvcCounter(){
					var tvcCurrentCount = parseInt({{tvc_page_visit}});
					var tvcUpdateCount = tvcCurrentCount+1;
					console.log('count is'+tvcUpdateCount);
					tvc_setCookie('tvc_page_visit',tvcUpdateCount,'1');


					if(parseInt({{tvc_page_visit}}) > 2 && {{tvc_survey_p}} != undefined){
						setTimeout(function(){ 
							dataLayer.push({
							'event':'tvc_site_visit_form',
							'tvc_event_action':'form_displayed',
							'tvc_event_label':document.location.href
						});
							tvc_setCookie('tvc_survey_q','1','15');
							var tvcQDiv = document.createElement("div"); 
						tvcQDiv.innerHTML = '<div id="tvc_popup2"><div id="tvc_modal_body2"><div id="tvc_modal_close2"><span style="float: right;font-size: 32px;font-weight: 700;line-height: 1;color:#646363;opacity: .6;margin-top: -1%;cursor: pointer;">×</span> </div> <div style="padding: 2px 33px;"><div style="text-align:center;"><div style="margin-bottom: 4px;line-height: 1.7;"> <table style="border-color: white!important;border-style: hidden;width: 100%;font-weight: 500;font-size: 18px;text-align: left;"><tr><td colspan="3"><div style="margin-bottom: 7px;">What is the purpose of your visit to our website today?</div> </td> </tr><tr><td class="tvc_input_type"><input type="checkbox" name="purpose"> Gaining Knowledge on a Specific Topic </td> <td style="text-align: left;" class="tvc_input_type"> <input type="checkbox" name="purpose"> Updates on the Business World </td> <td style="text-align: left;visibility: hidden;" class="tvc_inactive"> purpose </td> </tr> <tr> <td class="tvc_input_type"> <input type="checkbox" name="purpose"> Industry & Market Research </td> <td style="text-align: left;" class="tvc_input_type"> <input type="checkbox" name="purpose"> Reading for Leisure </td> <td style="text-align: left;visibility: hidden;" class="tvc_inactive"> purpose</td></tr><tr><td class="tvc_input_type"><input type="checkbox" name="purpose"> Motivational Stories </td> <td style="text-align: left;" class="tvc_input_type"> <input type="checkbox" name="purpose"> Articles for Social Awareness </td> <td style="text-align: left;visibility: hidden;" class="tvc_inactive"> purpose </td></tr><tr><td colspan="3" style="font-size: 15px;"> <div id="tvc_check_error" style="color: white">*Please select a purpose</div> </td></tr><tr><td colspan="3"> </br> <div style="margin-bottom: 7px;">Were you able to complete your task today?</div> </td> </tr> <tr> <td class="tvc_input_type"> <input type="radio" name="task" value="Yes" checked> Yes </td> <td style="text-align: left;" class="tvc_input_type"> <input type="radio" name="task" value="No"> No </td> <td style="text-align: left;visibility: hidden;" class="tvc_inactive"> purpose </td> </tr> <tr> <td colspan="3"> </br> <div>If you were not able to complete your task today, why not?</div> </td> </tr> <tr> <td colspan="3"> <textarea name="reason" placeholder=" Write here..." style="width:96%;height: 60px;margin-top: 5px;border-top-color: black;border-left-color: black;border: 1px solid #575757!important;font-size: 15px!important;font-weight: 300;" disabled></textarea> </td> </tr> <tr> <td colspan="3" style="text-align: center;"><input type="button" value="Submit" id="tvc_modal_button2"></td></tr></table></div></div></div></div></div>';
						document.body.appendChild(tvcQDiv);
			             var tvcinputRadio = document.querySelectorAll('input[name=task]');
			           for(var i=0;i<tvcinputRadio.length;i++){
							tvcinputRadio[i].addEventListener("click", function(){
								var tvcRadio = document.querySelector('input[name=task]:checked').value;
								if(tvcRadio == 'No'){
									document.querySelector('#tvc_popup2 textarea').removeAttribute('disabled');
								}else{
                                	document.querySelector('#tvc_popup2 textarea').setAttribute('disabled','');
                                }
							});
			           }
			                    document.querySelector('#tvc_modal_close2 span').addEventListener("click", function(){
				   		 	dataLayer.push({
									'event':'tvc_site_visit_form',
									'tvc_event_action':'form_closed',
									'tvc_event_label':document.location.href
								});
				   		 	document.getElementById('tvc_popup2').style.display = "none";
						});
	            document.querySelector('#tvc_modal_button2').addEventListener("click", function(){
				 	var checkedValue = null; 
					var inputElements = document.querySelectorAll('.tvc_input_type input[type="checkbox"]');
					var tvcTempCount= 0;
					for(var i=0; inputElements[i]; ++i){
					      if(inputElements[i].checked){
					      		if(tvcTempCount == 0){
					      			tvcTempCount++;
					      			checkedValue = inputElements[i].parentNode.innerText;
					      		}else{
					      			checkedValue = checkedValue+'|'+inputElements[i].parentNode.innerText;
					      		}
					       }
					}
					var tvcRadio = document.querySelector('input[name=task]:checked').value;
					var tvcText = document.querySelector('#tvc_popup2 textarea').value;
					if(tvcTempCount==0){
						console.log('validation fail');
                      document.getElementById('tvc_check_error').style.color ="red";
					}else{
                      document.getElementById('tvc_popup2').style.display = "none";
						if(tvcRadio == 'Yes'){
							dataLayer.push({
								'event':'tvc_site_visit_form',
								'tvc_event_action':'form_submitted',
								'tvc_event_label':document.location.href,
								'tvc_purpose':checkedValue,
								'tvc_task':tvcRadio
							});
						}else{
							dataLayer.push({
								'event':'tvc_site_visit_form',
								'tvc_event_action':'form_submitted',
								'tvc_event_label':document.location.href,
								'tvc_purpose':checkedValue,
								'tvc_task':tvcRadio,
								'tvc_text':tvcText
							});
						}
					}
				});
						 }, 20000);  
			}
			}
			if (window.performance) {
				if (performance.navigation.type == 1) {
	    			console.info( "This page is reloaded" );
	  			} else {
	    			tvcCounter();
	  			}
			}else{
				tvcCounter();
			}
		}
	}
	  }catch(e){
	  tvc_track_error(e.message, {{Page Path}},'tvc_survey_deck');
	}
	</script>	
