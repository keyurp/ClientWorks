<script>
    if ({{Page Hostname}} == 'yourstory.com') {
        try {
            var tvc_logo = document.querySelector('#YSHeader > div > nav > div > div.navbar-header > a > img');
            if (tvc_logo != null) {
                tvc_logo.addEventListener('click', function() {
                    dataLayer.push({
                        'event': 'navigation_menu_click',
                        'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',
                        'tvc_event_action': 'navigation - top menu clicked',
                        'tvc_navigation_level_name': 'yourstory_logo_click',
                        'tvc_navigation_position': 'top',
                        'tvc_language': {{tvc_language_cjs}}
                    });
                });
            }

            if (document.querySelector('#morphsearch') != null) {
                document.querySelector('#morphsearch').addEventListener('click', function() {
                    dataLayer.push({
                        'event': 'navigation_menu_click',
                        'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',
                        'tvc_event_action': 'navigation - top menu clicked',
                        'tvc_navigation_level_name': 'search_click',
                        'tvc_navigation_position': 'top',
                        'tvc_language': {{tvc_language_cjs}}
                    });
                });
            }

            if (document.querySelector('#YSHeader > div > span > div.visible-lg.visible-md > div > svg') != null) {
                document.querySelector('#YSHeader > div > span > div.visible-lg.visible-md > div > svg').addEventListener('click', function() {
                    dataLayer.push({
                        'event': 'navigation_menu_click',
                        'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',
                        'tvc_event_action': 'navigation - top menu clicked',
                        'tvc_navigation_level_name': 'notification_click',
                        'tvc_navigation_position': 'top',
                        'tvc_language': {{tvc_language_cjs}}
                    });
                });
            }
            var tvc_navigation = document.querySelectorAll('#YSHeader > div > nav > div > div.navbar-collapse.collapse > ul > li > a');
            if (tvc_navigation.length > 0) {
                tvc_navigation[i].addEventListener('click', function() {
                    dataLayer.push({
                        'event': 'navigation_menu_click',
                        'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',
                        'tvc_event_action': 'navigation - top menu clicked',
                        'tvc_navigation_level_name': this.innerText.toLowerCase(),
                        'tvc_navigation_position': 'top',
                        'tvc_language': {{tvc_language_cjs}}
                    });
                });
            }
        } catch (e) {
            tvc_track_error(e.message, {{Page Path}}, '1 - tvc_navigation_menu_click_all_pages_d_inject');
        }
    } else {
        var tvc_logo = document.querySelector('body > header > section.sectionNavigation > div > div.container > div.fl.blockLeft > div > a');
        if (tvc_logo != null) {
        	try{
	            tvc_logo.addEventListener('click', function() {
	                dataLayer.push({
	                    'event': 'navigation_menu_click',
	                    'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',
	                    'tvc_event_action': 'navigation - top menu clicked',
	                    'tvc_navigation_level_name': 'yourstory_logo_click',
	                    'tvc_navigation_position': 'top',
	                    'tvc_language': {{tvc_language_cjs}}
	                });
	            });
        	}catch(e) {
        		tvc_track_error(e.message, {{Page Path}}, '2 - tvc_navigation_menu_click_all_pages_d_inject - subdomain_logo');
            }
    	}

        var tvc_subdomain = document.querySelectorAll('body > header > section.sectionLanguage > div > ul.fl.sectionLanguage_list.phn-hide > li > a,body > header > section.sectionNavigation > div > div.container > div.fl.blockLeft > div > div > div > ul > li > a');
        if (tvc_subdomain.length > 0) {
            for (var i = 0; i < tvc_subdomain.length; i++) {
                try {
                    tvc_subdomain[i].addEventListener('click', function() {
                        dataLayer.push({
                            'event': 'navigation_menu_click',
                            'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',
                            'tvc_event_action': 'navigation - top menu clicked',
                            'tvc_navigation_level_name': this.innerText.toLowerCase(),
                            'tvc_navigation_position': 'top',
                            'tvc_language': {{tvc_language_cjs}}
                        });
                    });
                }catch(e) {
                    tvc_track_error(e.message, {{Page Path}}, '2 - tvc_navigation_menu_click_all_pages_d_inject - subdomain');
                }
            }
        }

        var tvc_subdomain_social = document.querySelectorAll('body > header > section.sectionLanguage > div > ul.socialLinks.socialLinks-red.fr > li > a');
        if (tvc_subdomain_social.length > 0) {
            for (var i = 0; i < tvc_subdomain_social.length; i++) {
                try {
                    tvc_subdomain_social[i].addEventListener('click', function() {
                        dataLayer.push({
                            'event': 'navigation_menu_click',
                            'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',
                            'tvc_event_action': 'navigation - top menu clicked',
                            'tvc_navigation_level_name': this.querySelector('i').className.split('-')[1],
                            'tvc_navigation_position': 'top',
                            'tvc_language': '{{tvc_language_cjs}}'
                        });
                    });
                } catch (e) {
                    tvc_track_error(e.message, {{Page Path}}, '2 - tvc_navigation_menu_click_all_pages_d_inject - subdomain_social');
                }
            }
        }
    } 
</script>