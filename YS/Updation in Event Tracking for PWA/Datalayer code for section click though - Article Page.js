//Article Page

<script>

	var tvc_read_next = document.querySelectorAll('#banner_cta > div._54306314.visible-md.visible-lg > div._9a5c56de div._29663c92.row a');
	if(tvc_read_next.length > 0){
		var tvc_fromPage=window.location.pathname;
		for(var i=0;i<tvc_read_next.length;i++){
			tvc_read_next[i].addEventListener('click',function(){
				var tvc_toPage=this.href.split('https://yourstory.com')[1];
				dataLayer.push({
					'event':'tvc_section_click_through',
					'tvc_event_category': 'traversal_read_next',
					'tvc_event_action': tvc_toPage,
					'tvc_event_label': tvc_fromPage,
					'tvc_event_value': 1
				});
			});
		}
	}

	var tvc_you_might_also_like = document.querySelectorAll('#banner_cta > div.col-lg-7.col-lg-offset-1.col-md-7.col-md-offset-1.col-sm-10.col-sm-offset-1.col-xs-10.col-xs-offset-1 div._24314fa1 a,#banner_cta > div.col-lg-7.col-lg-offset-1.col-md-7.col-md-offset-1.col-sm-10.col-sm-offset-1.col-xs-10.col-xs-offset-1 div.LazyLoad.is-visible > div > div > div > a');
	if(tvc_you_might_also_like.length > 0){
		var tvc_fromPage=window.location.pathname;
		for(var i=0;i<tvc_you_might_also_like.length;i++){
			tvc_you_might_also_like[i].addEventListener('click',function(){
				var tvc_toPage=this.href.split('https://yourstory.com')[1];
				dataLayer.push({
					'event':'tvc_section_click_through',
					'tvc_event_category': 'traversal_read_more',
					'tvc_event_action': tvc_toPage,
					'tvc_event_label': tvc_fromPage,
					'tvc_event_value': 1
				});
			});
		}
	}

	var tvc_article_trending = document.querySelectorAll('#article-trending a');
	if(tvc_article_trending.length > 0){
		var tvc_fromPage=window.location.pathname;
		for(var i=0;i<tvc_article_trending.length;i++){
			tvc_article_trending[i].addEventListener('click',function(){
				var tvc_toPage=this.href.split('https://yourstory.com')[1];
				dataLayer.push({
					'event':'tvc_section_click_through',
					'tvc_event_category': 'traversal_trending',
					'tvc_event_action': tvc_toPage,
					'tvc_event_label': tvc_fromPage,
					'tvc_event_value': 1
				});
			});
		}
	}
</script>