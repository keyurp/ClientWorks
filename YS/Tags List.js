var tvc_tags = {
 "tag": [
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/1",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "1",
   "name": "GA PageViews",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useHashAutoLink",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_PAGEVIEW"
    },
    {
     "type": "list",
     "key": "contentGroup",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "1"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_get_language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_cg_active_menu}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "3"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_get_articleCategory}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "4"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_get_authorName}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "decorateFormsAutoLink",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "1"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{authorName}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_articleCategory_filtered_cjs}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{articleTitle}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "6"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{publicationYear}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "7"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_month_year}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "8"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_final_date}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "10"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{videoUrl}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "9"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{articleLength}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "14"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tags}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "15"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_age}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "18"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_dr_cj}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "19"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_internalTeam_lower_cj}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "20"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_id}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1502173417954",
   "firingTriggerId": [
    "2147479553"
   ],
   "setupTag": [
    {
     "tagName": "tvc_common_functions"
    }
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/1?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/4",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "4",
   "name": "GA Category View",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "CATEGORY_VIEW"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{Click Text}}"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1477400745872",
   "firingTriggerId": [
    "5"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/4?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/5",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "5",
   "name": "Search Event",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "SEARCH"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "VIEW"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{Search Query}}"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1461407230624",
   "firingTriggerId": [
    "6"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/5?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/6",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "6",
   "name": "GA Adjusted Bounce Rate",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "INTERACTION"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "BASIC"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "Basic Interaction"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1477486577809",
   "firingTriggerId": [
    "7"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/6?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/7",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "7",
   "name": "Infinite Scroll Page Views",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "/infinitescroll"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "useHashAutoLink",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_PAGEVIEW"
    },
    {
     "type": "list",
     "key": "contentGroup",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "1"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_get_language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_get_top_nav}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "3"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_get_articleCategory}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "4"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_get_authorName}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "decorateFormsAutoLink",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1483346445220",
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/7?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/8",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "8",
   "name": "Campaign Page Views",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useHashAutoLink",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_PAGEVIEW"
    },
    {
     "type": "boolean",
     "key": "decorateFormsAutoLink",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaCampaignPageviewsId}}"
    }
   ],
   "fingerprint": "1477489364164",
   "firingTriggerId": [
    "12"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/8?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/10",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "10",
   "name": "SectionTopPostLinkClick",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "SECTION_TOP_POST"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "LINK_CLICK"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1478769749662",
   "firingTriggerId": [
    "19"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/10?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/11",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "11",
   "name": "tvc_clientid_implementation",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n  var tvc_domain = '.yourstory.com'; //domain name\n  var tvc_UAID = 'UA-18111131-5'; //add property ID here\n\n  function tvc_setCookie(d,t_data,exdays) {\n          var tvc_time = new Date();\n          tvc_time.setTime(tvc_time.getTime() + (exdays * 24 * 60 * 60 * 1000));\n          var c = escape(t_data) + (\"; expires=\" + tvc_time.toGMTString()) + \"; path=/\";\n          document.cookie = d +\"=\" + c + \";expires=\" + tvc_time.toGMTString() \n                        + \";domain=\" + tvc_domain + \";path=/\";\n  }\n  function tvc_cid_function(){\n              ga(function(){\n                var tvc_trackers = ga.getAll();\n                var tvc_i, tvc_len;\n                for (tvc_i = 0, tvc_len = tvc_trackers.length; tvc_i \u003c tvc_len; tvc_i += 1) {\n                  if (tvc_trackers[tvc_i].get('trackingId') === tvc_UAID) { \n                    tvc_visitorID = tvc_trackers[tvc_i].get('clientId');\n                  }\n                }\n                if(tvc_visitorID != '' && typeof tvc_visitorID != 'undefined'){\n                  dataLayer.push({\n                    'event':'tvc_cid',\n                    'tvc_clientid':tvc_visitorID\n                  });\n                  tvc_setCookie('tvc_user_a_live',tvc_visitorID,730);\n                }\n              });\n     }\n   try{\n     var tvc_cid_myVar = setInterval(function(){ \n        if (typeof window.ga != 'undefined'){\n          clearInterval(tvc_cid_myVar);\n          tvc_cid_function();\n        }\n      }, 300);\n    }catch(e){\n      dataLayer.push({\n        'event':'tvc_error',\n        'tvc_error_msg':'tvc_cid_'+e.message\n      });\n    }\n  \n  \n  \n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1484212828195",
   "firingTriggerId": [
    "22"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/11?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/12",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "12",
   "name": "tvc_cid",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_client_id"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "tvc_client_id"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_clientid}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "3"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_clientid}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1479130366330",
   "firingTriggerId": [
    "23"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/12?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/13",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "13",
   "name": "tvc_Internal Promotion Impressions",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useEcommerceDataLayer",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "article listing"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "impressions"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "true"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481522955513",
   "parentFolderId": "29",
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/13?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/14",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "14",
   "name": "tvc_Internal Promotion Clicks",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useEcommerceDataLayer",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "article listing"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "clicks"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "true"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1480923566906",
   "parentFolderId": "29",
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/14?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/15",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "15",
   "name": "tvc_navigation",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_page_name}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "navigation - {{tvc_navigation_position}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_navigation_level_name}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481034386122",
   "firingTriggerId": [
    "35"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/15?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/16",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "16",
   "name": "tvc_language_change",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_page_name}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "language changed"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_language}} / {{tvc_new_language}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1483342557812",
   "firingTriggerId": [
    "36"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/16?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/17",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "17",
   "name": "tvc_footer_links",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_page_name}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "footer link clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_footer_link_label}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481032433966",
   "firingTriggerId": [
    "37"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/17?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/18",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "18",
   "name": "tvc_writeyourstory",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_page_name}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "writeyourownstory clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "writeyourownstory"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481032592423",
   "firingTriggerId": [
    "38"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/18?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/19",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "19",
   "name": "tvc_subscription",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_page_name}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "subscription triggered"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "n/a"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "16"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_get_email_id}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1483340193927",
   "firingTriggerId": [
    "39"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/19?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/20",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "20",
   "name": "tvc_article",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_page_name}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_page_section}} - article clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_article_name}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_category}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "12"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_position}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_name}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "13"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_page_section}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481033835793",
   "firingTriggerId": [
    "40"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/20?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/21",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "21",
   "name": "tvc_author",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_page_name}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_page_section}} - author clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_author_name}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_category}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "12"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_position}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_name}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "13"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_page_section}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481033653907",
   "firingTriggerId": [
    "41"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/21?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/22",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "22",
   "name": "tvc_social_media_icon",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_page_name}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_page_section}} - social media icon clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_social_media_platform}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_category}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_name}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "13"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_page_section}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481033813815",
   "firingTriggerId": [
    "42"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/22?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/23",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "23",
   "name": "tvc_pagination",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_page_name}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "pagination clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_pagination_action_item}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481033991065",
   "firingTriggerId": [
    "43"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/23?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/24",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "24",
   "name": "tvc_articlepage_tags",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_page_name}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "tag clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_tag_name}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_category}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_name}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481034147262",
   "firingTriggerId": [
    "44"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/24?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/25",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "25",
   "name": "tvc_infinite_scroll",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_page_name}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_inifinite_position}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_article_name}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_category}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_name}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481034292285",
   "firingTriggerId": [
    "45"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/25?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/26",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "26",
   "name": "TVC Infinite Scroll and Reader Scanner Tracking Inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n(function(){\n    function trackLocation()\n    {\n    \ttry{\n            if(tvc_counter && !tvc_docHeight && tvc_pageSeparator.length){\n                tvc_prevPageLength += tvc_scrollingArea[tvc_counter-1].scrollHeight + tvc_pageSeparator[tvc_counter-1].scrollHeight;\n            }\n            if(!tvc_docHeight){\n                if(tvc_scrollingArea[tvc_counter]){\n                    tvc_pageLength = tvc_scrollingArea[tvc_counter].scrollHeight;\n                    tvc_readingArea = tvc_scrollingArea[tvc_counter].querySelector('.ys_post_content');\n                    tvc_page_url = tvc_scrollingArea[tvc_counter]?tvc_scrollingArea[tvc_counter].getAttribute('data-page-path'):'NA';\n                    tvc_is_data = tvc_scrollingArea[tvc_counter].querySelector('.INFINITE_SCROLL_DATA');\n                    tvc_docHeight = true;\n                    tvc_didComplete = false;\n                    tvc_scroller = false;\n                    tvc_flag_25=false;\n                    tvc_flag_50=false;\n                    tvc_flag_75=false;\n                    tvc_articleBodyEnd=false;\n\n                    tvc_author_name = tvc_is_data?tvc_is_data.getAttribute('data-tvc_author_name'):'NA';\n\t                tvc_article_category = tvc_is_data?tvc_is_data.getAttribute('data-tvc_article_category'):'NA';\n\t                tvc_article_name = tvc_is_data?tvc_is_data.getAttribute('data-tvc_article_name'):'NA';\n\t                tvc_publication_year = tvc_is_data?tvc_is_data.getAttribute('data-tvc_publication_year'):'NA';\n\t                tvc_publication_month = tvc_is_data?tvc_is_data.getAttribute('data-tvc_publication_month') + ' ' + tvc_publication_year:'NA';\n\t                tvc_publication_date = tvc_is_data?tvc_is_data.getAttribute('data-tvc_publication_date') + ' ' + tvc_publication_month:'NA';\n\n\t                tvc_article_length = tvc_is_data?tvc_is_data.getAttribute('data-tvc_article_length'):'NA';\n\t                tvc_tags = tvc_is_data?tvc_is_data.getAttribute('data-tvc_tags'):'NA';\n\t                tvc_inifinite_position = tvc_is_data?(tvc_is_data.getAttribute('data-tvc_inifinite_position')?tvc_is_data.getAttribute('data-tvc_inifinite_position'):0):'NA';\n\n\n                    if(tvc_readingArea){\n                        if (window.innerHeight \u003c tvc_readingArea.clientHeight)\n                        {\n                            pageTimeLoad = new Date().getTime();\n                            tvc_readerLength = tvc_readingArea.clientHeight;\n                        }\n                    }\n                }\n            }\n            tvc_currentPosition = window.innerHeight + window.scrollY;\n\n            if (tvc_currentPosition \u003e= tvc_readingArea.scrollTop + tvc_readingArea.scrollHeight + tvc_readingArea.offsetTop + tvc_prevPageLength && !tvc_articleBodyEnd)\n            {\n                tvc_timeToScroll = new Date().getTime();\n                tvc_contentTime = Math.round((tvc_timeToScroll-scrollTimeStart)/1000);\n                if (tvc_contentTime \u003c tvc_readerTime)\n                {\n                    dataLayer.push({\n                        \"event\": \"tvc_infinite_scrolling\",\n                        \"tvc_event_category\": \"tvc_reader_scanner\",\n                        \"tvc_event_action\":\"scanner\",\n                        \"tvc_event_label\": tvc_page_url,\n                        \"tvc_author_name_scroll\": tvc_author_name,\n\t                    \"tvc_article_category_scroll\": tvc_article_category,\n\t                    \"tvc_article_name_scroll\": tvc_article_name,\n\t                    \"tvc_publication_year_scroll\": tvc_publication_year,\n\t                    \"tvc_publication_month_scroll\": tvc_publication_month,\n\t                    \"tvc_publication_date_scroll\": tvc_publication_date,\n\t                    \"tvc_article_length_scroll\": tvc_article_length,\n\t                    \"tvc_tags_scroll\": tvc_tags,\n\t                    \"tvc_inifinite_position_scroll\": tvc_inifinite_position\n                    });\n                }\n                else\n                {\n                    dataLayer.push({\n                        \"event\": \"tvc_infinite_scrolling\",\n                        \"tvc_event_category\": \"tvc_reader_scanner\",\n                        \"tvc_event_action\":\"reader\",\n                        \"tvc_event_label\": tvc_page_url,\n                        \"tvc_author_name_scroll\": tvc_author_name,\n\t                    \"tvc_article_category_scroll\": tvc_article_category,\n\t                    \"tvc_article_name_scroll\": tvc_article_name,\n\t                    \"tvc_publication_year_scroll\": tvc_publication_year,\n\t                    \"tvc_publication_month_scroll\": tvc_publication_month,\n\t                    \"tvc_publication_date_scroll\": tvc_publication_date,\n\t                    \"tvc_article_length_scroll\": tvc_article_length,\n\t                    \"tvc_tags_scroll\": tvc_tags,\n\t                    \"tvc_inifinite_position_scroll\": tvc_inifinite_position\n                    });\n                }\n                tvc_articleBodyEnd = true;\n            }\n\n            if (!tvc_scroller)\n            {\n                tvc_scroller = true;\n                scrollTimeStart = new Date().getTime();\n                if (pageTimeLoad \u003e 0)\n                {\n                    tvc_timeToScroll = Math.round((scrollTimeStart-pageTimeLoad)/1000);\n                }\n                else\n                { \n                    tvc_timeToScroll = \"\";\n                }\n                dataLayer.push({\n                    \"event\":\"tvc_infinite_scrolling\",\n                    \"tvc_event_category\": 'tvc_scrolling',\n                    \"tvc_event_action\":\"0%\",\n                    \"tvc_event_label\": tvc_page_url,\n                    \"tvc_author_name_scroll\": tvc_author_name,\n                    \"tvc_article_category_scroll\": tvc_article_category,\n                    \"tvc_article_name_scroll\": tvc_article_name,\n                    \"tvc_publication_year_scroll\": tvc_publication_year,\n                    \"tvc_publication_month_scroll\": tvc_publication_month,\n                    \"tvc_publication_date_scroll\": tvc_publication_date,\n                    \"tvc_article_length_scroll\": tvc_article_length,\n                    \"tvc_tags_scroll\": tvc_tags,\n                    \"tvc_inifinite_position_scroll\": tvc_inifinite_position\n                });\n            }\n            if (tvc_currentPosition \u003e= (tvc_pageLength + tvc_prevPageLength) && !tvc_didComplete)\n            {\n                dataLayer.push({\n                    \"event\":\"tvc_infinite_scrolling\",\n                    \"tvc_event_category\": 'tvc_scrolling',\n                    \"tvc_event_action\":\"100%\",\n                    \"tvc_event_label\": tvc_page_url,\n                    \"tvc_author_name_scroll\": tvc_author_name,\n                    \"tvc_article_category_scroll\": tvc_article_category,\n                    \"tvc_article_name_scroll\": tvc_article_name,\n                    \"tvc_publication_year_scroll\": tvc_publication_year,\n                    \"tvc_publication_month_scroll\": tvc_publication_month,\n                    \"tvc_publication_date_scroll\": tvc_publication_date,\n                    \"tvc_article_length_scroll\": tvc_article_length,\n                    \"tvc_tags_scroll\": tvc_tags,\n                    \"tvc_inifinite_position_scroll\": tvc_inifinite_position\n                });\n                tvc_didComplete = true;\n                tvc_docHeight = false;\n                tvc_counter += 1;\n            }\n            if (Math.round((tvc_pageLength*25)/100) + tvc_prevPageLength \u003c= tvc_currentPosition && !tvc_didComplete && !tvc_flag_25)\n            {\n                dataLayer.push({\n                    \"event\":\"tvc_infinite_scrolling\",\n                    \"tvc_event_category\": 'tvc_scrolling',\n                    \"tvc_event_action\":\"25%\",\n                    \"tvc_event_label\": tvc_page_url,\n                    \"tvc_author_name_scroll\": tvc_author_name,\n                    \"tvc_article_category_scroll\": tvc_article_category,\n                    \"tvc_article_name_scroll\": tvc_article_name,\n                    \"tvc_publication_year_scroll\": tvc_publication_year,\n                    \"tvc_publication_month_scroll\": tvc_publication_month,\n                    \"tvc_publication_date_scroll\": tvc_publication_date,\n                    \"tvc_article_length_scroll\": tvc_article_length,\n                    \"tvc_tags_scroll\": tvc_tags,\n                    \"tvc_inifinite_position_scroll\": tvc_inifinite_position\n                });\n                tvc_flag_25 = true;\n            }\n            if (Math.round((tvc_pageLength*50)/100) + tvc_prevPageLength \u003c= tvc_currentPosition && !tvc_didComplete && !tvc_flag_50)\n            {\n                dataLayer.push({\n                    \"event\":\"tvc_infinite_scrolling\",\n                    \"tvc_event_category\": 'tvc_scrolling',\n                    \"tvc_event_action\":\"50%\",\n                    \"tvc_event_label\": tvc_page_url,\n                    \"tvc_author_name_scroll\": tvc_author_name,\n                    \"tvc_article_category_scroll\": tvc_article_category,\n                    \"tvc_article_name_scroll\": tvc_article_name,\n                    \"tvc_publication_year_scroll\": tvc_publication_year,\n                    \"tvc_publication_month_scroll\": tvc_publication_month,\n                    \"tvc_publication_date_scroll\": tvc_publication_date,\n                    \"tvc_article_length_scroll\": tvc_article_length,\n                    \"tvc_tags_scroll\": tvc_tags,\n                    \"tvc_inifinite_position_scroll\": tvc_inifinite_position\n                });\n                tvc_flag_50 = true;\n            }\n            if (Math.round((tvc_pageLength*75)/100) + tvc_prevPageLength \u003c= tvc_currentPosition && !tvc_didComplete && !tvc_flag_75)\n            {\n                dataLayer.push({\n                    \"event\":\"tvc_infinite_scrolling\",\n                    \"tvc_event_category\": 'tvc_scrolling',\n                    \"tvc_event_action\":\"75%\",\n                    \"tvc_event_label\": tvc_page_url,\n                    \"tvc_author_name_scroll\": tvc_author_name,\n                    \"tvc_article_category_scroll\": tvc_article_category,\n                    \"tvc_article_name_scroll\": tvc_article_name,\n                    \"tvc_publication_year_scroll\": tvc_publication_year,\n                    \"tvc_publication_month_scroll\": tvc_publication_month,\n                    \"tvc_publication_date_scroll\": tvc_publication_date,\n                    \"tvc_article_length_scroll\": tvc_article_length,\n                    \"tvc_tags_scroll\": tvc_tags,\n                    \"tvc_inifinite_position_scroll\": tvc_inifinite_position\n                });\n                tvc_flag_75 = true;\n            }\n    \t}catch(e){}\n    }\n    try{\n        dataLayer = window.dataLayer || [];\n        var tvc_sectionHeader = document.querySelector('.sectionHeader');\n        var tvc_scrollingArea = document.querySelectorAll(\".ys_post_section\");\n        var tvc_readingArea;\n        var tvc_pageSeparator = document.querySelectorAll('.hr.pt-5');\n        var tvc_counter = 0;\n        var tvc_prevPageLength = tvc_sectionHeader?tvc_sectionHeader.scrollHeight:0;\n        var tvc_pageLength = 0;\n        var tvc_readerLength = 0;\n        var tvc_callBackTime = 100;\n        var tvc_timer = 0;\n        var tvc_scroller = false;\n        var tvc_didComplete = false;\n        var tvc_flag_25=false;\n        var tvc_flag_50=false;\n        var tvc_flag_75=false;\n        var tvc_docHeight = false;\n        var tvc_currentPosition, tvc_is_data;\n        var tvc_author_name, tvc_article_category, tvc_article_name, tvc_publication_year, tvc_publication_month, tvc_publication_date, tvc_article_length, tvc_tags, tvc_inifinite_position, tvc_page_url;\n        //var tvc_page_path;\n\n        var pageTimeLoad = 0;\n        var scrollTimeStart = 0;\n        var tvc_timeToScroll = 0;\n        var tvc_contentTime = 0;\n        var tvc_readerTime = 30;\n        var tvc_articleBodyEnd = false;\n\n        if(tvc_scrollingArea.length\u003e0)\n        {\n            window.onscroll = function(){\n                if (tvc_timer)\n                {\n                    clearTimeout(tvc_timer);\n                }\n                tvc_timer = setTimeout(trackLocation, tvc_callBackTime);\n            };\n            document.addEventListener('click', function(e){\n                if(e.target.className == 'INFINITE_SCROLL_DATA'){\n                    tvc_scrollingArea = document.querySelectorAll(\".ys_post_section\");\n                    tvc_pageSeparator = document.querySelectorAll('.hr.pt-5');\n                }\n            });\n            window.onbeforeunload = function(){\n                if(!tvc_didComplete && tvc_scroller){\n                    tvc_timeToScroll = new Date().getTime();\n                    tvc_contentTime = Math.round((tvc_timeToScroll-scrollTimeStart)/1000);\n                    if (tvc_contentTime \u003c (tvc_currentPosition*tvc_readerTime)/(tvc_prevPageLength+tvc_readingArea.scrollHeight+tvc_readingArea.offsetTop))\n                    {\n                        dataLayer.push({\n                            \"event\": \"tvc_infinite_scrolling\",\n                            \"tvc_event_category\": \"tvc_reader_scanner\",\n                            \"tvc_event_action\":\"scanner\",\n                            \"tvc_event_label\": tvc_page_url,\n                            \"tvc_author_name_scroll\": tvc_author_name,\n\t\t                    \"tvc_article_category_scroll\": tvc_article_category,\n\t\t                    \"tvc_article_name_scroll\": tvc_article_name,\n\t\t                    \"tvc_publication_year_scroll\": tvc_publication_year,\n\t\t                    \"tvc_publication_month_scroll\": tvc_publication_month,\n\t\t                    \"tvc_publication_date_scroll\": tvc_publication_date,\n\t\t                    \"tvc_article_length_scroll\": tvc_article_length,\n\t\t                    \"tvc_tags_scroll\": tvc_tags,\n\t\t                    \"tvc_inifinite_position_scroll\": tvc_inifinite_position\n                        });\n                    }\n                    else\n                    {\n                        dataLayer.push({\n                            \"event\": \"tvc_infinite_scrolling\",\n                            \"tvc_event_category\": \"tvc_reader_scanner\",\n                            \"tvc_event_action\":\"reader\",\n                            \"tvc_event_label\": tvc_page_url,\n                            \"tvc_author_name_scroll\": tvc_author_name,\n\t\t                    \"tvc_article_category_scroll\": tvc_article_category,\n\t\t                    \"tvc_article_name_scroll\": tvc_article_name,\n\t\t                    \"tvc_publication_year_scroll\": tvc_publication_year,\n\t\t                    \"tvc_publication_month_scroll\": tvc_publication_month,\n\t\t                    \"tvc_publication_date_scroll\": tvc_publication_date,\n\t\t                    \"tvc_article_length_scroll\": tvc_article_length,\n\t\t                    \"tvc_tags_scroll\": tvc_tags,\n\t\t                    \"tvc_inifinite_position_scroll\": tvc_inifinite_position\n                        });\n                    }\n                }\n            };\n        }\n    }catch(e){}\n})();\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1513949204173",
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/26?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/27",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "27",
   "name": "TVC Scroll Tracking Inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\nfunction tvc_trackScroll(){\n      try {\n        dataLayer = window.dataLayer || [];\n       \n          var tvc_scrollingArea = document.querySelector(\"body\");\n  \t\t  var tvc_callBackTime = 100;\n          var tvc_timer = 0;\n          var tvc_scroller = false;\n          var tvc_endContent = false;\n          var tvc_didComplete = false;\n          var tvc_pageTimeLoad = 0;\n          var tvc_scrollTimeStart = 0;\n          var tvc_flag_25 = false;\n          var tvc_flag_50 = false;\n          var tvc_flag_75 = false;\n\n          if (tvc_scrollingArea) {\n              if (window.innerHeight \u003c tvc_scrollingArea.clientHeight) {\n                  tvc_pageTimeLoad = new Date().getTime();\n              }\n                   \n              var tvc_trackLocation = function() {\n                \n                  tvc_currentPosition = window.innerHeight + window.scrollY;\n                  tvc_pageLength = document.body.scrollHeight;               \n                \n                  if (!tvc_scroller) {\n                      tvc_scroller = true;\n                    dataLayer.push({\n                          \"event\": \"tvc_scrolling\",\n                          \"tvc_event_category\": \"tvc_scrolling\",\n                          \"tvc_event_action\": \"0%\",\n                          \"tvc_event_label\": location.pathname\n                      });\n                  }\n                  //\tif (tvc_currentPosition == tvc_pageLength)\n                \n                  if (Math.round((tvc_pageLength * 25) / 100) \u003c= tvc_currentPosition && !tvc_didComplete && !tvc_flag_25) {\n                      dataLayer.push({\n                          \"event\": \"tvc_scrolling\",\n                          \"tvc_event_category\": \"tvc_scrolling\",\n                          \"tvc_event_action\": \"25%\",\n                          \"tvc_event_label\": location.pathname\n                      });\n                      tvc_flag_25 = true;\n                  }\n\t\t\t\n                  if (Math.round((tvc_pageLength * 50) / 100) \u003c= tvc_currentPosition && !tvc_didComplete && !tvc_flag_50) {\n                      dataLayer.push({\n                          \"event\": \"tvc_scrolling\",\n                          \"tvc_event_category\": \"tvc_scrolling\",\n                          \"tvc_event_action\": \"50%\",\n                          \"tvc_event_label\": location.pathname\n                      });\n                      tvc_flag_50 = true;\n                  }\n                \n                  if (Math.round((tvc_pageLength * 75) / 100) \u003c= tvc_currentPosition && !tvc_didComplete && !tvc_flag_75) {\n                      dataLayer.push({\n                          \"event\": \"tvc_scrolling\",\n                          \"tvc_event_category\": \"tvc_scrolling\",\n                          \"tvc_event_action\": \"75%\",\n                          \"tvc_event_label\": location.pathname\n                      });\n                      tvc_flag_75 = true;\n                  }\n\n                  if (tvc_currentPosition == tvc_pageLength && !tvc_didComplete) {\n                      dataLayer.push({\n                          \"event\": \"tvc_scrolling\",\n                          \"tvc_event_category\": \"tvc_scrolling\",\n                          \"tvc_event_action\": \"100%\",\n                          \"tvc_event_label\": location.pathname\n                      });\n                      tvc_didComplete = true;\n                  }\n              }\n\n              {{TVCaddEvent}}(window, 'scroll', function(){\n                  if (tvc_timer) {\n                      clearTimeout(tvc_timer);\n                  }\n                  tvc_timer = setTimeout(tvc_trackLocation, tvc_callBackTime);\n              });\n          }\n      }catch(e){\n    \ttvc_track_error(e.message, {{Page Path}},'TVC Scroll Tracking Inject - 1');\n      }\n}\n  \n  \n  tvc_trackScroll();\n/*  \n  var tvc_old_url = window.location.href;\nvar tvc_interval =   setInterval(function() {\n    \n    if (window.location.href != tvc_old_url) {\n             \n\t\ttvc_old_url = window.location.href;\n      tvc_trackScroll();\n      console.log(\"interval\");\n     \n    }\n  }, 500);\n  \n  */\n\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1517205662273",
   "firingTriggerId": [
    "56",
    "299"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/27?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/28",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "28",
   "name": "TVC Heatmap Collection",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\nfunction tvc_create_cookie(cname, cvalue, exyears) {\n  var d = new Date();\n  d.setTime(d.getTime() + (3600 * 1000 * 24 * 365 * exyears));\n  var expires = \"expires=\"+ d.toUTCString();\n  document.cookie = cname + \"=\" + cvalue + \"; \" + expires;\n}\nfunction tvc_read_cookie(key)\n{\n var result;\n return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;\n}\ntry{\n  var tvc_heatmap_deviceType = /iPad|Tablet|KFAPWI/.test(navigator.userAgent) ? \"Tablet\" : /Mobile|iPhone|iPod|Android|BlackBerry|IEMobile|Silk|BB10|UC[\\s]?Browser|Opera M(ob|in)i/g.test(navigator.userAgent) ? \"Mobile\" : \"Desktop\";\n  if(tvc_heatmap_deviceType == \"Desktop\"){\n    var tvc_date = new Date();\n    var tvc_d = ((\"0\" + tvc_date.getDate()).slice(-2) + (\"0\" + tvc_date.getMonth()).slice(-2) + tvc_date.getFullYear());\n    var tvc_user_type;\n    var tvc_gaid = tvc_read_cookie(\"_ga\")?(tvc_read_cookie(\"_ga\").split(\".\")[2] + \".\" + tvc_read_cookie(\"_ga\").split(\".\")[3]):\"NA\";\n    if(tvc_read_cookie(\"tvc_heatmap\")){\n      tvc_user_type = \"returning\";\n    }else{\n      tvc_user_type = \"new\";\n    }\n    if((window.innerWidth \u003e ((window.screen.width*80)/100)) && (window.top == window.self)){\n      //var tvc_heatmap_pageview_pixel = new Image();\n      //tvc_heatmap_pageview_pixel.src= \"//d2ca9sd3xc5ctu.cloudfront.net/collect.gif?type=pageview&res=\" + (window.screen.width + \"x\" + window.screen.height) + \"&page=\" + (window.location.host + window.location.pathname) + \"&ut=\" + tvc_user_type + \"&ga=\" + tvc_gaid + \"&env=l&d=\" + tvc_d;\n    }\n    if(!tvc_read_cookie(\"tvc_heatmap\")){\n      tvc_create_cookie(\"tvc_heatmap\",\"enabled\",2);\n    }\n    document.body.addEventListener(\"mousedown\",function(e){\n      if((window.innerWidth \u003e ((window.screen.width*80)/100)) && (window.top == window.self)){\n\n        var tvc_tag = {\n          BODY: \"0\",\n          H1: \"1\",\n          H2: \"2\",\n          H3: \"3\",\n          H4: \"4\",\n          H5: \"5\",\n          H6: \"6\",\n          MAIN: \"7\",\n          PICTURE: \"8\",\n          TBODY: \"9\",\n          SUB: \"_\",\n          STRIKE: \"-\",\n          OUTPUT: \":\",\n          IFRAME: \"!\",\n          SMALL: \".\",\n          FIGCAPTION: \"(\",\n          FIGURE: \")\",\n          HEADER: \"[\",\n          FOOTER: \"]\",\n          FIELDSET: \"{\",\n          ASIDE: \"}\",\n          PATH: \"§\",\n          ABBR: \"@\",\n          ARTICLE: \"*\",\n          I: \"/\",\n          VIDEO: \"#\",\n          PROGRESS: \"%\",\n          METER: \"^\",\n          SUP: \"°\",\n          HGROUP: \"+\",\n          DATALIST: \"\u003c\",\n          BUTTON: \"=\",\n          MARK: \"\u003e\",\n          SECTION: \"|\",\n          AUDIO: \"~\",\n          TIME: \"$\",\n          A: \"a\",\n          AREA: \"A\",\n          B: \"B\",\n          BLOCKQUOTE: \"b\",\n          CENTER: \"c\",\n          CITE: \"C\",\n          CANVAS: \"D\",\n          DIV: \"d\",\n          EM: \"E\",\n          EMBED: \"e\",\n          OBJECT: \"e\",\n          FONT: \"f\",\n          FORM: \"F\",\n          BIG: \"G\",\n          STRONG: \"g\",\n          HR: \"h\",\n          TH: \"H\",\n          IMG: \"i\",\n          INPUT: \"I\",\n          SAMP: \"j\",\n          TT: \"J\",\n          KBD: \"k\",\n          S: \"K\",\n          LABEL: \"l\",\n          LI: \"L\",\n          MAP: \"m\",\n          SVG: \"M\",\n          MENU: \"n\",\n          NAV: \"N\",\n          OL: \"O\",\n          OPTION: \"o\",\n          P: \"p\",\n          PRE: \"P\",\n          CODE: \"Q\",\n          Q: \"q\",\n          BDI: \"R\",\n          TR: \"r\",\n          SELECT: \"s\",\n          SPAN: \"S\",\n          TABLE: \"T\",\n          TD: \"t\",\n          ADDRESS: \"U\",\n          UL: \"u\",\n          U: \"v\",\n          VAR: \"V\",\n          DD: \"w\",\n          DL: \"W\",\n          DT: \"X\",\n          TEXTAREA: \"x\",\n          CAPTION: \"Y\",\n          LEGEND: \"y\",\n          DETAILS: \"z\",\n          SUMMARY: \"Z\"\n        };\n\n        // Coordinates\n        e = e || window.event;\n        var tvc_target = e.target || e.srcElement,\n        tvc_clk_ele_style = tvc_target.currentStyle || window.getComputedStyle(tvc_target, null),\n        tvc_borderLeftWidth = parseInt(tvc_clk_ele_style['borderLeftWidth'], 10),\n        tvc_borderTopWidth = parseInt(tvc_clk_ele_style['borderTopWidth'], 10),\n        tvc_rect = tvc_target.getBoundingClientRect(),\n        tvc_offsetX = e.clientX - tvc_borderLeftWidth - tvc_rect.left,\n        tvc_offsetY = e.clientY - tvc_borderTopWidth - tvc_rect.top;\n        tvc_offsetX = ( !tvc_offsetX || isNaN(tvc_offsetX) || (tvc_offsetX\u003c0) )?0:tvc_offsetX;\n        tvc_offsetY = ( !tvc_offsetY || isNaN(tvc_offsetY) || (tvc_offsetY\u003c0) )?0:tvc_offsetY;\n        //console.log(\"tvc_offsetX: \" + tvc_offsetX);\n        //console.log(\"tvc_offsetY: \" + tvc_offsetY);\n\n        // Parent Structure\n        var tvc_current_ele = tvc_target;\n        var tvc_parent_tree = [];\n        var tvc_parent_tree_index = [];\n        while(tvc_current_ele.tagName.toLowerCase() != \"body\"){\n          var tvc_childItems = [];\n          var tvc_children = tvc_current_ele.parentElement.childNodes;\n          for(var i = 0; i \u003c tvc_children.length; i++) {\n              if(tvc_children[i].nodeName == tvc_current_ele.tagName) {\n                  tvc_childItems.push(tvc_children[i]);\n              }\n          }\n          var tvc_node_list = Array.prototype.slice.call( tvc_childItems );\n          tvc_parent_tree.push(tvc_tag[tvc_current_ele.tagName]?tvc_tag[tvc_current_ele.tagName]:tvc_current_ele.tagName);\n          tvc_parent_tree_index.push(tvc_node_list.indexOf(tvc_current_ele));\n          tvc_current_ele = tvc_current_ele.parentElement;\n        }\n        tvc_parent_tree = tvc_parent_tree.reverse().join(\",\");\n        tvc_parent_tree_index = tvc_parent_tree_index.reverse().join(\"|\");\n        //console.log(\"Parent Tree: \" + tvc_parent_tree);\n        //console.log(\"Parent Tree Index: \" + tvc_parent_tree_index);\n        //console.log(\"Page: \" + window.location.href);\n        //console.log(\"Resolution: \" + window.screen.width + \"x\" + window.screen.height);\n        var tvc_heatmap_visible = sessionStorage.getItem(\"map\")?sessionStorage.getItem(\"map\"):0;\n        if(tvc_offsetX && tvc_offsetY && !tvc_heatmap_visible){\n          var tvc_heatmap_click_pixel = new Image();\n          tvc_heatmap_click_pixel.src= \"//d2ca9sd3xc5ctu.cloudfront.net/collect.gif?x=\" + tvc_offsetX + \"&y=\" + tvc_offsetY + \"&pt=\" + tvc_parent_tree + \"&pti=\" + tvc_parent_tree_index + \"&res=\" + (window.screen.width + \"x\" + window.screen.height) + \"&page=\" + (window.location.host + window.location.pathname) + \"&ut=\" + tvc_user_type + \"&ga=\" + tvc_gaid + \"&env=l&d=\" + tvc_d;\n        }\n      }\n    });\n  }\n}catch(e){}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1495445838590",
   "firingTriggerId": [
    "47"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/28?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/29",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "29",
   "name": "tvc_js_error",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_js_error"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{Error URL}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{Error Message}} : {{Error Line}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1513949387118",
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/29?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/30",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "30",
   "name": "tvc_404_error",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_404_error"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{Page Path}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{Referrer}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1501839702590",
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/30?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/32",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "32",
   "name": "tvc_adblock_content_ratio_copytext_implementation",
   "type": "html",
   "priority": {
    "type": "integer",
    "value": "1000"
   },
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n/**** Start of Adblock and content ratio ****/\n  try{\n    var tvc_AdBlockEnabled = function() {\n      var tvc_ad = document.createElement('ins');\n      tvc_ad.className = 'AdSense';\n      tvc_ad.style.display = 'block';\n      tvc_ad.style.position = 'absolute';\n      tvc_ad.style.top = '-1px';\n      tvc_ad.style.height = '1px';\n      document.body.appendChild(tvc_ad);\n      var tvc_isAdBlockEnabled = !tvc_ad.clientHeight;\n      document.body.removeChild(tvc_ad);\n      return tvc_isAdBlockEnabled;\n    }\n  }catch(e){\n    tvc_track_error(e.message, {{Page Path}},'tvc_adblock_content_ratio_copytext_implementation - tvc_AdBlockEnabled');\n  }\n\n  try{\n    var tvc_getRatio = function(){\n      if(!tvc_AdBlockEnabled()){\n        var tvc_adsArea = 0;\n        //finding the total height width of the document the element which contain the whole content\n        var tvc_total = document.getElementsByTagName('body')[0].offsetWidth * document.getElementsByTagName('body')[0].offsetHeight;\n        $('iframe').each(function(){ \n\n          if($(this).attr('id') !== undefined){\n            var tvc_temp = $(this).attr('id').replace(/\\//g,'\\\\\\\\/');\n          }\n\n          tvc_height = $(this).attr('height');\n          tvc_width =  $(this).attr('width');\n          var tvc_flag =$($(this), window.parent.document).is(\":visible\");\n\n          if(tvc_temp !== undefined && tvc_height \u003e 0 && tvc_width \u003e 0 && tvc_flag ){ \n            if(tvc_temp.indexOf(\"google\") != -1)\n                tvc_adsArea += (tvc_height * tvc_width);\n          }\n        });\n\n        var tvc_final_content_ratio = ((tvc_adsArea/tvc_total)*100).toFixed(2);\n        var tvc_content_ratio_range = \"NA\";\n        if( tvc_final_content_ratio \u003e 0 && tvc_final_content_ratio \u003c=5 ){\n            tvc_content_ratio_range = \"0-5\";\n        }\n        else if( tvc_final_content_ratio \u003e 5 && tvc_final_content_ratio \u003c=10 ){\n            tvc_content_ratio_range = \"6-10\";\n        }\n        else if( tvc_final_content_ratio \u003e 10 && tvc_final_content_ratio \u003c=15 ){\n            tvc_content_ratio_range = \"11-15\";\n        }\n        else if( tvc_final_content_ratio \u003e 15 && tvc_final_content_ratio \u003c=20 ){\n            tvc_content_ratio_range = \"16-20\";\n        }\n        else if( tvc_final_content_ratio \u003e 20 && tvc_final_content_ratio \u003c=25 ){\n            tvc_content_ratio_range = \"21-25\";\n        }\n        else if( tvc_final_content_ratio \u003e 25 && tvc_final_content_ratio \u003c=30 ){\n            tvc_content_ratio_range = \"26-30\";\n        }\n        else{\n            tvc_content_ratio_range = \"NA\";\n        }\n\n        dataLayer.push({\n          \"event\" : \"tvc_adblock_content_ratio\",  \n          \"tvc_event_category\": \"tvc_ad_to_content_ratio\",\n          \"tvc_event_action\": tvc_content_ratio_range\n        });\n      }\n    }\n  }catch(e){\n    tvc_track_error(e.message, {{Page Path}},'tvc_adblock_content_ratio_copytext_implementation - tvc_AdBlockEnabled');\n  }\n/**** End of Adblock and content ratio ****/\n  \n  \n/**** Start of Content copy script****/\nvar tvc_flag = true;\n(function(){\n  tvc_custom = [];\n}());\n\n\n  try{\n    var tvc_getSelected = function() {\n        if(window.getSelection) {return window.getSelection();}\n        else if(document.getSelection) {return document.getSelection();}\n        else {\n        var selection = document.selection && document.selection.createRange();\n        if(selection.text) { return selection.text; }\n        return false;\n        }\n      return false;\n    }\n  }catch(e){\n    tvc_track_error(e.message, {{Page Path}},'tvc_adblock_content_ratio_copytext_implementation - tvc_AdBlockEnabled');\n  }\n\n  try{\n    var tvcTrack = function(){\n      var tvc_selection = tvc_getSelected();\n      var tvc_maxLength = 150;\n      if(tvc_selection && (tvc_selection = new String(tvc_selection).replace(/^\\s+|\\s+$/g,''))){\n        var tvc_textLength = tvc_selection.length;\n        if (tvc_selection.length \u003e tvc_maxLength){\n          tvc_selection = tvc_selection.substr(0, tvc_maxLength) + \"...\"\n        }\n        else {\n          tvc_selection = tvc_selection;\n            }\n\n        var x = tvc_selection.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.[a-zA-Z0-9._-]+)/gi);\n\n        if(!x == null){\n          for(var i=0; i \u003c x.length; i++){\n            tvc_selection = tvc_selection.replace(x[i],\"emailid\");\n          }\n        }\n        for(i=0; i \u003c tvc_custom.length; i++){\n          if(tvc_custom[i]['lastcopy']== tvc_selection)\n            tvc_flag = false;\n        }\n\n        if(tvc_custom.length === 0 || tvc_flag){\n          tvc_custom.push({'lastcopy':tvc_selection});\n          dataLayer.push({\n            \"event\" : \"tvc_site_text_copy\",\n            \"tvc_event_label\": tvc_selection,\n            \"tvc_event_value\": tvc_textLength\n          });\n        }\n        tvc_flag = true;\n      }\n    }\n  }catch(e){\n    tvc_track_error(e.message, {{Page Path}},'tvc_adblock_content_ratio_copytext_implementation - tvc_AdBlockEnabled');\n  }\n\n// End script from Motyar\ndocument.onreadystatechange = function () {\ndocument.body.addEventListener(\"copy\",tvcTrack);\ndocument.body.addEventListener(\"oncopy\",tvcTrack); //For IE\n}\n/**** End of Content copy script****/\n  \n\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1517312575917",
   "firingTriggerId": [
    "56"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/32?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/33",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "33",
   "name": "TVC YouTube Video Tracking Inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript type=\"text/javascript\"\u003e\n    function onYouTubeIframeAPIReady() {\n        for (var tvc_e = document.getElementsByTagName(\"iframe\"), x = tvc_e.length; x--;) {\n            if (/youtube.com\\/embed/.test(tvc_e[x].src)) {\n                tvc_gtmYTPlayers.push(new YT.Player(tvc_e[x], {\n                    events: {\n                        onStateChange: onPlayerStateChange,\n                        onError: onPlayerError\n                    }\n                }));\n                YT.gtmLastAction = \"p\";\n            }\n        }\n    }\n\n    function onPlayerStateChange(tvc_e) {\n        tvc_e[\"data\"] == YT.PlayerState.PLAYING && setTimeout(onPlayerPercent, 1000, tvc_e[\"target\"]);\n        var tvc_video_data = tvc_e.target[\"getVideoData\"](),\n            tvc_label = tvc_video_data.video_id + ' : ' + tvc_video_data.title;\n        var tvc_volume_level = (tvc_e.target[\"isMuted\"]())?0:(tvc_e.target[\"getVolume\"]());\n        var tvc_playback_quality = tvc_e.target[\"getPlaybackQuality\"]();\n        var tvc_time_elapsed = tvc_e.target[\"getCurrentTime\"]();\n        if (tvc_e[\"data\"] == YT.PlayerState.PLAYING && YT.gtmLastAction == \"p\") {\n            window.tvc_tot_play += 1;\n            dataLayer.push({\n                event: 'tvc_youtube',\n                tvc_event_category: 'tvc_ystv_videos',\n                tvc_event_action: 'Play',\n                tvc_event_label: tvc_label\n            });\n            YT.gtmLastAction = \"\";\n        }\n        if (tvc_e[\"data\"] == YT.PlayerState.PLAYING && YT.gtmLastAction == \"b\") {\n            window.tvc_tot_play += 1;\n            var d = new Date();\n            window.tvc_end_time = d.getTime();\n            window.tvc_buffer_time += window.tvc_end_time - window.tvc_start_time;\n            dataLayer.push({\n                event: 'tvc_youtube',\n                tvc_event_category: 'tvc_ystv_videos',\n                tvc_event_action: 'Play',\n                tvc_event_label: tvc_label\n            });\n            YT.gtmLastAction = \"\";\n        }\n        if (tvc_e[\"data\"] == YT.PlayerState.PAUSED && YT.gtmLastAction != \"b\") {\n            window.tvc_tot_pause += 1;\n            dataLayer.push({\n                event: 'tvc_youtube',\n                tvc_event_category: 'tvc_ystv_videos',\n                tvc_event_action: 'Pause',\n                tvc_event_label: tvc_label\n            });\n            YT.gtmLastAction = \"p\";\n        }\n        if (tvc_e[\"data\"] == YT.PlayerState.BUFFERING && (tvc_e.target[\"getCurrentTime\"]() != 0)) {\n            window.tvc_tot_interrupt += 1;\n            var d = new Date();\n            window.tvc_start_time = d.getTime();\n            YT.gtmLastAction = \"b\";\n        }\n        if (tvc_e[\"data\"] == YT.PlayerState.BUFFERING && (tvc_e.target[\"getCurrentTime\"]() == 0)) {\n            window.tvc_buffer_time = 0;\n            window.tvc_tot_play = 0;\n            window.tvc_tot_pause = 0;\n            window.tvc_tot_interrupt = 0;\n            var d = new Date();\n            window.tvc_start_time = d.getTime();\n            YT.gtmLastAction = \"b\";\n        }\n    }\n\n    function onPlayerError(tvc_e) {\n        dataLayer.push({\n            event: 'tvc_youtube',\n            tvc_event_category: 'tvc_ystv_videos',\n            tvc_event_action: 'Error',\n            tvc_event_label: 'youtube: ' + tvc_e\n        })\n    }\n\n    function onPlayerPercent(tvc_e) {\n        if (tvc_e[\"getPlayerState\"]() == YT.PlayerState.PLAYING) {\n            var tvc_t = tvc_e[\"getDuration\"]() - tvc_e[\"getCurrentTime\"]() \u003c= 1.5 ? 1 : (Math.floor(tvc_e[\"getCurrentTime\"]() / tvc_e[\"getDuration\"]() * 10) / 10).toFixed(2);\n            if (parseFloat(tvc_t) \u003c 0.25) {\n                tvc_t = 0.00;\n            }\n            else if (parseFloat(tvc_t) \u003c 0.5){\n                tvc_t = 0.25;\n            }\n            else if (parseFloat(tvc_t) \u003c 0.75){\n                tvc_t = 0.50;\n            }\n            else if (parseFloat(tvc_t) \u003c 0.9){\n                tvc_t = 0.75;\n            }\n            else if (parseFloat(tvc_t) \u003c 1){\n                tvc_t = 0.90;\n            }\n            tvc_t = tvc_t.toFixed(2);\n\n            if (!tvc_e[\"lastP\"] || tvc_t \u003e tvc_e[\"lastP\"]) {\n                var tvc_video_data = tvc_e[\"getVideoData\"](),\n                    tvc_label = tvc_video_data.video_id + ' : ' + tvc_video_data.title;\n                var tvc_volume_level = (tvc_e[\"isMuted\"]())?0:(tvc_e[\"getVolume\"]());\n                var tvc_playback_quality = tvc_e[\"getPlaybackQuality\"]();\n                var tvc_time_elapsed = tvc_e[\"getCurrentTime\"]();\n                tvc_e[\"lastP\"] = tvc_t;\n                dataLayer.push({\n                    event: \"tvc_youtube\",\n                    tvc_event_category: 'tvc_ystv_videos',\n                    tvc_event_action: tvc_t * 100 + \"%\",\n                    tvc_event_label: tvc_label\n                });\n            }\n            tvc_e[\"lastP\"] != 1 && setTimeout(onPlayerPercent, 1000, tvc_e);\n        }\n    }\ntry{\n    var tvc_gtmYTPlayers = [];\n    var tvc_start_time,tvc_end_time,tvc_buffer_time;\n    var tvc_tot_play, tvc_tot_pause, tvc_tot_interrupt;\n\n    for (var tvc_e = document.getElementsByTagName(\"iframe\"), x = tvc_e.length; x--;)\n        if (/youtube.com\\/embed/.test(tvc_e[x].src))\n            if (tvc_e[x].src.indexOf('enablejsapi=') === -1)\n                tvc_e[x].src += (tvc_e[x].src.indexOf('?') === -1 ? '?' : '&') + 'enablejsapi=1';\n\n    var tvc_j = document.createElement(\"script\"),\n        tvc_f = document.getElementsByTagName(\"script\")[0];\n    tvc_j.src = \"//www.youtube.com/iframe_api\";\n    tvc_j.async = true;\n    tvc_f.parentNode.insertBefore(tvc_j, tvc_f);\n}catch(e){}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1481533624969",
   "firingTriggerId": [
    "54"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/33?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/34",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "34",
   "name": "tvc_adblock_content_ratio_UA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "{{tvc_event_category}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_event_action}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{Page Path}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1516284637412",
   "firingTriggerId": [
    "55"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/34?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/35",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "35",
   "name": "TVC YouTube Video Tracking GA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "{{tvc_event_category}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_event_action}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481266995147",
   "firingTriggerId": [
    "57"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/35?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/36",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "36",
   "name": "tvc_site_text_copy",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_text_copy"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "text_copied"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "eventValue",
     "value": "{{tvc_event_value}}"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1513949407778",
   "firingTriggerId": [
    "59"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/36?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/37",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "37",
   "name": "tvc_outbound_link_click",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_outbound_link_clicked"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_outbound_link}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_outbound_link_text}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1481282859409",
   "firingTriggerId": [
    "65"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/37?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/39",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "39",
   "name": "TVC_HeatMap Visualization",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n  var tvc_hm_opc =0.3;\nvar getQueryString = function ( field, url ) {\n    var href = url ? url : window.location.href;\n    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );\n    var string = reg.exec(href);\n    return string ? string[1] : null;\n};\nfunction tvc_hm_run(tvc_hm_opc){\n    sessionStorage.setItem('map',1);\n    sessionStorage.setItem('opc',tvc_hm_opc);\n    var tvc_hm = document.createElement('script');\n    tvc_hm.type = 'text/javascript';\n    tvc_hm.async = 'true';\n    tvc_hm.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'd2ca9sd3xc5ctu.cloudfront.net/asserts/render.js';\n    var tvc_s = document.getElementsByTagName('script')[0];\n    tvc_s.parentNode.insertBefore(tvc_hm, tvc_s);\n}\n\nvar tvc_hm_map = getQueryString('map',document.URL);\nvar tvc_hm_key = getQueryString('access_key',document.URL);\n tvc_hm_opc = parseFloat(getQueryString('opc',document.URL));\nif(tvc_hm_map == 'heat' && tvc_hm_key == '8062747227'){\n    tvc_hm_run(tvc_hm_opc);\n}\n\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1484234263688",
   "firingTriggerId": [
    "67"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/39?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/40",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "40",
   "name": "tvc_infinite_scroll_pageview",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useHashAutoLink",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_PAGEVIEW"
    },
    {
     "type": "list",
     "key": "contentGroup",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "1"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_get_language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_cg_active_menu}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "3"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_get_articleCategory}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "4"
        },
        {
         "type": "template",
         "key": "group",
         "value": "{{tvc_get_authorName}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "decorateFormsAutoLink",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{tvc_page_url}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "1"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_author_name}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_name}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_category}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "6"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_year}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "7"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_month_year_is}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "8"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_final_date_is}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "9"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_length}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "10"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_video_url}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "14"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_tags}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "15"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_age_is}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1484646720553",
   "firingTriggerId": [
    "45"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/40?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/42",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "42",
   "name": "TVC Scroll and Reader Scanner Tracking GA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "{{tvc_event_category}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_event_action}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "useBeacon"
        },
        {
         "type": "template",
         "key": "value",
         "value": "true"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "1"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_author_name_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_name_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_category_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "6"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_year_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "7"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_month_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "8"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_date_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "9"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_length_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "12"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_inifinite_position_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "14"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_tags_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "15"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_age_scroll}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1513949218321",
   "firingTriggerId": [
    "51",
    "84"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/42?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/43",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "43",
   "name": "tvc_image_text_ratio_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\nfunction clickOrigin(e){\n    var target = e.target;\n    var tag = [];\n    tag.tagType = target.tagName.toLowerCase();\n    tag.tagClass = target.className.split(' ');\n    tag.id = target.id;\n    tag.parent = target.parentNode;\n  \ttag.t = target;\n\n    return tag;\n}\n  \nfunction pushDataLayer(a, b, c){\n\tdataLayer.push({\n    \tevent: a,\n        tvc_clicked_element: b,\n        tvc_target_link: c\n    });\n}\ndocument.body.addEventListener(\"click\", function(e){\n  \n    elem = clickOrigin(e);\n    var tvc_text_click = \"text click\";\n  \tvar tvc_img_click = \"image click\";\n  \n  \tvar tvc_clicked_element;\n    var tvc_target_link;\n\t// check for the img tag type\n  \tif(elem.tagType == \"img\"){\n  \t\ttvc_clicked_element = tvc_img_click;\n        // if elem.parent.parentElement is a then find the href\n      \ttry{\n      \t\ttvc_target_link = elem.parent.parentElement.href;\n        }catch(e){}\n    }\n  \tif(elem.tagType == \"p\"){\n       tvc_clicked_element = tvc_text_click;\n        // if elem.parent.parentElement is a then find the href\n        if(elem.parent.tagName.toLowerCase() == \"a\"){\n            try{\n                console.log(\"a\");\n      \t\t    tvc_target_link = elem.parent.href;\n            }catch(e){}\n        }else{\n            try{\n      \t\t    tvc_target_link = elem.parent.parentElement.href;\n            }catch(e){}\n        }\n      \t\n        \n    }  \n  \tif(elem.tagType == \"div\"){\n  \t    if(elem.parent.className.includes(\"btn-write-story\")){\n    \t        return;\n    \t}\n  \t    if(elem.tagClass.includes(\"overlay\") || elem.tagClass.includes(\"pill\") ){\n  \t        tvc_clicked_element = tvc_img_click;\n  \t        try{\n              \ttvc_target_link = elem.parent.parentElement.href;\n            }catch(e){}\n  \t    }\n  \t    else if(elem.id == \"iframeBlocker\"){\n  \t        tvc_clicked_element = tvc_img_click;\n  \t        try{\n              \ttvc_target_link = elem.parent.parentElement.parentElement.href;\n            }catch(e){}\n  \t    }else{\n    \t    tvc_clicked_element = tvc_text_click;\n    \t    \n          \ttry{\n              \ttvc_target_link = elem.parent.href;\n            }catch(e){}\n  \t    }\n    }\n  \tif(elem.tagType == \"span\"){\n    \ttvc_clicked_element = tvc_text_click;\n        try{\n          \ttvc_target_link = elem.parent.parentElement.parentElement.href\n        }catch(e){}      \n    }\n  \tif(elem.tagType == \"a\"){\n  \t    if(elem.t.className.includes(\"btn-write-story\") || \n  \t    elem.t.className.includes(\"logo\") || \n  \t    elem.parent.parentElement.className.includes(\"navigation\") ||\n  \t    elem.parent.parentElement.className.includes(\"navDropdown_list\") ||\n  \t    elem.parent.parentElement.className.includes(\"sectionLanguage_list\") ||\n  \t    elem.parent.parentElement.className.includes(\"pagesNav\") ||\n  \t    elem.t.className.includes(\"pagesNav-nextPage\") || \n  \t    elem.t.className.includes(\"footerLogo\") ||\n  \t    elem.parent.parentElement.parentElement.className.includes(\"navigation\")){\n    \t        return;\n    \t}\n       \ttvc_clicked_element = tvc_text_click;\n      \ttry{\n          \ttvc_target_link = elem.t.href;\n        }catch(e){}\n   \t}\n  \tif(tvc_clicked_element != undefined && tvc_target_link != undefined)\n  \t\tpushDataLayer(\"tvc_it_article_clicked\", tvc_clicked_element, tvc_target_link);\n});\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1513949440849",
   "firingTriggerId": [
    "89"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/43?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/44",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "44",
   "name": "tvc_it_article_clicked",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_image_text_ratio"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_clicked_element}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_target_link}}"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1484140196506",
   "firingTriggerId": [
    "90"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/44?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/45",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "45",
   "name": "Scroll Map Visualization",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\nvar tvc_parts = 4; // How many ranges 20 40 60 80 100 then 5 parts above data is for 10 parts\n\nvar getQueryString = function ( field, url ) {\n  var href = url ? url : window.location.href;\n  var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );\n  var string = reg.exec(href);\n  return string ? string[1] : null;\n};\n\nfunction tvcMapIt() {\nvar tvc_smap = document.createElement('script'); tvc_smap.type = 'text/javascript'; tvc_smap.async = true;\ntvc_smap.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'd2ca9sd3xc5ctu.cloudfront.net/asserts/tvc_scroll2.js';\nvar s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tvc_smap, s);\n}\n  \nfunction tvcRunScroll(){\n  \n\n\n  (function() {\n    var tvcScrolld = document.createElement('script'); tvcScrolld.type = 'text/javascript'; tvcScrolld.async = false;\n    tvcScrolld.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'd2ca9sd3xc5ctu.cloudfront.net/yourstory_com/yscroll.js';\n    var tvcS = document.getElementsByTagName('script')[0]; tvcS.parentNode.insertBefore(tvcScrolld, tvcS);\n})();\n\ntvcMapIt();\n}\nvar tvc_hm_map = getQueryString('map',document.URL);\nvar tvc_hm_key = getQueryString('access_key',document.URL);\n\n   if(tvc_hm_map == 'scroll' && tvc_hm_key == '8062747227'){\nsetTimeout(function(){ tvcRunScroll() },5000);\n}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1484833934950",
   "firingTriggerId": [
    "91"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/45?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/46",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "46",
   "name": "tvc_internet_speed_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\nif (typeof({{NetSpeedCookie}}) == \"undefined\") {\n\n\n        var tvc_arrTimes = [];\n        var i = 0; // start\n        var tvc_timesToTest = 5;\n        var tvc_tThreshold = 100; //ms\n        var tvc_testImage = \"https://d25medu75j19j3.cloudfront.net/wp-content/uploads/2014/07/Mark-Zuckerberg.jpg\"; // small image in your server\n        var tvc_dummyImage = new Image();\n        var tvc_isConnectedFast = false;\n        var tvc_Labels = ['2G', '3G', '4G'];\n\n        testLatency(function(avg) {\n            var getSpeed = ( 24.8 * 5) / (avg / 1000);\n            tvc_isConnectedFast = (getSpeed \u003e tvc_tThreshold);\n            var tvc_roundOffSpeed = Math.ceil(getSpeed);\n            var tvc_myspeed = '';\n\n            if (tvc_roundOffSpeed \u003e= 5 && tvc_roundOffSpeed \u003c= 35) {\n                tvc_myspeed = tvc_Labels[0];\n            } else if (tvc_roundOffSpeed \u003e= 36 && tvc_roundOffSpeed \u003c= 85) {\n                tvc_myspeed = tvc_Labels[1];\n            }\n\n            if (tvc_roundOffSpeed \u003e= 86) {\n                tvc_myspeed = tvc_Labels[2];\n            }\n\n            dataLayer.push({\n                \"tvc_connection_type\": tvc_myspeed,\n                \"tvc_connection_speed\": getSpeed.toFixed(2)\n            });\n\n        });\n\n        tvc_dummyImage.onerror = function(err, msg) {\n            //tvc_track_error(err,Page Path , \"tvc_internet_speed_inject\");\n        }\n\n        function testLatency(cb) {\n            var tStart = performance.now();\n            if (i \u003c tvc_timesToTest - 1) {\n                tvc_dummyImage.src = tvc_testImage + '?t=' + tStart;\n                tvc_dummyImage.onload = function() {\n                    var tEnd = performance.now();\n                    var tTimeTook = tEnd - tStart;\n                    tvc_arrTimes[i] = tTimeTook;\n\n                    testLatency(cb);\n                    i++;\n                };\n            } else {\n                /** calculate average of array items then callback */\n                var sum = tvc_arrTimes.reduce(function(a, b) {\n                    return a + b;\n                });\n                var avg = sum / tvc_arrTimes.length;\n                //console.log(sum);\n                cb(sum);\n\n                dataLayer.push({\n                    \"event\": \"tvc_internet_speed_tracking\"\n                });\n            }\n\n        }\n\n}\nvar d = new Date();\nd.setTime(d.getTime()+1800000);\nvar expires = \"expires=\"+d.toGMTString();\ndocument.cookie = \"netSpeed=1; \"+expires+\"; path=/\";\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1500384280062",
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/46?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/47",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "47",
   "name": "tvc_share_link_click",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_article_share"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_get_click_text}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_clientid}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "list",
     "key": "metric",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "1"
        },
        {
         "type": "template",
         "key": "metric",
         "value": "1"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "1"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{authorName}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_articleCategory_filtered_cjs}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{articleTitle}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "6"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{publicationYear}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "7"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_month_year}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "8"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_final_date}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "10"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{videoUrl}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "9"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{articleLength}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "14"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tags}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "15"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_age}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "18"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_dr_cj}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "19"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_internalTeam_lower_cj}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1502173714654",
   "firingTriggerId": [
    "103",
    "109"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/47?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/48",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "48",
   "name": "tvc_client_id_push",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n\tvar tvc_UAID = \"UA-18111131-5\";\n\tga(function() {\n\t\tvar tvc_trackers = ga.getAll();\n\t\tvar tvc_i, tvc_len;\n\t\tfor (tvc_i = 0, tvc_len = tvc_trackers.length; tvc_i \u003c tvc_len; tvc_i += 1) {\n\t\t\tif (tvc_trackers[tvc_i].get('trackingId') === tvc_UAID) {\n              dataLayer.push({\n              \ttvc_clientid : tvc_trackers[tvc_i].get('clientId')\n              });\n\t\t\t}\n\t\t}\n\t});\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1485237907867",
   "firingTriggerId": [
    "56"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/48?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/49",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "49",
   "name": "tvc_error_occurred_from_script",
   "type": "img",
   "parameter": [
    {
     "type": "boolean",
     "key": "useCacheBuster",
     "value": "true"
    },
    {
     "type": "template",
     "key": "url",
     "value": "https://script.google.com/macros/s/AKfycbzKLZyc8y4vClBSC8FHy81h-pEJhwyOXMxJFlgfJ1SAoRTL8c_s/exec?tvc_error={{tvc_error}}&tvc_page={{tvc_page}}&tvc_tag={{tvc_tag}}&tvc_timestamp={{tvc_timestamp}}&sheetname=error_report"
    },
    {
     "type": "template",
     "key": "cacheBusterQueryParam",
     "value": "gtmcb"
    }
   ],
   "fingerprint": "1491557090103",
   "firingTriggerId": [
    "111"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/49?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/50",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "50",
   "name": "tvc_common_functions",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n  \n  // Function to push datalayer when error is occured\n  if (typeof tvc_track_error != 'function'){\n    var tvc_track_error = function(tvc_error, tvc_page, tvc_tag){\n      dataLayer.push({\n          \"event\" : \"tvc_error_occurred\",\n          \"tvc_error\" : tvc_error,\n          \"tvc_page\" : tvc_page,\n          \"tvc_tag\" : tvc_tag,\n          \"tvc_timestamp\" : new Date().toString()\n      });\n    }\n  }\n  if (typeof tvc_setCookie != 'function') {\n    var tvc_setCookie = function(cname, cvalue, exdays) {\n      var d = new Date();\n      d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));\n      var expires = 'expires=' + d.toUTCString();\n      document.cookie = cname + '=' + cvalue + '; ' + expires + '; domain=.yourstory.com; path=/';\n    }\n  }\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1491376244599",
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/50?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/51",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "51",
   "name": "tvc_internet_speed_tracking",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_internet_speed"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_connection_type}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_connection_speed}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "17"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_connection_speed}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1500387465053",
   "firingTriggerId": [
    "112"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/51?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/52",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "52",
   "name": "tvc_words_per_sentence_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\nfunction tvcGetEndOfSentence() {\n\n    var tvc_div_len;\n    var tvc_div_text;\n    var tvc_get_content = []; // Declare tvc_get_content array\n    var tvc_tot_sum = 0; // Declare sum Var\n    var tvc_tot_len = 0; //Declare Total Len Var\n    var tvc_avg_word_count = 0; //Declare Avg word count variable\n    var tvc_ys_url = window.location.href;\n    var tvc_ys_lang = tvc_ys_url.substring(8, 21);\n    var tvc_range = '';\n\n    try {\n        if (tvc_ys_lang == \"yourstory.com\") {\n            tvc_div_len = document.querySelectorAll(\n                '.articleWrapper \u003e .ys_post_content.text p').length;\n            tvc_div_text = document.querySelectorAll(\n                '.articleWrapper \u003e .ys_post_content.text p');\n        } else {\n            tvc_div_len = document.querySelectorAll(\n                '.articleWrapper \u003e .sectionUgcEditor.article p').length;\n            tvc_div_text = document.querySelectorAll(\n                '.articleWrapper \u003e .sectionUgcEditor.article p');\n        }\n    } catch (e) {\n        tvc_track_error(e.message, {{Page Path}}, \"1 - tvc_words_per_sentence_inject\");\n    }\n\n\n    for (var i = 0; i \u003c tvc_div_len; i++) {\n      try{\n        var tvc_text = tvc_div_text[i].textContent; // Get The Text Content\n        tvc_split = tvc_text.match(/[^\\.!।۔\\?]+[\\.!।۔\\?]+/g); //Check That If There is . , !,। , ? (۔)Urdu Full Stop In The sentence\n        if (tvc_split != null) { //If tvc_split returns null then do not push it in the array\n            tvc_get_content.push(tvc_split);\n        }\n      } catch (e) {\n        tvc_track_error(e.message, {{Page Path}}, \"2 - tvc_words_per_sentence_inject\");\n      }\n    }\n\n    for (var j = 0; j \u003c tvc_get_content.length; j++) {\n      try{\n        tvc_tot_len = tvc_tot_len + tvc_get_content[j].length; //This will get the total sentence\n        for (var k = 0; k \u003c tvc_get_content[j].length; k++) {\n            tvc_sen_len = tvc_get_content[j][k].split(\" \").length; //This will get the Word in sentence\n            tvc_tot_sum = tvc_tot_sum + tvc_sen_len; //This will get the Total count of the word\n        }\n      } catch (e) {\n        tvc_track_error(e.message, {{Page Path}}, \"3 - tvc_words_per_sentence_inject\");\n      }\n    }\n    try{\n      tvc_avg_word_count = tvc_tot_sum / tvc_tot_len; //Get the word count\n      tvc_answer = parseInt(tvc_avg_word_count); //store the word count\n    } catch (e) {\n      tvc_track_error(e.message, {{Page Path}}, \"4 - tvc_words_per_sentence_inject\");\n    }\n\n    try{\n      if (!isNaN(tvc_answer)) {\n          //Call get range function and check in which range Word Count falls\n          if (tvc_getTheRange(tvc_answer, 0, 10)) {\n              tvc_range = '0-10';\n          } else if (tvc_getTheRange(tvc_answer, 11, 20)) {\n              tvc_range = '11-20';\n          } else if (tvc_getTheRange(tvc_answer, 21, 30)) {\n              tvc_range = '21-30';\n          } else {\n              tvc_range = '31-above';\n          }\n          //return tvc_range;\n\n          dataLayer.push({\n              'event': 'tvc_words_per_sentence',\n              'tvc_words': tvc_answer,\n              'tvc_words_range': tvc_range\n          });\n      } else {\n          //tvc_track_error(\"Error in count : \" + tvc_avg_word_count,Page Path , \"tvc_words_per_sentence_inject\");\n      }\n    } catch (e) {\n      tvc_track_error(e.message, {{Page Path}}, \"5 - tvc_words_per_sentence_inject\");\n    }\n\n\n}\n\n//This is function is used to get the Range\nfunction tvc_getTheRange(tvc_final_avg, tvc_Min, tvc_Max) {\n    return tvc_final_avg \u003e= tvc_Min && tvc_final_avg \u003c= tvc_Max;\n}\n\ntvcGetEndOfSentence();\n\u003c/script\u003e\n"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1491397172936",
   "firingTriggerId": [
    "46"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/52?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/54",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "54",
   "name": "tvc_words_per_sentence",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_words_per_sentence"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_words_range}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{Page Path}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "flashVersion"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{tvc_words}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1487595766266",
   "firingTriggerId": [
    "115"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/54?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/57",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "57",
   "name": "tvc_page_not_found_404_pages",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "404 Error"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{Page Path}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{Referrer}}"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1501841506669",
   "firingTriggerId": [
    "143"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/57?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/60",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "60",
   "name": "tvc_reader_scanner_eng_lang_article_pages_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n  \t(function(){\n        var getOffsetTop= function( elem ) { \n           var offsetTop = 0; \n           do { \n               if ( !isNaN( elem.offsetTop ) ) { \n                   offsetTop += elem.offsetTop; \n               } \n           } while( elem = elem.offsetParent ); \n           return offsetTop; \n        }\n\n        dataLayer = window.dataLayer || [];\n        var readingArea = document.querySelector(\"#article1 \u003e div\");\n        var readerTime = 60;\n        var readerLocation = getOffsetTop(readingArea);\n        var callBackTime = 100;\n        var timer = 0;\n        var contentLength = 0;\n        var scroller = false;\n        var endContent = false;\n        var didComplete = false;\n        var pageTimeLoad = 0;\n        var scrollTimeStart = 0;\n        var timeToScroll = 0;\n        var contentTime = 0;\n        var endTime = 0;\n        if (readingArea) {\n            if (window.innerHeight \u003c readingArea.clientHeight) {\n                pageTimeLoad = new Date().getTime();\n                contentLength = readingArea.clientHeight;\n            }\n\n            var trackLocation = function() {\n                bottom = window.innerHeight + window.scrollY;\n                height = document.body.scrollHeight;\n                if (bottom \u003e readerLocation && !scroller) {\n                    scroller = true;\n                    scrollTimeStart = new Date().getTime();\n                    if (pageTimeLoad \u003e 0) {\n                        timeToScroll = Math.round((scrollTimeStart - pageTimeLoad) / 1000);\n                    } else {\n                        timeToScroll = \"\";\n                    }\n                    // Article scroll started\n                    dataLayer.push({\n                        \"event\": \"tvc_scrolling\",\n                        \"tvc_event_category\": \"tvc_reader_scanner\",\n                        \"tvc_event_action\": \"scroll_start\",\n                        \"tvc_event_label\": \"{{Page URL}}\"\n                    });\n                }\n                // Determine Reader-Scanner when user reaches end of the article\n                if (bottom \u003e= readingArea.scrollTop + readingArea.scrollHeight && !endContent) {\n                    timeToScroll = new Date().getTime();\n                    contentTime = Math.round((timeToScroll - scrollTimeStart) / 1000);\n                    if (contentTime \u003c readerTime) {\n                        dataLayer.push({\n                            \"event\": \"tvc_scrolling\",\n                            \"tvc_event_category\": \"tvc_reader_scanner\",\n                            \"tvc_event_action\": \"scanner\",\n                            \"tvc_event_label\": \"{{Page URL}}\"\n                        });\n                    } else {\n                        dataLayer.push({\n                            \"event\": \"tvc_scrolling\",\n                            \"tvc_event_category\": \"tvc_reader_scanner\",\n                            \"tvc_event_action\": \"reader\",\n                            \"tvc_event_label\": \"{{Page URL}}\"\n                        });\n                    }\n                    endContent = true;\n                }\n                if (bottom == height && !didComplete) {\n                    endTime = new Date().getTime();\n                    totalTime = Math.round((endTime - scrollTimeStart) / 1000);\n                    dataLayer.push({\n                        \"event\": \"tvc_scrolling\",\n                        \"tvc_event_category\": \"tvc_reader_scanner\",\n                        \"tvc_event_action\": \"scroll_bottom\",\n                        \"tvc_event_label\": \"{{Page URL}}\"\n                    });\n                    didComplete = true;\n                }\n            }\n            {{TVCaddEvent}}(window, 'scroll', function() {\n                if (timer) {\n                    clearTimeout(timer);\n                }\n                timer = setTimeout(trackLocation, callBackTime);\n            });\n            // Determine Reader-Scanner when user navigates away from the page without seeing whole article\n          {{TVCaddEvent}}(window, 'beforeunload', function() {\n                if (!endContent && scroller) {\n                    timeToScroll = new Date().getTime();\n                    contentTime = Math.round((timeToScroll - scrollTimeStart) / 1000);\n                    if (contentTime \u003c (bottom * readerTime) / contentLength) {\n                        dataLayer.push({\n                            \"event\": \"tvc_scrolling\",\n                            \"tvc_event_category\": \"tvc_reader_scanner\",\n                            \"tvc_event_action\": \"scanner\",\n                            \"tvc_event_label\": \"{{Page URL}}\"\n                        });\n                    } else {\n                        dataLayer.push({\n                            \"event\": \"tvc_scrolling\",\n                            \"tvc_event_category\": \"tvc_reader_scanner\",\n                            \"tvc_event_action\": \"reader\",\n                            \"tvc_event_label\": \"{{Page URL}}\"\n                        });\n                    }\n                    dataLayer.push({\n                        \"event\": \"tvc_scrolling\",\n                        \"tvc_event_category\": \"tvc_reader_scanner\",\n                        \"tvc_event_action\": \"abandoned\",\n                        \"tvc_event_label\": \"{{Page URL}}\"\n                    });\n                }\n            });\n        }\n    })();\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1513949230343",
   "firingTriggerId": [
    "175"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/60?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/61",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "61",
   "name": "Campaigns-Signature-Banner",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\nvar qs = (function(a) {\n    if (a == \"\") return {};\n    var b = {};\n    for (var i = 0; i \u003c a.length; ++i) {\n        var p = a[i].split('=', 2);\n        if (p.length == 1)\n            b[p[0]] = \"\";\n        else\n            b[p[0]] = decodeURIComponent(p[1].replace(/\\+/g, \" \"));\n    }\n    return b;\n})(window.location.search.substr(1).split('&'));\nif (qs['ref'] === 'signature') {\n    var isMobile = false;\n    if (/(android|bb\\d+|meego).+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|zte\\-/i.test(navigator.userAgent.substr(0, 4))) {\n        isMobile = true;\n    }\n    var banner_cta = document.getElementById('banner_cta');\n    var imageSrc = 'https://cdn.yourstory.com/campaigns/signature/201709/banner_story_n.jpg'\n    if (isMobile) {\n        imageSrc = 'https://cdn.yourstory.com/campaigns/signature/201709/banner_article_mobile_n.jpg';\n    }\n    if (banner_cta) {\n      var element = document.createElement(\"div\");\n      element.innerHTML = '\u003ca href=\"https://www.liveinstyle.com/signature?utm_source=yourstory&utm_medium=banner&utm_campaign=lis-signature\" target=\"_blank\"\u003e\u003cimg src=\"' + imageSrc + '\" alt=\"signature startups\" style=\"width: 100%; padding: 0 40px;\"/\u003e\u003c/a\u003e';\n      banner_cta.insertBefore(element,banner_cta.childNodes[0]);\n        \n     var x = document.getElementsByClassName(\"sideAdCard\");\n\t var i;\n     for (i = 0; i \u003c x.length; i++) {\n        x[i].style.display = 'none';\n     }\n     var topAd = document.getElementById('div-gpt-ad-1510750481771-26');\n     if(topAd){\n     \ttopAd.style.display = 'none';\n     }\n    }\n}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1513701557013",
   "firingTriggerId": [
    "175"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/61?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/62",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "62",
   "name": "tvc_share_link_click_eng_lang_article_pages_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n  var tvc_share_links = document.querySelectorAll('#article1 \u003e div \u003e div \u003e div.userfeed-info.card-userdata \u003e ul \u003e span \u003e li \u003e span \u003e a');\n  \n  for (var i=0; i\u003ctvc_share_links.length; i++) {\n    tvc_share_links[i].addEventListener('click', function(){\n     var tvc_className = this.querySelector('i').className;\n     var tvc_eventAction = tvc_className.replace('icon20','').trim(); \n      dataLayer.push({'event': 'tvc_share_link_click_eng_lang_article',\n                      'tvc_share_text_dlv': tvc_eventAction\n                     });\n    });\n  }\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1505126691465",
   "firingTriggerId": [
    "175"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/62?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/63",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "63",
   "name": "tvc_share_link_click_eng_lang_article_pages",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_article_share"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_share_text_dlv}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_clientid}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "list",
     "key": "fieldsToSet",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "fieldName",
         "value": "page"
        },
        {
         "type": "template",
         "key": "value",
         "value": "{{Page Path}}"
        }
       ]
      }
     ]
    },
    {
     "type": "list",
     "key": "metric",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "1"
        },
        {
         "type": "template",
         "key": "metric",
         "value": "1"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "1"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{authorName}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{articleTitle}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_articleCategory_filtered_cjs}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "6"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{publicationYear}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "7"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_month_year}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "8"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_final_date}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "9"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{articleLength}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "10"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{videoUrl}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{language}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "14"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tags}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "15"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_age}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "18"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_dr_cj}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "19"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_internalTeam_lower_cj}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{gaPrimaryPropertyId}}"
    }
   ],
   "fingerprint": "1505124328682",
   "firingTriggerId": [
    "182"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/63?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/64",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "64",
   "name": "Bottom_CTA",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\nvar cta = document.getElementById(\"bottom-cta\");\nif(cta){\ncta.innerHTML = '\u003cdiv class=\"row\" style=\"margin: .5rem;\"\u003e \u003cdiv class=\" col-lg-offset-3 col-lg-3 col-md-6 col-sm-12\" style=\"padding: 15px;text-align: center;border-right: 1px solid #F1F1F1;\"\u003e \u003cdiv\u003e \u003cdiv \u003e\u003cimg src=\"https://cdn.yourstory.com/assets/images/Bitcoin.png\" alt=\"Bitcoin Logo\" class=\"img-circle\" style=\"width:90px\"\u003e\u003c/div\u003e \u003cp\u003eEverything you need to know about Bitcoins, curated just for you\u003c/p\u003e \u003ca target=\"_blank\" href=\"https://yourstory.com/collections/bitcoin/?utm_source=article_page&utm_medium=banner&utm_campaign=collections\" style=\"color:#e5002d;\"\u003eCheck it out now\u003c/a\u003e \u003c/div\u003e \u003c/div\u003e\u003cdiv class=\"col-lg-3 col-md-6 col-sm-12\" style=\"padding: 15px;text-align: center;\"\u003e \u003ch2 style=\"color: #e5002d;\"\u003eStartup Legal Help\u003c/h2\u003e \u003cdiv\u003eGet expert legal advise related to your startup\u003c/p\u003e \u003ca target=\"_blank\" href=\"http://forum.yourstory.com/?utm_source=article_page&utm_medium=banner&utm_campaign=forum_promo\" style=\"color: #e5002d;\"\u003eHead over to our forum\u003c/a\u003e \u003c/div\u003e \u003c/div\u003e \u003c/div\u003e';\n}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1512366385378",
   "firingTriggerId": [
    "46"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/64?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/66",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "66",
   "name": "tvc_navigation_menu_click_all_pages_d_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n    if ({{Page Hostname}} == 'yourstory.com') {\n        try {\n            var tvc_logo = document.querySelector('#YSHeader \u003e div \u003e nav \u003e div \u003e div.navbar-header \u003e a \u003e img');\n            if (tvc_logo != null) {\n                tvc_logo.addEventListener('click', function() {\n                  dataLayer.push({});\n                  dataLayer.push({\n                        'event': 'navigation_menu_click',\n                        'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',\n                        'tvc_event_action': 'navigation - top menu clicked',\n                        'tvc_navigation_level_name': 'yourstory_logo_click',\n                        'tvc_navigation_position': 'top',\n                        'tvc_language': {{tvc_language_cjs}}\n                    });\n                });\n            }\n\n            if (document.querySelector('.morphsearch') != null) {\n                document.querySelector('.morphsearch').addEventListener('click', function() {\n                    dataLayer.push({\n                        'event': 'navigation_menu_click',\n                        'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',\n                        'tvc_event_action': 'navigation - top menu clicked',\n                        'tvc_navigation_level_name': 'search_click',\n                        'tvc_navigation_position': 'top',\n                        'tvc_language': {{tvc_language_cjs}}\n                    });\n                });\n            }\n          \t\n            if (document.querySelector('#YSHeader \u003e div \u003e span \u003e div.visible-lg.visible-md \u003e div \u003e svg') != null) {\n                document.querySelector('#YSHeader \u003e div \u003e span \u003e div.visible-lg.visible-md \u003e div \u003e svg').addEventListener('click', function() {\n                    dataLayer.push({\n                        'event': 'navigation_menu_click',\n                        'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',\n                        'tvc_event_action': 'navigation - top menu clicked',\n                        'tvc_navigation_level_name': 'notification_click',\n                        'tvc_navigation_position': 'top',\n                        'tvc_language': {{tvc_language_cjs}}\n                    });\n                });\n            }\n          \n            var tvc_navigation = document.querySelectorAll('#YSHeader \u003e div \u003e nav \u003e div \u003e div.navbar-collapse.collapse \u003e ul \u003e li \u003e a');\n            if (tvc_navigation.length \u003e 0) {\n              for(var i=0;i\u003ctvc_navigation.length;i++){\n                tvc_navigation[i].addEventListener('click', function() {\n                    dataLayer.push({\n                        'event': 'navigation_menu_click',\n                        'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',\n                        'tvc_event_action': 'navigation - top menu clicked',\n                        'tvc_navigation_level_name': this.innerText.toLowerCase(),\n                        'tvc_navigation_position': 'top',\n                        'tvc_language': {{tvc_language_cjs}}\n                    });\n                });\n               }\n            }\n        } catch (e) {\n            tvc_track_error(e.message, {{Page Path}}, '1 - tvc_navigation_menu_click_all_pages_d_inject');\n        }\n    }else{\n        var tvc_logo = document.querySelector('body \u003e header \u003e section.sectionNavigation \u003e div \u003e div.container \u003e div.fl.blockLeft \u003e div \u003e a');\n        if (tvc_logo != null) {\n        \ttry{\n\t            tvc_logo.addEventListener('click', function() {\n\t                dataLayer.push({\n\t                    'event': 'navigation_menu_click',\n\t                    'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',\n\t                    'tvc_event_action': 'navigation - top menu clicked',\n\t                    'tvc_navigation_level_name': 'yourstory_logo_click',\n\t                    'tvc_navigation_position': 'top',\n\t                    'tvc_language': {{tvc_language_cjs}}\n\t                });\n\t            });\n        \t}catch(e) {\n        \t\ttvc_track_error(e.message, {{Page Path}}, '2 - tvc_navigation_menu_click_all_pages_d_inject - subdomain_logo');\n            }\n    \t}\n\n        var tvc_subdomain = document.querySelectorAll('body \u003e header \u003e section.sectionLanguage \u003e div \u003e ul.fl.sectionLanguage_list.phn-hide \u003e li \u003e a,body \u003e header \u003e section.sectionNavigation \u003e div \u003e div.container \u003e div.fl.blockLeft \u003e div \u003e div \u003e div \u003e ul \u003e li \u003e a');\n        if (tvc_subdomain.length \u003e 0) {\n            for (var i = 0; i \u003c tvc_subdomain.length; i++) {\n                try {\n                    tvc_subdomain[i].addEventListener('click', function() {\n                        dataLayer.push({\n                            'event': 'navigation_menu_click',\n                            'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',\n                            'tvc_event_action': 'navigation - top menu clicked',\n                            'tvc_navigation_level_name': this.innerText.toLowerCase(),\n                            'tvc_navigation_position': 'top',\n                            'tvc_language': {{tvc_language_cjs}}\n                        });\n                    });\n                }catch(e) {\n                    tvc_track_error(e.message, {{Page Path}}, '2 - tvc_navigation_menu_click_all_pages_d_inject - subdomain');\n                }\n            }\n        }\n\n        var tvc_subdomain_social = document.querySelectorAll('body \u003e header \u003e section.sectionLanguage \u003e div \u003e ul.socialLinks.socialLinks-red.fr \u003e li \u003e a');\n        if (tvc_subdomain_social.length \u003e 0) {\n            for (var i = 0; i \u003c tvc_subdomain_social.length; i++) {\n                try {\n                    tvc_subdomain_social[i].addEventListener('click', function() {\n                        dataLayer.push({\n                            'event': 'navigation_menu_click',\n                            'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',\n                            'tvc_event_action': 'navigation - top menu clicked',\n                            'tvc_navigation_level_name': this.querySelector('i').className.split('-')[1],\n                            'tvc_navigation_position': 'top',\n                            'tvc_language': '{{tvc_language_cjs}}'\n                        });\n                    });\n                } catch (e) {\n                    tvc_track_error(e.message, {{Page Path}}, '2 - tvc_navigation_menu_click_all_pages_d_inject - subdomain_social');\n                }\n            }\n        }\n    } \n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1515048611679",
   "firingTriggerId": [
    "311",
    "312"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/66?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/67",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "67",
   "name": "tvc_newsletter_allpages_dr_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\ntry{\n  if({{Page Hostname}}=='yourstory.com'){\n    if(document.querySelector('.newsletter-link') != null){\n      document.querySelector('.newsletter-link').addEventListener('click',function(){\n        dataLayer.push({\n          'event':'newsletter_click',\n          'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n          'tvc_event_action':'newsletter clicked',\n          'tvc_event_label':'newsletter'\n        });\n      });\n    }\n  }\n}catch(e){\n  tvc_track_error(e.message, {{Page Path}},'tvc_newsletter_allpages_dr_inject');\n}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1510554348629",
   "firingTriggerId": [
    "56"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/67?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/68",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "68",
   "name": "tvc_writeyourownstory_click_allpages_dr_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\ntry{\n  if({{Page Hostname}}=='yourstory.com'){\n    if(document.querySelector('.write-pop') != null){\n      document.querySelector('.write-pop').addEventListener('click',function(){\n          dataLayer.push({\n              'event':'writeyourownstory_click',\n              'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n              'tvc_event_action':'writeyourownstory clicked',\n              'tvc_event_label':'writeyourownstory'\n          });\n      });\n    }\n  }\n}catch (e){\n  tvc_track_error(e.message, {{Page Path}}, \"tvc_writeyourownstory_click_allpages_dr_inject\");\n}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1510554326912",
   "firingTriggerId": [
    "56"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/68?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/69",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "69",
   "name": "tvc_hamburger_menu_allpages_dr_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n\ttry{\n      if({{Page Hostname}}=='yourstory.com'){\n        var tvc_hamburger_menu = document.querySelectorAll('section.level-one div.display-relative.menu-content ul \u003e li \u003e a,section.level-one div.menu-others ul.menu-otherlast \u003e li \u003e a');\n          if(tvc_hamburger_menu.length \u003e 0){\n            for(var i=0;i\u003ctvc_hamburger_menu.length;i++){\n                tvc_hamburger_menu[i].addEventListener('click',function(){\n                    dataLayer.push({\n                        'event':'hamburger_menu_click',\n                        'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n                        'tvc_event_action':'hamburger_menu_click',\n                        'tvc_event_label': this.innerText.toLowerCase()\n                    });\n                });\n            }\n        }\n      }\n  \t}catch(e){\n      tvc_track_error(e.message, {{Page Path}},'1 - tvc_hamburger_menu_allpages_dr_inject');\n\t}\n  \n      var tvc_hamburger_social = document.querySelectorAll('section.level-one \u003e div.top-section \u003e div.content-topsection \u003e div.super-menu \u003e div \u003e div.menu-others ul \u003e li \u003e ul \u003e li \u003e a \u003e i');\n    if(tvc_hamburger_social.length \u003e 0){\n        for(var i=0;i\u003ctvc_hamburger_social.length;i++){\n          try{\n            tvc_hamburger_social[i].addEventListener('click',function(){\n              dataLayer.push({\n                'event':'hamburger_menu_click',\n                'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n                'tvc_event_action':'hamburger_social_click',\n                'tvc_event_label': 'social_'+this.className.split(' ')[1].split('-')[0].toLowerCase()\n              });\n            });\n          }catch(e){\n            tvc_track_error(e.message, {{Page Path}},'2 - tvc_hamburger_menu_allpages_dr_inject');\n          }\n      \t}\n    }\n  \n  var tvc_hamburger_readmore = document.querySelectorAll('#menuinfo-carousel .item div.clearfix a');\n    if(tvc_hamburger_readmore.length \u003e 0){\n      for(var i=0;i\u003ctvc_hamburger_readmore.length;i++){\n        try{\n            tvc_hamburger_readmore[i].addEventListener('click',function(){\n                dataLayer.push({\n                    'event':'hamburger_menu_click',\n                    'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n                    'tvc_event_action':'hamburger_readmore_click',\n                    'tvc_event_label': this.innerText.trim()\n                });\n            });\n        }catch(e){\n          tvc_track_error(e.message, {{Page Path}},'3 - tvc_hamburger_menu_allpages_dr_inject');\n        }\n      }\n    }\n  \n  try{\n    if({{Page Hostname}}=='yourstory.com'){\n      if(document.querySelector('.open-menu') != null){\n        document.querySelector('.open-menu').addEventListener('click',function(){\n          dataLayer.push({\n            'event':'hamburger_menu_click',\n            'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n            'tvc_event_label': 'hamburger_menu_open'\n          });\n\t\t});\n      }\n\n      if(document.querySelector('section.level-one div.super-menu div.menu-header a.pull-right') != null){\n        document.querySelector('section.level-one div.super-menu div.menu-header a.pull-right').addEventListener('click',function(){\n          dataLayer.push({\n            'event':'hamburger_menu_click',\n            'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n            'tvc_event_label': 'hamburger_menu_close'\n          });\n\t\t});\n      }\n    }\n  }catch(e){\n    tvc_track_error(e.message, {{Page Path}},'4 - tvc_hamburger_menu_allpages_dr_inject');\n}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1510554030625",
   "firingTriggerId": [
    "56"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/69?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/70",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "70",
   "name": "tvc_footer_click_allpages_dr_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\ntry {\n    if ({{Page Hostname}} == 'yourstory.com') {\n        tvc_footer = document.querySelectorAll('#footerBody \u003e div \u003e div.col-lg-3.col-md-3.col-sm-3.col-xs-6 \u003e div \u003e ul \u003e li \u003e a,#footerBody \u003e div \u003e div.col-lg-2.col-md-2.col-sm-2.col-xs-6 \u003e div \u003e ul \u003e li \u003e a,#footerBody \u003e div \u003e div.col-lg-4.col-md-4.col-sm-4.col-xs-12 \u003e div \u003e p \u003e a');\n        if (tvc_footer.length \u003e 0) {\n            for (var i = 0; i \u003c tvc_footer.length; i++) {\n                try {\n                    tvc_footer[i].addEventListener('click', function() {\n                        var tvc_label = this.innerText.toLowerCase();\n                        dataLayer.push({\n                            'event': 'footer_click',\n                            'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',\n                            'tvc_event_action': 'footer clicked',\n                            'tvc_event_label': tvc_label\n                        });\n                    });\n                } catch (e) {\n                    tvc_track_error(e.message, {{Page Path}}, \"1 - tvc_footer_click_allpages_dr_inject\");\n                }\n            }\n        }\n\n        var tvc_footer_social = document.querySelectorAll('#footerBody \u003e div \u003e div.col-lg-3.col-md-3.col-sm-3.col-xs-12 \u003e div \u003e ul \u003e li \u003e a');\n        if (tvc_footer_social.length \u003e 0) {\n            for (var i = 0; i \u003c tvc_footer_social.length - 1; i++) {\n                try {\n                    tvc_footer_social[i].addEventListener('click', function() {\n                      var tvc_label;\n                      if(this.getAttribute('href').split('//')[1].split('.')[0] == 'www'){\n                        tvc_label = this.getAttribute('href').split('//')[1].split('.')[1].split('.')[0];\n                      }else{\n                      \ttvc_label = this.getAttribute('href').split('//')[1].split('.')[0];\n                      }\n                        dataLayer.push({\n                            'event': 'footer_click',\n                            'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',\n                            'tvc_event_action': 'footer clicked social',\n                            'tvc_event_label': tvc_label\n                        });\n                    });\n                } catch (e) {\n                    tvc_track_error(e.message, {{Page Path}}, \"2 - social - tvc_footer_click_allpages_dr_inject\");\n                }\n            }\n        }\n\n        try {\n            document.querySelector('#footerBody \u003e div:nth-child(1) \u003e div.col-lg-3.col-md-3.col-sm-3.col-xs-12 \u003e div \u003e ul \u003e li:nth-child(6) \u003e a').addEventListener('click', function() {\n                var tvc_label = this.getAttribute('href').split('/')[1];\n                dataLayer.push({\n                    'event': 'footer_click',\n                    'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',\n                    'tvc_event_action': 'footer clicked social',\n                    'tvc_event_label': tvc_label\n                });\n            });\n        }catch (e) {\n                tvc_track_error(e.message, {{Page Path}}, \"3 - social - tvc_footer_click_allpages_dr_inject\");\n            }\n        } else {\n            tvc_footer = document.querySelectorAll('body \u003e footer \u003e div.sectionFooterNavigation.bg-red \u003e div \u003e ul \u003e li \u003e a');\n            if (tvc_footer.length \u003e 0) {\n                for (var i = 0; i \u003c tvc_footer.length; i++) {\n                    try {\n                        tvc_footer[i].addEventListener('click', function() {\n                            dataLayer.push({\n                                'event': 'footer_click',\n                                'tvc_event_category': 'web - {{tvc_pagetype_cjs}}',\n                                'tvc_event_action': 'footer clicked',\n                                'tvc_event_label': this.innerText.toLowerCase()\n                            });\n                        });\n                    } catch (e) {\n                        tvc_track_error(e.message, {{Page Path}}, \"4 - tvc_footer_click_allpages_dr_inject\");\n                    }\n                }\n            }\n        }\n    } catch (e) {\n        tvc_track_error(e.message, {{Page Path}}, \"tvc_footer_click_allpages_dr_inject\");\n    }\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1515131096681",
   "firingTriggerId": [
    "56"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/70?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/71",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "71",
   "name": "tvc_yourstory_click_ social_allpages_dr_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\ntry{\n  if({{tvc_pagetype_cjs}}=='home page'){\n    var tvc_yourstory_click_social_click = document.querySelectorAll('.story-share a .icon20,.footer-sharing a .icon20,.socialIconWrapper');\n    var tvc_social_media_platform,tvc_page_section,tvc_article_category,tvc_article_name;\n      \n    if(tvc_yourstory_click_social_click.length \u003e 0){\n        for(var i=0;i\u003ctvc_yourstory_click_social_click.length;i++){\n            try{\n                tvc_yourstory_click_social_click[i].addEventListener('click',function(){\n                    tvc_social_media_platform = this.className.split(' ')[1];\n\n                    if({{tvc_closest_cjs}}(this,'.level-one')){\n                        tvc_page_section = 'top';\n                    }else if({{tvc_closest_cjs}}(this,'.level-two')){\n                        tvc_page_section = 'middle';\n                    }else if({{tvc_closest_cjs}}(this,'.footer-sharing')){\n                        tvc_page_section = 'bottom';\n                    }else if({{tvc_closest_cjs}}(this,'.imgWrapper')){\n                        tvc_page_section = 'imagehover';\n                    }\n\n                  if(tvc_page_section!='bottom'){\n                     if(tvc_page_section=='imagehover'){\n                          tvc_article_name = {{tvc_closest_cjs}}(this,'.content').querySelector('.title-small').innerText;\n                          tvc_article_category = {{tvc_closest_cjs}}(this,'.content').querySelector('.pill-white').innerText;\n                      }else{                                             \n                          tvc_article_name = {{tvc_closest_cjs}}(this,'.cardlayout').querySelector('.userfeed-title').innerText;\n                          tvc_article_category = {{tvc_closest_cjs}}(this,'.cardlayout').querySelector('.trending-graph a').innerText;\n                      }\n\n                      dataLayer.push({\n                          'event':'yourstory_click_social_media',\n                          'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n                          'tvc_social_media_platform': tvc_social_media_platform,\n                          'tvc_page_section':tvc_page_section,\n                          'tvc_article_category':tvc_article_category.toLowerCase(),\n                          'tvc_article_name':tvc_article_name.toLowerCase()\n                      });\n                  }else{\n                      dataLayer.push({\n                          'event':'yourstory_click_social_media',\n                          'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n                          'tvc_social_media_platform': tvc_social_media_platform,\n                          'tvc_page_section':tvc_page_section,\n                          'tvc_article_category':'NA',\n                          'tvc_article_name':'NA'\n                      });\n                  }\n                });\n            }catch(e){\n              tvc_track_error(e.message, {{Page Path}},'1 - tvc_yourstory_click_social_allpages_dr_inject');\n            }\t\n        }\n      }\n    }\n  }catch(e){\n    tvc_track_error(e.message, {{Page Path}},'tvc_yourstory_click_social_allpages_dr_inject');\n      }\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1510555233220",
   "firingTriggerId": [
    "56"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/71?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/72",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "72",
   "name": "tvc_yourstory_click_pagination_homepage_dr_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\ntry{\n\tvar tvc_pagination=document.querySelectorAll('section.pv-20 \u003e div \u003e ul \u003e li \u003e a,section.pv-20 \u003e div \u003e a');\n\t//var tvc_pageNumber = (document.querySelector('body \u003e section.pv-20 \u003e div \u003e ul \u003e li \u003e a.active')) ? document.querySelector('body \u003e section.pv-20 \u003e div \u003e ul \u003e li \u003e a.active').innerText : 'more stories';\n\n  \tif(tvc_pagination.length \u003e 0){\n      for(var i=0;i\u003ctvc_pagination.length;i++){\n          try{\n            tvc_pagination[i].addEventListener('click',function(){\n              var tvc_pageNumber = document.querySelector('body \u003e section.pv-20 \u003e div \u003e ul \u003e li \u003e a.active').innerText;\n                dataLayer.push({\n                    'event':'yourstory_click_pagination',\n                    'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n                    'tvc_pagination_action_item': tvc_pageNumber + ' \u003e ' + this.innerText.toLowerCase()\n                });\n            });\n          }catch (e){\n            tvc_track_error(e.message, {{Page Path}},\"1 - tvc_yourstory_click_pagination_homepage_dr_inject\");\n          }\n      }\n    }\n}catch(e){\n  tvc_track_error(e.message, {{Page Path}},'2 - tvc_yourstory_click_pagination_homepage_dr_inject');\n}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1515056675164",
   "firingTriggerId": [
    "56"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/72?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/73",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "73",
   "name": "tvc_yourstory_click_inarticle_articlepage_dr_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\ntry{\n  var tvc_inarticle=document.querySelectorAll('.articleWrapper .sectionUgcEditor a,#article1 div.col-sm-12.col-md-8.col-lg-8 div.clearfix a,section \u003e div.container \u003e div \u003e div \u003e p a,#article1 div.col-sm-12.col-md-8.col-lg-8 div \u003e p \u003e a');\n\n    if(tvc_inarticle.length \u003e 0){\n      for(var i=0;i\u003ctvc_inarticle.length;i++){\n        try{\n          tvc_inarticle[i].addEventListener('click',function(){\n              //tvc_article_name = tvc_closest_cjs}}(this,'.article-detailed').querySelector('h1').innerText;\n              //tvc_article_category = tvc_closest_cjs}}(this,'.article-detailed').querySelector('.trending-graph a').innerText;\n              dataLayer.push({\n                  'event':'yourstory_click_inarticle',\n                  'tvc_event_action':'inarticle_link_click',\n                  'tvc_event_label':this.innerText\n              });\n          });\n        }catch(e){\n          tvc_track_error(e.message, {{Page Path}},'1 - tvc_yourstory_click_inarticle_articlepage_dr_inject');\n        }\n      }\n    }\n}catch(e){\n  tvc_track_error(e.message, {{Page Path}},'2 - tvc_yourstory_click_inarticle_articlepage_dr_inject');\n}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1510554283022",
   "firingTriggerId": [
    "194"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/73?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/74",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "74",
   "name": "tvc_hamburger_menu_click_UA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_pagetype_cjs}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "hamburger_menu_click"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language_cjs}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1509694794027",
   "firingTriggerId": [
    "195"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/74?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/75",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "75",
   "name": "tvc_newsletter_click_UA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_pagetype_cjs}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "newsletter clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "newsletter"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language_cjs}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1509543812851",
   "firingTriggerId": [
    "196"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/75?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/76",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "76",
   "name": "tvc_writeyourownstory_click_UA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_pagetype_cjs}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "writeyourownstory clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "writeyourownstory"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language_cjs}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1509543950600",
   "firingTriggerId": [
    "197"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/76?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/77",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "77",
   "name": "tvc_footer_click_UA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_pagetype_cjs}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "footer clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language_cjs}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1509544086200",
   "firingTriggerId": [
    "198"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/77?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/78",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "78",
   "name": "tvc_yourstory_click_inarticle_UA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_pagetype_cjs}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "inarticle_link_click"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language_cjs}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{articleCategory}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{articleTitle}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1509607393530",
   "firingTriggerId": [
    "199"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/78?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/79",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "79",
   "name": "tvc_yourstory_click_social_media_UA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_pagetype_cjs}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_page_section}} - social media icon clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_social_media_platform}}"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language_cjs}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_category}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_name}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "13"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_page_section}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1509545258808",
   "firingTriggerId": [
    "200"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/79?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/80",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "80",
   "name": "tvc_yourstory_click_pagination_UA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_pagetype_cjs}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "pagination clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_pagination_action_item}}"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language_cjs}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1509545472330",
   "firingTriggerId": [
    "201"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/80?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/81",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "81",
   "name": "tvc_navigation_menu_click_UA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_pagetype_cjs}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "navigation - {{tvc_navigation_position}} menu clicked"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_navigation_level_name}}"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language_cjs}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1509545639226",
   "firingTriggerId": [
    "202"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/81?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/82",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "82",
   "name": "tvc_language_selection_all_pages_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\ntry{\n\tvar tvc_lang_change = document.querySelectorAll('#nav div.navbar-header.pull-left ul \u003e li \u003e a,section.sectionLanguage ul.fl.sectionLanguage_list.phn-hide \u003e li \u003e a');\nvar tvc_origin=window.location.origin;\n  \tif(tvc_lang_change.length \u003e 0){\n        for(var i=0;i\u003ctvc_lang_change.length;i++){\n          try{\n            tvc_lang_change[i].addEventListener('click',function(){\n                if(tvc_origin!=this.href){\n                    var tvc_hostname=tvc_origin.split('.')[0].split('//')[1];\n                    var tvc_href=this.href.split('.')[0].split('//')[1];\n\n                    if(tvc_hostname == 'yourstory'){\n                        tvc_hostname='english';\n                    }else if(tvc_href == 'yourstory'){\n                        tvc_href='english';\n                    }\n\n                    dataLayer.push({\n                        'event':'language_selection',\n                        'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n                        'tvc_event_action':'language changed',\n                        'tvc_event_label': tvc_href,\n                        'tvc_language':tvc_href\n                    });\n                }\n            });\n        }catch(e){\n          tvc_track_error(e.message, {{Page Path}},'1 - tvc_language_selection_all_pages_inject');\n        }\n      }\n    }\n}catch(e){\n  tvc_track_error(e.message, {{Page Path}},'2 - tvc_language_selection_all_pages_inject');\n}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1510554113366",
   "firingTriggerId": [
    "56"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/82?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/83",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "83",
   "name": "tvc_language_selection_UA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "web - {{tvc_pagetype_cjs}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "language changed"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1509701727823",
   "firingTriggerId": [
    "203"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/83?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/85",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "85",
   "name": "Top_CTA",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\nvar topBanner = document.createElement('div');\ntopBanner.setAttribute('class','container');\ntopBanner.innerHTML = '\u003ch4\u003e\u003ca target=\"_blank\" href=\"https://mobilesparks.yourstory.com/tickets\" style=\"color:#000000;\"\u003eUse promo code NEWBILLION and get MobileSparks tickets just at Rs 2099! Click here to buy now!\u003c/a\u003e \u003c/h4\u003e';\ntopBanner.setAttribute('style', 'background-color: #ff9a00;color: white;text-align: center;    margin-bottom: 2px; width: 100%');\nvar top_section = document.querySelector('.content-topsection');\nif(top_section){\n top_section.parentNode.insertBefore(topBanner, top_section); \n}\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1513498098397",
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/85?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/144",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "144",
   "name": "tvc_bottom_popup_cjs",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n  try{\n  \tif({{tvc_popup_check_ck}} === undefined || {{tvc_popup_check_ck}} != \"true\"){\n  \tsetTimeout(function(){\n  \tvar elem = document.createElement(\"div\");\n\telem.id = \"tvc_footer\";\n\telem.innerHTML = '\u003cdiv class=\"tvc_footer_container\"\u003e\u003cspan class=\"tvc_footer_text\" style=\"text-align: center;display: block;\"\u003e\u003cspan class=\"tvc_footer_link\" tabindex=\"0\" style=\"display: block;position: fixed;left: 0;z-index: 10000000;bottom: 0px;font-size: 1rem;padding: 18px 1.5em;overflow: hidden;cursor: default;border: 1px;color: black;text-decoration: none;align-items: center;width: 100%;left: 0;right: 0;margin-left: auto;margin-right: auto;/* line-height: 1.25rem; */font-family: Montserrat,sans-serif;font-weight: 500;background-color: white;box-shadow: 0px -2px 10px 2px rgba(0, 0, 0, 0.41);\"\u003e\u003cspan id=\"tvc_popup_close\" style=\"color: #777;font: 14px/100% arial, sans-serif;position: absolute;right: 5px;text-decoration: none;text-shadow: 0 1px 0 #fff;top: 5px;cursor: pointer;\"\u003ex\u003c/span\u003e\u003cspan style=\"line-height: 1.25rem;\"\u003eRead Shradha’s weekly wrap up of her favorite conversations &amp; interesting reads from the startup ecosystem &nbsp;&nbsp;&nbsp;\u003c/span\u003e\u003ca href=\"https://yourstory.com/newsletters/conversations_shradha\" onclick=\"return tvc_popup_eng();\" target=\"_blank\" style=\"background-color: #e63a37;color: white;padding: 10px;border-radius: 10px;line-height: 2.5rem;white-space: nowrap;\"\u003eSignup for her newsletter\u003c/a\u003e\u003c/span\u003e\u003c/span\u003e\u003c/div\u003e';\n\tdocument.body.appendChild(elem);\n\t\n   \tdocument.getElementById(\"tvc_popup_close\").addEventListener('click', function(){\n      \ttvc_popup_eng();\n\t});\n   \n    }, 20000);\n  }\n  }catch(err){\n  }\n  function tvc_popup_eng(){\n    document.getElementById(\"tvc_footer\").style.display = 'none';\n  \ttvc_setCookie(\"tvc_popup_check\",\"true\");\n  }\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "true"
    }
   ],
   "fingerprint": "1510941108961",
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/144?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/145",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "145",
   "name": "OptInMonster",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003c!-- This site is converting visitors into subscribers and customers with OptinMonster - https://optinmonster.com --\u003e\u003cscript\u003evar om2929_18306,om2929_18306_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){om2929_18306_poll(function(){if(window['om_loaded']){if(!om2929_18306){om2929_18306=new OptinMonsterApp();return om2929_18306.init({\"a\":18306,\"staging\":0,\"dev\":0,\"beta\":0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src=\"https://a.optnmstr.com/app/js/api.min.js\",o.async=true,o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState===\"loaded\"||this.readyState===\"complete\"){try{d=om_loaded=true;om2929_18306=new OptinMonsterApp();om2929_18306.init({\"a\":18306,\"staging\":0,\"dev\":0,\"beta\":0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName(\"head\")[0]||document.documentElement).appendChild(o)}(document,\"script\",\"omapi-script\");\u003c/script\u003e\u003c!-- / OptinMonster --\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1510940808419",
   "firingTriggerId": [
    "2147479553"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/145?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/146",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "146",
   "name": "tvc_survey_deck_dr_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n  try{    \nif({{tvc_survey_p}} == undefined){\n\t//code for popup\n\tvar tvc_Img_contact = document.createElement(\"img\");\n    tvc_Img_contact.setAttribute('src', 'https://d2blwevgjs7yom.cloudfront.net/tvc_contact.png');\n    tvc_Img_contact.setAttribute('alt', 'na');\n    tvc_Img_contact.setAttribute('height', '1px');\n    tvc_Img_contact.setAttribute('width', '1px');\n    document.body.appendChild(tvc_Img_contact);\n  \n  var tvc_Img_role = document.createElement(\"img\");\n    tvc_Img_role.setAttribute('src', 'https://d2blwevgjs7yom.cloudfront.net/tvc_role.png');\n    tvc_Img_role.setAttribute('alt', 'na');\n    tvc_Img_role.setAttribute('height', '1px');\n    tvc_Img_role.setAttribute('width', '1px');\n    document.body.appendChild(tvc_Img_role);\n  setTimeout(function(){\n\t\tdataLayer.push({'event':'tvc_optimize_survey_start'});\n\t}, 20000);\n}\n  }catch(e){\n  tvc_track_error(e.message, {{Page Path}},'tvc_survey_deck');\n}\n\u003c/script\u003e\t"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1513755721510",
   "firingTriggerId": [
    "281"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/146?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/147",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "147",
   "name": "tvc_survey_ua",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_survey"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_event_action}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "21"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_industry_dl}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "22"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_role_dl}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1513755762555",
   "firingTriggerId": [
    "282"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/147?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/148",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "148",
   "name": "tvc_survey_question_dr",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n\t  try{\n\tif({{tvc_survey_q}} == undefined){\n\t\tif({{tvc_page_visit}} == undefined){\n\t\t\ttvc_setCookie('tvc_page_visit',1,'1');\n\t\t}else{\n\t\t\tvar tvcCounter = function (){\n\t\t\t\t\tvar tvcCurrentCount = parseInt({{tvc_page_visit}});\n\t\t\t\t\tvar tvcUpdateCount = tvcCurrentCount+1;\n\t\t\t\t\ttvc_setCookie('tvc_page_visit',tvcUpdateCount,'1');\n\n\t\t\t\t\tif(tvcUpdateCount \u003e 2 && {{tvc_survey_p}} != undefined){\n\t\t\t\t\t\tsetTimeout(function(){\n                        \tdataLayer.push({'event':'tvc_optimize_survey_start'});\n                        }, 20000);  \n\t\t\t}\n\t\t\t}\n\t\t\tif (window.performance) {\n\t\t\t\tif (performance.navigation.type == 1) {\n\t  \t\t\t} else {\n\t    \t\t\ttvcCounter();\n\t  \t\t\t}\n\t\t\t}else{\n\t\t\t\ttvcCounter();\n\t\t\t}\n\t\t}\n\t}\n\t  }catch(e){\n\t  tvc_track_error(e.message, {{Page Path}},'tvc_survey_question');\n\t}\n\t\u003c/script\u003e\t\n"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1515741198374",
   "firingTriggerId": [
    "283"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/148?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/151",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "151",
   "name": "tvc_survey_form_all_pages_UA",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "tvc_site_visit_form"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_event_action}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "23"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_purpose_dlv}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "24"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_task_dlv}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "25"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_text_dlv}}"
        }
       ]
      }
     ]
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1513755751115",
   "firingTriggerId": [
    "288"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/151?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/155",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "155",
   "name": "tvc_optimize",
   "type": "opt",
   "parameter": [
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "optimizeContainerId",
     "value": "GTM-TF6RV5R"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1513755845796",
   "firingTriggerId": [
    "294"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/155?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/160",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "160",
   "name": "tvc_yourstory_more_stories_PWA_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n  try{\n    //code to Remove Event attached to particular element\n    \n      if({{Page Hostname}}=='yourstory.com'){\n        var tvc_more_stories = document.querySelectorAll('._8d25a355');\n        if(tvc_more_stories.length \u003e 0){\n          for(var i=0;i\u003ctvc_more_stories.length;i++){\n            tvc_more_stories[i].addEventListener('click',function(){\n              dataLayer.push({\n                'event':'yourstory_click_pagination',\n                'tvc_event_category':'web - {{tvc_pagetype_cjs}}',\n                'tvc_pagination_action_item': 'more stories'\n              });\n            });\n          }\n        }\n      }\n  }catch(e){\n  \ttvc_track_error(e.message, {{Page Path}},'tvc_yourstory_more_stories_PWA_inject');\n  }\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1515159781407",
   "firingTriggerId": [
    "56",
    "313"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/160?apiLink=tag",
   "paused": true
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/163",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "163",
   "name": "tvc_article_share_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n  try{\n    var tvc_home_page_article_share = function(){\n      setTimeout(function(){\n        var tvc_article_share = document.querySelectorAll('#root div._5fc72cc5 svg.social-icon,#root div._5fc72cc5 svg.visible-xm,#root div._5fc72cc5 svg.visible-md,#root div._5fc72cc5 svg.visible-lg,#articleShareList \u003e div._632c3311 div svg');\n        if(tvc_article_share.length \u003e 0){\n          var tvc_network_name;\n          window.dataLayer = window.dataLayer || [];\n          for (var i = 0; i \u003c tvc_article_share.length; i++) {\n            tvc_article_share[i].addEventListener('click', function() {\n              if (this.getAttribute('class').match('visible')) {\n                tvc_network_name = 'pocket';\n              }else {\n                tvc_network_name = this.getAttribute('class').split('social-icon--')[1];\n              }\n              \n              window.dataLayer.push({\n                'event': 'tvc_article_share',\n                'tvc_event_category': 'share',\n                'tvc_event_action': tvc_network_name,\n                'tvc_event_label': window.location.href\n              });\n            });\n          }\n        }\n      },5000);\n    }\n    \n    var tvc_article_page_article_share = function(){\n      setTimeout(function(){\n        var tvc_other_article_share = document.querySelectorAll('#articleShareList \u003e div._632c3311 svg,#banner_cta \u003e div.col-lg-7.col-lg-offset-1.col-md-7.col-md-offset-1.col-sm-10.col-sm-offset-1.col-xs-10.col-xs-offset-1 span svg,#banner_cta \u003e div._54306314.visible-md.visible-lg \u003e div \u003e div._1575ac37.row svg,#banner_cta \u003e div.col-lg-7.col-lg-offset-1.col-md-7.col-md-offset-1.col-sm-10.col-sm-offset-1.col-xs-10.col-xs-offset-1 div.LazyLoad.is-visible div.row svg');\n        if(tvc_other_article_share.length \u003e 0){\n          var tvc_network_name;\n          window.dataLayer = window.dataLayer || [];\n          for (var i = 0; i \u003c tvc_other_article_share.length; i++) {\n            tvc_other_article_share[i].addEventListener('click', function() {\n              if (this.getAttribute('class').match('visible')) {\n                tvc_network_name = 'pocket';\n              }else{\n                tvc_network_name = this.getAttribute('class').split('social-icon--')[1];\n              }\n              \n              window.dataLayer.push({\n                'event': 'tvc_article_share',\n                'tvc_event_category': 'share',\n                'tvc_event_action': tvc_network_name,\n                'tvc_event_label': window.location.href\n              });\n            });\n          }\n        }\n      },5000);\n    }\n\n    var tvc_flag = 0;\n    var tvc_old_url;\n    setInterval(function(){\n        if(window.location.href != tvc_old_url){\n          \n          if(window.location.pathname.match(/^\\/[0-9]{4}\\/[0-9]{2}\\/.*$/)){\n            if(tvc_flag \u003c 1){\n              tvc_article_page_article_share();\n              tvc_flag = tvc_flag + 1;\n            }\n          }else if(window.location.href == 'https://yourstory.com/'){\n            tvc_home_page_article_share();\n          }\n          \n          tvc_old_url = window.location.href;\n        }\n    },300);\n    \n    }catch(e){\n    \ttvc_track_error(e.message, {{Page Path}},'tvc_article_share_inject - 1');\n    }\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1517399814808",
   "firingTriggerId": [
    "56"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/163?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/166",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "166",
   "name": "tvc_adblocker_function_call_a_all_pages_inject",
   "type": "html",
   "priority": {
    "type": "integer",
    "value": "1"
   },
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n  try{\n    (function(){\n      var domain = '.yourstory.com';\n      setTimeout(function tvc_chek(){\n        var tvc_status = (tvc_AdBlockEnabled())?'true':'false';\n      if(tvc_status != {{tvc_adblocker_check_ck}}){\n\n        if(tvc_AdBlockEnabled()){\n\n          dataLayer.push({\n          \"event\" : \"tvc_adblock_content_ratio\",  \n          \"tvc_event_category\": \"tvc_adblocks\",\n          \"tvc_event_action\": 'ad_blocked'\n          });\n        }\n        else{\n\n          tvc_getRatio();\n        }\n        {{TVCsetCookie}}('tvc_adblocker_check',tvc_status,domain,{minutes:30});\n\n        }\n      },3000);\n    })();\n  }catch(e){\n    tvc_track_error(e.message, {{Page Path}},'tvc_adblocker_function_call_a_all_pages_inject - 1');\n  }\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1517205820116",
   "firingTriggerId": [
    "56",
    "299"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/166?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/167",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "167",
   "name": "tvc_scroll_tracking_ua",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "useDebugVersion",
     "value": "false"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "{{tvc_event_category}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_event_action}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "boolean",
     "key": "doubleClick",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "setTrackerName",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "enableLinkId",
     "value": "false"
    },
    {
     "type": "list",
     "key": "dimension",
     "list": [
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "1"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_author_name_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "2"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_name_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "5"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_category_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "6"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_year_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "7"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_month_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "8"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_publication_date_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "9"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_length_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "11"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_language_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "12"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_inifinite_position_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "14"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_tags_scroll}}"
        }
       ]
      },
      {
       "type": "map",
       "map": [
        {
         "type": "template",
         "key": "index",
         "value": "15"
        },
        {
         "type": "template",
         "key": "dimension",
         "value": "{{tvc_article_age_scroll}}"
        }
       ]
      }
     ]
    },
    {
     "type": "boolean",
     "key": "enableEcommerce",
     "value": "false"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1516628519281",
   "firingTriggerId": [
    "51"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/167?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/168",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "168",
   "name": "tvc_article_share_ua",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "{{tvc_event_category}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_event_action}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1516629015644",
   "firingTriggerId": [
    "322"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/168?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/169",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "169",
   "name": "tvc_section_click_through_homepage_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\nwindow.dataLayer = window.dataLayer || [];\nsetTimeout(\n    function() {\n      try{\n          var tvc_latest_stories = document.querySelectorAll('#root div._602184c0 div.col-md-8.col-md-offset-0.col-sm-10.col-sm-offset-1.col-xs-12 div._0a453618 a');\n          if (tvc_latest_stories.length \u003e 0) {\n              var tvc_fromPage = window.location.pathname;\n              for (var i = 0; i \u003c tvc_latest_stories.length; i++) {\n                  tvc_latest_stories[i].addEventListener('click', function() {\n                      var tvc_toPage = this.href.split('https://yourstory.com')[1];\n                      dataLayer.push({\n                          'event': 'tvc_section_click_through',\n                          'tvc_event_category': 'traversal_latest',\n                          'tvc_event_action': tvc_toPage,\n                          'tvc_event_label': tvc_fromPage,\n                          'tvc_event_value': 1\n                      });\n                  });\n              }\n          }\n\n          var tvc_startup_stories = document.querySelectorAll('#root \u003e div \u003e div \u003e div \u003e div:nth-child(2) \u003e div._602184c0 \u003e div.show-grid.row \u003e div.col-md-4.hidden-sm.hidden-xs div._0a453618 a');\n\n          if (tvc_startup_stories.length \u003e 0) {\n              var tvc_fromPage = window.location.pathname;\n              for (var i = 0; i \u003c tvc_startup_stories.length; i++) {\n                  tvc_startup_stories[i].addEventListener('click', function() {\n                      var tvc_toPage = this.href.split('https://yourstory.com')[1];\n                      dataLayer.push({\n                          'event': 'tvc_section_click_through',\n                          'tvc_event_category': 'traversal_startup',\n                          'tvc_event_action': tvc_toPage,\n                          'tvc_event_label': tvc_fromPage,\n                          'tvc_event_value': 1\n                      });\n                  });\n              }\n          }\n      }catch(e){\n    \ttvc_track_error(e.message, {{Page Path}},'tvc_section_click_through_homepage_inject - 1');\n      }\n    }, 5000);\n\nsetTimeout(\n    function() {\n      try{\n          var tvc_just_in = document.querySelectorAll('#root div._602184c0 \u003e div:nth-child(10) div._24314fa1 a');\n\n          if (tvc_just_in.length \u003e 0) {\n              var tvc_fromPage = window.location.pathname;\n              for (var i = 0; i \u003c tvc_just_in.length; i++) {\n                  tvc_just_in[i].addEventListener('click', function() {\n                      var tvc_toPage = this.href.split('https://yourstory.com')[1];\n                      dataLayer.push({\n                          'event': 'tvc_section_click_through',\n                          'tvc_event_category': 'traversal_just_in',\n                          'tvc_event_action': tvc_toPage,\n                          'tvc_event_label': tvc_fromPage,\n                          'tvc_event_value': 1\n                      });\n                  });\n              }\n          }\n\n          var tvc_trending = document.querySelectorAll('#root div._602184c0 \u003e div:nth-child(14) div._1b9d89c0 a');\n\n          if (tvc_trending.length \u003e 0) {\n              var tvc_fromPage = window.location.pathname;\n              for (var i = 0; i \u003c tvc_trending.length; i++) {\n                  tvc_trending[i].addEventListener('click', function() {\n                      var tvc_toPage = this.href.split('https://yourstory.com')[1];\n                      dataLayer.push({\n                          'event': 'tvc_section_click_through',\n                          'tvc_event_category': 'traversal_trending',\n                          'tvc_event_action': tvc_toPage,\n                          'tvc_event_label': tvc_fromPage,\n                          'tvc_event_value': 1\n                      });\n                  });\n              }\n          }\n      }catch(e){\n    \ttvc_track_error(e.message, {{Page Path}},'tvc_section_click_through_homepage_inject - 2');\n      }\n    }, 10000);\n\nsetTimeout(\n  function() {\n      try{\n          var tvc_social_stories = document.querySelectorAll('#root div._602184c0 \u003e div:nth-child(21) div._24314fa1 a');\n\n          if (tvc_social_stories.length \u003e 0) {\n              var tvc_fromPage = window.location.pathname;\n              for (var i = 0; i \u003c tvc_social_stories.length; i++) {\n                  tvc_social_stories[i].addEventListener('click', function() {\n                      var tvc_toPage = this.href.split('https://yourstory.com')[1];\n                      dataLayer.push({\n                          'event': 'tvc_section_click_through',\n                          'tvc_event_category': 'traversal_ss_home',\n                          'tvc_event_action': tvc_toPage,\n                          'tvc_event_label': tvc_fromPage,\n                          'tvc_event_value': 1\n                      });\n                  });\n              }\n          }\n\n          var tvc_her_stories = document.querySelectorAll('#root div._602184c0 \u003e div:nth-child(27) div._24314fa1 a');\n\n          if (tvc_her_stories.length \u003e 0) {\n              var tvc_fromPage = window.location.pathname;\n              for (var i = 0; i \u003c tvc_her_stories.length; i++) {\n                  tvc_her_stories[i].addEventListener('click', function() {\n                      var tvc_toPage = this.href.split('https://yourstory.com')[1];\n                      dataLayer.push({\n                          'event': 'tvc_section_click_through',\n                          'tvc_event_category': 'traversal_hs_home',\n                          'tvc_event_action': tvc_toPage,\n                          'tvc_event_label': tvc_fromPage,\n                          'tvc_event_value': 1\n                      });\n                  });\n              }\n          }\n      }catch(e){\n    \ttvc_track_error(e.message, {{Page Path}},'tvc_section_click_through_homepage_inject - 3');\n      }\n    }, 15000);\n\nsetTimeout(\n  function() {\n  \ttry{\n      var tvc_guest_author_stories = document.querySelectorAll('#root div._602184c0 \u003e div:nth-child(33) div._1b9d89c0 a');\n\n      if (tvc_guest_author_stories.length \u003e 0) {\n        var tvc_fromPage = window.location.pathname;\n        for (var i = 0; i \u003c tvc_guest_author_stories.length; i++) {\n          tvc_guest_author_stories[i].addEventListener('click', function() {\n            var tvc_toPage = this.href.split('https://yourstory.com')[1];\n            dataLayer.push({\n              'event': 'tvc_section_click_through',\n              'tvc_event_category': 'traversal_ga_home',\n              'tvc_event_action': tvc_toPage,\n              'tvc_event_label': tvc_fromPage,\n              'tvc_event_value': 1\n            });\n          });\n        }\n      }\n\n      var tvc_editors_pick = document.querySelectorAll('#root div._602184c0 \u003e div:nth-child(38) div._24314fa1 a');\n\n      if (tvc_editors_pick.length \u003e 0) {\n        var tvc_fromPage = window.location.pathname;\n        for (var i = 0; i \u003c tvc_editors_pick.length; i++) {\n          tvc_editors_pick[i].addEventListener('click', function() {\n            var tvc_toPage = this.href.split('https://yourstory.com')[1];\n            dataLayer.push({\n              'event': 'tvc_section_click_through',\n              'tvc_event_category': 'traversal_rp_home',\n              'tvc_event_action': tvc_toPage,\n              'tvc_event_label': tvc_fromPage,\n              'tvc_event_value': 1\n            });\n          });\n        }\n      }\n\n      var tvc_videos = document.querySelectorAll('#root div._602184c0 div._364125f4 a,#root div._602184c0 div.videoStoryRich a');\n\n      if (tvc_videos.length \u003e 0) {\n        var tvc_fromPage = window.location.pathname;\n        for (var i = 0; i \u003c tvc_videos.length; i++) {\n          tvc_videos[i].addEventListener('click', function() {\n            var tvc_toPage = this.href.split('https://yourstory.com')[1];\n            dataLayer.push({\n              'event': 'tvc_section_click_through',\n              'tvc_event_category': 'traversal_videos',\n              'tvc_event_action': tvc_toPage,\n              'tvc_event_label': tvc_fromPage,\n              'tvc_event_value': 1\n            });\n          });\n        }\n      }\n    }catch(e){\n    \ttvc_track_error(e.message, {{Page Path}},'tvc_section_click_through_homepage_inject - 4');\n    }\n  }, 20000);\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1517205253460",
   "firingTriggerId": [
    "324"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/169?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/170",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "170",
   "name": "tvc_section_click_through_article_page_inject",
   "type": "html",
   "parameter": [
    {
     "type": "template",
     "key": "html",
     "value": "\u003cscript\u003e\n   var tvc_section_click = function(){\n    setTimeout(function(){\n      try{\n          var tvc_read_next = document.querySelectorAll('#banner_cta \u003e div._54306314.visible-md.visible-lg \u003e div._9a5c56de div._29663c92.row a');\n          if(tvc_read_next.length \u003e 0){\n              var tvc_fromPage=window.location.pathname;\n              for(var i=0;i\u003ctvc_read_next.length;i++){\n                  tvc_read_next[i].addEventListener('click',function(){\n                      var tvc_toPage=this.href.split('https://yourstory.com')[1];\n                    dataLayer.push({\n                          'event':'tvc_section_click_through',\n                          'tvc_event_category': 'traversal_read_next',\n                          'tvc_event_action': tvc_toPage,\n                          'tvc_event_label': tvc_fromPage,\n                          'tvc_event_value': 1\n                      });\n                  });\n              }\n          }\n      }catch(e){\n    \ttvc_track_error(e.message, {{Page Path}},'tvc_section_click_through_article_page_inject - 1');\n      }\n    },5000);\n    setTimeout(function(){\n      try{\n          var tvc_you_might_also_like = document.querySelectorAll('#banner_cta \u003e div.col-lg-7.col-lg-offset-1.col-md-7.col-md-offset-1.col-sm-10.col-sm-offset-1.col-xs-10.col-xs-offset-1 div._24314fa1 a,#banner_cta \u003e div.col-lg-7.col-lg-offset-1.col-md-7.col-md-offset-1.col-sm-10.col-sm-offset-1.col-xs-10.col-xs-offset-1 div.LazyLoad.is-visible \u003e div \u003e div \u003e div \u003e a');\n          if(tvc_you_might_also_like.length \u003e 0){\n              var tvc_fromPage=window.location.pathname;\n              for(var i=0;i\u003ctvc_you_might_also_like.length;i++){\n                  tvc_you_might_also_like[i].addEventListener('click',function(){\n                      var tvc_toPage=this.href.split('https://yourstory.com')[1];\n                      dataLayer.push({\n                          'event':'tvc_section_click_through',\n                          'tvc_event_category': 'traversal_read_more',\n                          'tvc_event_action': tvc_toPage,\n                          'tvc_event_label': tvc_fromPage,\n                          'tvc_event_value': 1\n                      });\n                  });\n              }\n          }\n        \n        if({{tvc_device_cjs}}.toLowerCase() == 'mobile'){\n        \tvar tvc_mobile_horizon = document.querySelectorAll('._037ad2c0 a');\n            var tvc_fromPage=window.location.pathname;\n            for(var i=0;i\u003ctvc_mobile_horizon.length;i++){\n                tvc_mobile_horizon[i].addEventListener('click',function(){\n                    var tvc_toPage=this.href.split('https://yourstory.com')[1];\n                    dataLayer.push({\n                        'event':'tvc_section_click_through',\n                        'tvc_event_category': 'traversal_mobile_horizon',\n                        'tvc_event_action': tvc_toPage,\n                        'tvc_event_label': tvc_fromPage,\n                        'tvc_event_value': 1\n                    });\n                });\n            }          \t\n        }\n        \n      }catch(e){\n    \ttvc_track_error(e.message, {{Page Path}},'tvc_section_click_through_article_page_inject - 2');\n      }\n    },11000);\n}\n\n  try{\n    if(window.location.pathname.match(/^\\/[0-9]{4}\\/[0-9]{2}\\/.*$/)){\n        tvc_section_click();\n    }\n  }catch(e){\n    tvc_track_error(e.message, {{Page Path}},'tvc_section_click_through_article_page_inject - 3');\n  }\n\u003c/script\u003e"
    },
    {
     "type": "boolean",
     "key": "supportDocumentWrite",
     "value": "false"
    }
   ],
   "fingerprint": "1517320565443",
   "firingTriggerId": [
    "193",
    "299"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/170?apiLink=tag"
  },
  {
   "path": "accounts/250834879/containers/2159573/workspaces/323/tags/171",
   "accountId": "250834879",
   "containerId": "2159573",
   "workspaceId": "323",
   "tagId": "171",
   "name": "tvc_section_click_through_ua",
   "type": "ua",
   "parameter": [
    {
     "type": "boolean",
     "key": "nonInteraction",
     "value": "false"
    },
    {
     "type": "boolean",
     "key": "overrideGaSettings",
     "value": "true"
    },
    {
     "type": "template",
     "key": "eventValue",
     "value": "{{tvc_event_value}}"
    },
    {
     "type": "template",
     "key": "eventCategory",
     "value": "{{tvc_event_category}}"
    },
    {
     "type": "template",
     "key": "trackType",
     "value": "TRACK_EVENT"
    },
    {
     "type": "template",
     "key": "eventAction",
     "value": "{{tvc_event_action}}"
    },
    {
     "type": "template",
     "key": "eventLabel",
     "value": "{{tvc_event_label}}"
    },
    {
     "type": "template",
     "key": "trackingId",
     "value": "{{tvc_UA_ID_cv}}"
    }
   ],
   "fingerprint": "1516722529073",
   "firingTriggerId": [
    "328"
   ],
   "tagFiringOption": "oncePerEvent",
   "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/250834879/containers/2159573/workspaces/323/tags/171?apiLink=tag"
  }
 ]
};
var tvc_arr_tags=[];
for(var i=0;i<tvc_tags.tag.length;i++){
  tvc_arr.push(tvc_tags.tag[i].name);
  console.log(tvc_tags.tag[i].name);
}
console.log(tvc_arr_tags);