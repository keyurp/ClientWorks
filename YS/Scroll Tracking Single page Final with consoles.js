<script>
  
  function tvc_Execute_scroll() {
      try {
        
        dataLayer = window.dataLayer || [];
        
          var tvc_scrollingArea = document.querySelector("body");
          var tvc_callBackTime = 100;
          var tvc_timer = 0;
          var tvc_scroller = false;
          var tvc_endContent = false;
          var tvc_didComplete = false;
          var tvc_pageTimeLoad = 0;
          var tvc_scrollTimeStart = 0;
          var tvc_flag_25 = false;
          var tvc_flag_50 = false;
          var tvc_flag_75 = false;

          if (tvc_scrollingArea) {
              if (window.innerHeight < tvc_scrollingArea.clientHeight) {
                  tvc_pageTimeLoad = new Date().getTime();
              }

              var tvc_trackLocation = function() {
                
                
                  tvc_currentPosition = window.innerHeight + window.scrollY;
                  tvc_pageLength = document.body.scrollHeight;
					console.log("currentPosition"+tvc_currentPosition);
                	console.log(window.location.pathname);
                	console.log('complete:'+tvc_didComplete);
                	console.log('Page Length:'+tvc_pageLength);
                	console.log("scroller"+tvc_scroller);
               
                
                  if (!tvc_scroller) {
                      tvc_scroller = true;
                    dataLayer.push({
                          "event": "tvc_scrolling",
                          "tvc_event_category": "tvc_scrolling",
                          "tvc_event_action": "0%",
                          "tvc_event_label": location.pathname
                      });
                  }
                  //	if (tvc_currentPosition == tvc_pageLength)
                console.log('25:'+tvc_flag_25);
                
                  if (Math.round((tvc_pageLength * 25) / 100) <= tvc_currentPosition && !tvc_didComplete && !tvc_flag_25) {
                      dataLayer.push({
                          "event": "tvc_scrolling",
                          "tvc_event_category": "tvc_scrolling",
                          "tvc_event_action": "25%",
                          "tvc_event_label": location.pathname
                      });
                      tvc_flag_25 = true;
                  }
			
                console.log('50:'+tvc_flag_50);
                  if (Math.round((tvc_pageLength * 50) / 100) <= tvc_currentPosition && !tvc_didComplete && !tvc_flag_50) {
                      dataLayer.push({
                          "event": "tvc_scrolling",
                          "tvc_event_category": "tvc_scrolling",
                          "tvc_event_action": "50%",
                          "tvc_event_label": location.pathname
                      });
                      tvc_flag_50 = true;
                  }
                
                console.log('75:'+tvc_flag_75);
                  if (Math.round((tvc_pageLength * 75) / 100) <= tvc_currentPosition && !tvc_didComplete && !tvc_flag_75) {
                      dataLayer.push({
                          "event": "tvc_scrolling",
                          "tvc_event_category": "tvc_scrolling",
                          "tvc_event_action": "75%",
                          "tvc_event_label": location.pathname
                      });
                      tvc_flag_75 = true;
                  }

                  if (tvc_currentPosition == tvc_pageLength && !tvc_didComplete) {
                      dataLayer.push({
                          "event": "tvc_scrolling",
                          "tvc_event_category": "tvc_scrolling",
                          "tvc_event_action": "100%",
                          "tvc_event_label": location.pathname
                      });
                      tvc_didComplete = true;
                  }
              }

              {{TVCaddEvent}}(window, 'scroll', function(){
                  if (tvc_timer) {
                      clearTimeout(tvc_timer);
                  }
                  tvc_timer = setTimeout(tvc_trackLocation, tvc_callBackTime);
              });
          }
      } catch (e) {}
  }

  tvc_Execute_scroll();
  var tvc_old_url = window.location.href;
  setInterval(function() {
    if (window.location.href != tvc_old_url) {
      
        tvc_Execute_scroll();
		tvc_old_url = window.location.href;
    }
  }, 500);
</script>