<style type="text/css">
@media screen and (max-width:1024px) and (min-width: 300px) {
    #tvc_modal_body{width: 85%!important;}
    td{display: block;width: 100%!important;}
    #tvc_role_img_mob{display: block!important;margin-top: 20px;}
    #tvc_role_img_web{display: none!important;}
    #tvc_modal_close span{font-size: 60px!important;}
    #tvc_popup{padding-top: 20%!important;}
}

#tvc_popup{display:none;position: fixed;z-index: 99999;padding-top: 7%;padding-bottom: 30px;left: 0;top: 0px;width: 100%;height: 100%;overflow: auto;background-color: rgba(0,0,0,0.4);font-weight: 500;}
#tvc_modal_body{position: relative;background-color: #fefefe;margin: auto;padding: 0;border: 1px solid #888;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);-webkit-animation-name: animatetop;-webkit-animation-duration: .4s;animation-name: animatetop;animation-duration: .4s;width: 50%;}
#tvc_modal_close{color: white;padding: 14px 16px;font-size: 19px;border-top-left-radius: 8px;border-top-right-radius: 8px;"> <span style="float: right;font-size: 32px;font-weight: 700;line-height: 1;color: #646363;opacity: .6;margin-top: -1%;}
.tvc_modal_select{border: 1px solid #575757!important;border-radius: 4px;width: 90%;margin-top: 4px;height: 40px;padding: 0 8px;background-color: white;text-align: left;}
#tvc_modal_button{background-color: #e5002d;color: white;font-size: 15px;line-height: 2.8;padding: 0 1.5em;text-transform: uppercase;overflow: hidden;display: inline-block;vertical-align: top;text-align: center;white-space: nowrap;position: relative;border: none;z-index: 0;cursor: pointer;transition: background-color 0.3s,box-shadow 0.3s,opacity 0.3s,color 0.3s;-webkit-tap-highlight-color: rgba(0,0,0,0);margin-top: 17px;}
</style>
<script>
  try{
if({{tvc_survey_p}} == undefined){
  var tvcDiv = document.createElement("div"); 
		tvcDiv.innerHTML = '<div id="tvc_popup"> <div id="tvc_modal_body"> <div id="tvc_modal_close"><span style="float: right;font-size: 32px;font-weight: 700;line-height: 1;color: #646363;opacity: .6;margin-top: -1%;cursor: pointer;">×</span> </div> <div style="padding: 2px 33px;"> <div style="text-align:center;"> <div style="margin-bottom: 4px;line-height: 1.7;"> <table style="border-color: white!important;border-style: hidden;"> <tbody style="text-align: center;"><tr class="tvc_r1"> <td colspan="2" style=""> <h3 style="text-align: left;margin-bottom: 17px;font-weight: 500;font-size: 26px;line-height: 1.4;">Thanks for stopping by! To serve you even better with more relatable content, please help us by filling this small survey.</h3></td> </tr> <tr style="text-align: center;border-style: hidden!important;"> <td style="text-align: left;width: 50%;border-style: hidden!important;"> <img src="https://d2blwevgjs7yom.cloudfront.net/tvc_contact.png" style="vertical-align: text-bottom;""> Industry Domain</td> <td style="text-align: left;border-style: hidden!important;" id="tvc_role_img_web"><img src="https://d2blwevgjs7yom.cloudfront.net/tvc_role.png" style="vertical-align: sub;""> Role</td> </tr> <tr style="border-style: hidden!important;"> <td style="text-align: left;border-style: hidden!important;"> <select class="tvc_modal_select" id="tvc_industry"> </select></td> <td id="tvc_role_img_mob" style="text-align: left;border-style: hidden!important;display: none"><img src="https://d2blwevgjs7yom.cloudfront.net/tvc_role.png" style="vertical-align: sub;""> Role</td> <td style="text-align: left;border-style:hidden!important"><select class="tvc_modal_select" id="tvc_role"></select></td> </tr> <tr class="tvc_r1"> <td colspan="2"><input type="button" value="Submit" id="tvc_modal_button"></td> </tr> </tbody></table> <span style="color: white;font-size: 15px;" id="tvc_error">*Please select both the options</span> </div> </div> </div> </div> </div>';
		document.body.appendChild(tvcDiv);
		document.getElementById('tvc_industry').innerHTML = ' <option value="select" selected="" disabled="disabled">Select Industry Domain</option> <option value="Advertising &amp; Public Relations">Advertising &amp; Public Relations</option> <option value="Arts, Entertainment and Recreation">Arts, Entertainment, and Recreation</option> <option value="Education">Education</option> <option value="Engineering">Engineering</option> <option value="Finance &amp; Insurance">Finance &amp; Insurance</option> <option value="Government and Public Administration">Government and Public Administration</option> <option value="Health Care &amp; Medicines">Health Care &amp; Medicines</option> <option value="Hospitality, Food &amp; Beverages">Hospitality, Food &amp; Beverages</option> <option value="Human Resources">Human Resources</option> <option value="Information Technology">Information Technology</option> <option value="Legal Services">Legal Services</option> <option value="Manufacturing">Manufacturing</option> <option value="Military">Military</option> <option value="Non-Profit Organizations">Non-Profit Organizations</option> <option value="Publishing">Publishing</option><option value="Research/Sciences">Research/Sciences</option> <option value="Retail">Retail</option> <option value="Telecommunications">Telecommunications</option> <option value="Travel">Travel</option> <option value="Other">Other</option>';
		document.getElementById('tvc_role').innerHTML = ' <option value="select" selected="" disabled="disabled">Select Role</option> <option value="Administrative Staff">Administrative Staff</option> <option value="Analyst">Analyst</option> <option value="Architect">Architect</option> <option value="Consultant">Consultant</option> <option value="CXO/EVP">CXO/EVP</option> <option value="Developer">Developer</option> <option value="Educator">Educator</option> <option value="Elected Official">Elected Official</option> <option value="Individual Contributor">Individual Contributor</option> <option value="Marketing">Marketing</option> <option value="Professional Staff">Professional Staff</option> <option value="Researcher">Researcher</option> <option value="Sales">Sales</option> <option value="Self-employed/Partner">Self-employed/Partner</option> <option value="Senior Director/Director">Senior Director/Director</option> <option value="Senior Manager/Manager">Senior Manager/Manager</option> <option value="Student">Student</option> <option value="Support Staff">Support Staff</option> <option value="SVP/VP">SVP/VP</option> <option value="Other">Other</option>';
	//code for popup
	setTimeout(function(){
		tvc_setCookie('tvc_survey_p','1','730');
		dataLayer.push({
			'event':'tvc_survey',
			'tvc_event_action':'survey_displayed',
			'tvc_event_label':'-'
		});
		document.getElementById('tvc_popup').style.display = "block";
		//code for event tracking
		document.getElementById('tvc_modal_button').addEventListener("click", function(){
   		 	var tvcRole = document.getElementById('tvc_role').value;
   		 	var tvcIndustry = document.getElementById('tvc_industry').value;
   		 	if(tvcRole == 'select' || tvcIndustry == 'select'){
   		 		document.getElementById('tvc_error').style.color = "red";
   		 	}else{
   		 		dataLayer.push({
					'event':'tvc_survey',
					'tvc_event_action':'survey_submitted',
					'tvc_event_label':tvcIndustry+'|'+tvcRole,
					'tvc_industry':tvcIndustry,
					'tvc_role':tvcRole
				});
				document.getElementById('tvc_popup').style.display = "none";
   		 	}
		});
		//close button 
		document.querySelector('#tvc_modal_close span').addEventListener("click", function(){
   		 	dataLayer.push({
					'event':'tvc_survey',
					'tvc_event_action':'survey_closed',
					'tvc_event_label':'-'
				});
   		 	document.getElementById('tvc_popup').style.display = "none";
		});
	}, 20000);
}
  }catch(e){
  tvc_track_error(e.message, {{Page Path}},'tvc_survey_deck');
}
</script>	