dataLayer.push({
    viewCart: [{			//Data of Product #1
        'value': '16890', 
        'currency': 'INR',
        'content_name': 'LG M 700 Dsk 64 GB Astro Black (4 GB RAM)',
        'content_type': 'Mobiles & TabletsSmartphones',
        'content_id': '16630',
        'contents': {
            'name': 'LG M 700 Dsk 64 GB Astro Black (4 GB RAM)',
            'id': '16630',
            'price': '16890',
            'brand': 'LG',
            'category': 'Mobiles & TabletsSmartphones',
            'variant': 'Astro Black'
        }
    },
    {									//Data of Product #2
    	'value': '21700', 
        'currency': 'INR',
        'content_name': 'LG 80 cm (32 inch) HD Ready LED Smart TV (32LJ542D)',
        'content_type': 'TV & Home EntertainmentSmart TVsTelevisionLG',
        'content_id': '10879',
        'contents': {
            'name': 'LG 80 cm (32 inch) HD Ready LED Smart TV (32LJ542D)',
            'id': '10879',
            'price': '21700',
            'brand': 'LG',
            'category': 'TV & Home EntertainmentSmart TVsTelevisionLG',
            'variant': 'Black'
        }
    }]
});