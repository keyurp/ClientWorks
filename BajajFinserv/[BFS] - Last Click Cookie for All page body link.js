//BajajFinserv All Page Internal Link Last click cookie Personal-loan and home-loan page
//For links in main content
try{
    var d = new Date();
    d.setTime(d.getTime() + (30 * 60 * 1000));
    var tvc_expires = "expires=" + d.toUTCString();

    var tvc_links = document.querySelectorAll('div.itemBoxBody a,div.list_LOAnEMI a');
    if (tvc_links.length > 0) {
        for (var i = 0; i < tvc_links.length; i++) {
            if (tvc_links[i].href.match('//www.bajajfinserv.in')) {
                tvc_links[i].addEventListener('click', function() {
                    var tvc_innerText = this.innerText.trim().replace(/ /g, '-');
                    var tvc_click_Text = window.location.pathname + "_bodytext_" + tvc_innerText;
                    var tvc_value = '{"click":"' + tvc_click_Text + '"}';
                    document.cookie = 'LastClickCookie=' + tvc_value + ';' + tvc_expires + ';path=/';
                });
            }
        }
    }
}catch(e){ 
	{{tvc_track_error_cjs}}(e.name, e.message, 'tvc_LastClickCookie_all_pages_inject');
}