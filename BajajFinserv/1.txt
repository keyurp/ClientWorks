if(document.querySelectorAll('.woocommerce-billing-fields input , select').length>0){
    var tvc_formSelector = '.woocommerce-billing-fields input , select';
    var tvc_elements = document.querySelectorAll(tvc_formSelector);
    
      for (i=0; i<tvc_elements.length; i++) {
        switch (tvc_elements[i].tagName) {
          case "INPUT":
          if(tvc_elements[i].type == "text" || tvc_elements[i].type == "email" || tvc_elements[i].type == "tel")
            tvc_timer_function(tvc_elements[i]);
          break; 
          case "SELECT":
          tvc_timer_function(tvc_elements[i]);
          break; 
          case "TEXTAREA":
          tvc_timer_function(tvc_elements[i]);
          break;
		  default:
			tvc_dataLayer(tvc_element[i]);
			
        }
      }
	function tvc_dataLayer(e){
      {{TVCaddEvent}}(e,'change',function(){
              tvc_fieldName=this.getAttribute('id');
                       window.dataLayer.push({
                        'event' : 'Form_Field_Analysis',
                         'eventAction' : tvc_field_name(tvc_fieldName)
                      });         
			});
		}
      function tvc_timer_function(e){
        {{TVCaddEvent}}(e,'focus', function() {
          tvc_start_time = Date.now();
        });

        {{TVCaddEvent}}(e,'change', function() {
          tvc_end_time = Date.now();
          var tvc_elapsed_ms = tvc_end_time - tvc_start_time;
          var tvc_elaspsed_sec = tvc_elapsed_ms/1000;
          tvc_fieldName=this.getAttribute('id');
          dataLayer.push({
              'event' : 'Form_Field_Analysis',
              'eventAction' : tvc_field_name(tvc_fieldName),
              'eventLabel' : tvc_elaspsed_sec
            });
      });
    }
     
	function tvc_field_name(tvc_fieldName){
    
     //if(tvc_fieldName.includes("billing_"))
      if(tvc_fieldName.match("billing_"))
     {
          tvc_fieldName = tvc_fieldName.replace("billing_","");
      	return tvc_fieldName;
     }
    //else if(tvc_fieldName.includes("s2id_autogen1_search"))
    else if(tvc_fieldName.match("s2id_autogen1_search"))
    {
    tvc_fieldName = tvc_fieldName.replace("s2id_autogen1_search","country");
    return tvc_fieldName;
    }
      
    else if(tvc_fieldName.match("s2id_autogen2_search"))
   // else if(tvc_fieldName.includes("s2id_autogen895_search"))
    {
    tvc_fieldName = tvc_fieldName.replace("s2id_autogen2_search","state");
      return tvc_fieldName;
    }
	else
    {
    	//if(tvc_fieldName.match("country_search") < 0){
        	return tvc_fieldName;
  		//}
    }
  }  
 }