var SHEET_NAME = 'error_reporting'; //Sheet name
var SHEET_KEY = "1zpvo2FmhOOnhVxaqP6XjszjWf2m0cz5lkxxE9096teU"; //Change occurding to spreadsheet end-point
var MAIL_TO = "keyurp@tatvic.com";
var MAIL_FREQ = 50;


var SCRIPT_PROP = PropertiesService.getScriptProperties(); // new property service

// If you don't want to expose either GET or POST methods you can comment out the appropriate function
function doGet(e) {
    return handleResponse(e);
    Logger.log(e);
}

function doPost(e) {
    return handleResponse(e);
    Logger.log(e);
}

function handleResponse(e) {
    var lock = LockService.getPublicLock();
    lock.waitLock(30000); // wait 30 seconds before conceding defeat.

    try {
        // next set where we write the data - you could write to multiple/alternate destinations
        var doc = SpreadsheetApp.openById(SHEET_KEY);
        var sheet = doc.getSheetByName(SHEET_NAME);
        // we'll assume header is in row 1 but you can override with header_row in GET/POST data
        var headRow = e.parameter.header_row || 1;
        var headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];
        var nextRow = sheet.getLastRow() + 1; // get next row
        var row = [];

        // loop through the header columns
        for (i in headers) {
            var d = new Date();
            var ttamp = d.toLocaleTimeString() + ',date =' + d.getDate();
            if (headers[i] == "timestamp") { // special case if you include a 'Timestamp' column
                row.push(ttamp);
            } else { // else use header name to get data
                row.push(e.parameter[headers[i]]);
            }
        }
        // more efficient to set values as [][] array than individually
        var range = sheet.getRange(nextRow - 1, 1);

        sheet.getRange(nextRow, 1, 1, row.length).setValues([row]);
        //Errors will be sent after 50 errors
        if ((parseInt(nextRow) % MAIL_FREQ) == 0) {
            MailApp.sendEmail(MAIL_TO, 'BajajFinserv - Error Reporting! You have got new ' + MAIL_FREQ + 'Errors', row.join(','));
        }
        // return json success results
        return ContentService
            .createTextOutput(JSON.stringify({ "result": "success", "row": nextRow }))
            .setMimeType(ContentService.MimeType.JSON);
    } catch (e) {
        // if error return this
        return ContentService
            .createTextOutput(JSON.stringify({ "result": "error", "error": e }))
            .setMimeType(ContentService.MimeType.JSON);
    } finally { //release lock
        lock.releaseLock();
    }
}