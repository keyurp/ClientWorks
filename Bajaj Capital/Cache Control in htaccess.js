//Code to be written in .htaccess or http.config file for Cache controling.

E.g. 1: 

# One year for image files
<filesMatch ".(jpg|jpeg|png|gif|ico)$">
Header set Cache-Control "max-age=31536000, public"
</filesMatch>

E.g. 2:  

# One day for css and js
<filesMatch ".(css|js)$">
Header set Cache-Control "max-age=86400, public"
</filesMatch>


Note: 

The max-age is expressed in seconds.

Common max-age values are:

One minute: max-age=60
One hour: max-age=3600
One day: max-age=86400
One week: max-age=604800
One month: max-age=2628000
One year: max-age=31536000