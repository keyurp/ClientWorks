<script>
var tvc_total_ms = 0;
(function() {
    var formSelector = '#form1';
    var attribute = 'name';
    var history = [];

    {{tvc_addEvent_cjs}}(window, 'beforeunload', function() {
        if (history.length) {
            window.dataLayer.push({
                'event': 'formAbandonment',
                'eventCategory': 'Form Abandonment',
                'eventAction': history.join(' > ')
            });
        }
    }, 'tvc_form_field_SIP_NPS_d_inject - form abandonment');

    {{tvc_addEvent_cjs}}(document.querySelector(formSelector), 'change', function(e) {
        history.push(e['target'].getAttribute(attribute));
    }, 'tvc_form_field_SIP_NPS_d_inject - change');
})();

//timing analysis
//elements like textbox, textarea, checkbox, radio buttons and dropdown
//will be tracked time wise by this snippet

var tvc_elements = document.querySelectorAll('input,SELECT,textarea');
window.onload = function() {
    //var tvc_end_time,tvc_elapsed_ms,tvc_elapsed_sec,tvc_field_name;
    var tvc_formName = document.querySelector('#form1').getAttribute('class');
    for (i = 0; i < tvc_elements.length; i++) {
        switch (tvc_elements[i].tagName) {
            case "INPUT":
                if (tvc_elements[i].type == "text" || "checkbox" || "radio")
                    tvc_timer_function(tvc_elements[i]);
                break;
            case "SELECT":
                tvc_timer_function(tvc_elements[i]);
                break;
            case "TEXTAREA":
                tvc_timer_function(tvc_elements[i]);
                break;
        }
    }

    function tvc_timer_function(e){ 
      	{{tvc_addEvent_cjs}}(e, 'focus', function() {
            tvc_start_time = Date.now();
        }, 'tvc_form_field_SIP_NPS_d_inject - focus');

        {{tvc_addEvent_cjs}}(e, 'blur', function() {
            tvc_end_time = Date.now();
            tvc_elapsed_ms = tvc_end_time - tvc_start_time;
            tvc_elapsed_sec = tvc_elapsed_ms / 1000;
            tvc_field_name = e.getAttribute('name') == "" ? e.getAttribute('id') : e.getAttribute('name');
            tvc_field_value = document.querySelector('#txtName').getAttribute('value');
            if (this.value == 'null' || this.value.match('Enter your') || this.value === '') {} else {
                window.dataLayer.push({
                    'event': 'Form_Field_Analysis',
                    'eventCategory': 'Form Field Analysis - ' + tvc_formName,
                    'eventAction': tvc_field_name + ' - complete',
                    'eventLabel': tvc_elapsed_sec,
                    'formName': tvc_formName,
                    'field_name': tvc_field_name,
                    'timingCategory': 'Form Field Timing',
                    'timingVariable': tvc_field_name,
                    'timingLabel': tvc_formName,
                    'timingValue': tvc_elapsed_sec
                });
            }
            tvc_total_ms += tvc_elapsed_ms;
        }, 'tvc_form_field_SIP_NPS_d_inject - blur');
    }

    if(window.location.href.match('https://www.bajajcapital.com/mutual-funds/lp/sip-2017/index.aspx')){
      var tvc_verfication_code = document.querySelectorAll('#BtnSubmit,#Submit');
      for (var i = 0; i < tvc_verfication_code.length; i++) {
          tvc_verfication_code[i].addEventListener('click', function() {
              tvc_field_name = this.getAttribute('name') == null ? this.getAttribute('id') : this.getAttribute('name');
                window.dataLayer.push({
                  'event': 'Form_Field_Analysis',
                  'eventCategory': 'Form Field Analysis - ' + tvc_formName,
                  'eventAction': tvc_field_name + ' - complete',
                  'eventLabel': tvc_total_ms / 1000,
                  'formName': tvc_formName,
                  'field_name': tvc_field_name,
                  'timingCategory': 'Form Field Timing',
                  'timingVariable': tvc_field_name,
                  'timingLabel': tvc_formName,
                  'timingValue': tvc_total_ms / 1000
                });
          });
      }
    }

  if(window.location.href.match('http://resources.bajajcapital.com/e-mailer/nps-2015/index.php')){
    var tvc_submit_btn = document.querySelector('#submit');
    var tvc_field_id = tvc_submit_btn.getAttribute('id');
    tvc_submit_btn.addEventListener('click', function() {
                window.dataLayer.push({
                    'event': 'Form_Field_Analysis',
                    'eventCategory': 'Form Field Analysis - ' + tvc_formName,
                    'eventAction': tvc_field_id + ' - complete',
                    'eventLabel': tvc_total_ms / 1000,
                    'formName': tvc_formName,
                    'field_name': tvc_field_id,
                    'timingCategory': 'Form Field Timing',
                    'timingVariable': tvc_field_id,
                    'timingLabel': tvc_formName,
                    'timingValue': tvc_total_ms / 1000
                });
    });
  }
};
</script>