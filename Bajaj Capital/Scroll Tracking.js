<script>

dataLayer = window.dataLayer || [];
var tvc_scrollingArea = document.querySelector("body");
var tvc_callBackTime = 100;
var tvc_timer = 0;
var tvc_scroller = false;
var tvc_endContent = false;
var tvc_didComplete = false;
var tvc_pageTimeLoad = 0;
var tvc_scrollTimeStart = 0;
var tvc_flag_10 = false;
var tvc_flag_25 = false;
var tvc_flag_50 = false;
var tvc_flag_75 = false;
var tvc_flag_90 = false;

if (tvc_scrollingArea) {
    if (window.innerHeight < tvc_scrollingArea.clientHeight) {
        tvc_pageTimeLoad = new Date().getTime();
    }

    var tvc_trackLocation = function () {
        tvc_currentPosition = window.innerHeight + window.scrollY;
        tvc_pageLength = document.body.scrollHeight;

        if (!tvc_scroller) {
          tvc_scroller = true;
          dataLayer.push({
            "event": "tvc_scrolling",
            "eventCategory": "tvc_scrolling",
            "eventAction": "0%",
            "eventLabel": location.pathname
          });
        }
      
      if (tvc_currentPosition >= document.body.scrollHeight) {
          dataLayer.push({
            "event": "tvc_scrolling",
            "eventCategory": "tvc_scrolling",
            "eventAction": "100%",
            "eventLabel": location.pathname
          });
          tvc_didComplete = true;
        }
      
        if (Math.round((tvc_pageLength * 10) / 100) <= tvc_currentPosition && !tvc_didComplete && !tvc_flag_10) {
            dataLayer.push({
                "event": "tvc_scrolling",
                "eventCategory": "tvc_scrolling",
                "eventAction": "10%",
                "eventLabel": location.pathname
            });
            tvc_flag_10 = true;
        }
        if (Math.round((tvc_pageLength * 25) / 100) <= tvc_currentPosition && !tvc_didComplete && !tvc_flag_25) {
            dataLayer.push({
                "event": "tvc_scrolling",
                "eventCategory": "tvc_scrolling",
                "eventAction": "25%",
                "eventLabel": location.pathname
            });
            tvc_flag_25 = true;
        }
        if (Math.round((tvc_pageLength * 50) / 100) <= tvc_currentPosition && !tvc_didComplete && !tvc_flag_50) {
            dataLayer.push({
                "event": "tvc_scrolling",
                "eventCategory": "tvc_scrolling",
                "eventAction": "50%",
                "eventLabel": location.pathname
            });
            tvc_flag_50 = true;
        }
        if (Math.round((tvc_pageLength * 75) / 100) <= tvc_currentPosition && !tvc_didComplete && !tvc_flag_75) {
            dataLayer.push({
                "event": "tvc_scrolling",
                "eventCategory": "tvc_scrolling",
                "eventAction": "75%",
                "eventLabel": location.pathname
            });
            tvc_flag_75 = true;
        }
        if (Math.round((tvc_pageLength * 90) / 100) <= tvc_currentPosition && !tvc_didComplete && !tvc_flag_90) {
            dataLayer.push({
                "event": "tvc_scrolling",
                "eventCategory": "tvc_scrolling",
                "eventAction": "90%",
                "eventLabel": location.pathname
            });
            tvc_flag_90 = true;
        }
    }
    {{tvc_addEvent_cjs}}(window, 'scroll', function() {
        if (tvc_timer) {
            clearTimeout(tvc_timer);
        }
        tvc_timer = setTimeout(tvc_trackLocation, tvc_callBackTime);
    });
}
</script>