<script>
window.dataLayer = window.dataLayer || [];
var pageType="";
var all_header_anchors=document.querySelectorAll('#home_header_area > header > div > section.header_top > nav > div > ul > li > a,#home_header_area > header > div > section.logo_warp > div > div > a > img,#home_header_area > header > div > section.logo_warp > nav > ul > li > a,#home_header_area > header > div > section.header_top > div > ul > li.button > a,#mrova_contactform > div > a > img');
if(window.location.href=='https://www.bajajcapital.com/'){
    pageType='home page';
}
for(var i=0;i<all_header_anchors.length;i++){
    all_header_anchors[i].addEventListener('click',function(){
    	dataLayer.push({
    		'event':'header_menu_all_pages_1',
    		'eventCategory':'web - '+pageType,
    		'eventAction':'header_menu',
    		'eventLabel':this.innerText.toLowerCase()
    	});
    });
}
</script>