//Regex for All form's Destination Submit button

.*\/tvc_vp\/Clildren_MF_BtnSubmit
.*\/tvc_vp\/Fixed_Deposit_submit1
.*\/tvc_vp\/Gold_Bond_submit
.*\/tvc_vp\/GOI_Bond_submit1
.*\/tvc_vp\/SIP_Crorepati_(BtnSubmit|submit1|submit)
.*\/tvc_vp\/NPS_(Submit|submit)

//Virtual Pageview for NPS form
/e-mailer/nps-2015/index.php/tvc_vp/NPS_txtName
/e-mailer/nps-2015/index.php/tvc_vp/NPS_txtMbl


//for SIP
/e-mailer/SIP-web-16/index.php/tvc_vp/SIP_Crorepati_txtName
/e-mailer/SIP-web-16/index.php/tvc_vp/SIP_Crorepati_txtMbl
/e-mailer/SIP-web-16/index.php/tvc_vp/SIP_Crorepati_txteMail
/e-mailer/SIP-web-16/index.php/tvc_vp/SIP_Crorepati_txtCity
/e-mailer/SIP-web-16/index.php/tvc_vp/SIP_Crorepati_submit1  //<-verify code button
/e-mailer/SIP-web-16/index.php/tvc_vp/SIP_Crorepati_vcode
/e-mailer/SIP-web-16/index.php/tvc_vp/SIP_Crorepati_Submit  //<-submit button


//Prefix for All Forms
Clildren_MF
Fixed_Deposit
Gold_Bond
GOI_Bond
SIP_Crorepati
NPS