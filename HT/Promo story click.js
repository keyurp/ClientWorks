window.dataLayer = window.dataLayer || [];
var tvc_listData = document.querySelectorAll('#lftData a');
tvc_listData[0].addEventListener('click', function() {
    dataLayer.push({
        'event': 'tvc_promostory',
        'eventCategory': 'Promo Story click',
        'eventAction': 'Story clicked - 1 | ' + this.innerText,
        'eventLabel': window.location.pathname
    });
});
tvc_listData[1].addEventListener('click', function() {
    dataLayer.push({
        'event': 'tvc_promostory',
        'eventCategory': 'Promo Story click',
        'eventAction': 'Story clicked - 2 | ' + this.innerText,
        'eventLabel': window.location.pathname
    });
});
tvc_listData[2].addEventListener('click', function() {
    dataLayer.push({
        'event': 'tvc_promostory',
        'eventCategory': 'Promo Story click',
        'eventAction': 'Story clicked - 3 | ' + this.innerText,
        'eventLabel': window.location.pathname
    });
});